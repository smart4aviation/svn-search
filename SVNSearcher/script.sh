#!/bin/sh
#This script use to install all needed libraries, to compile web application.
#$MY_LIBRARY_PATH should be seted to $SVNINDEXER_WEB_PATH/lib
#You find more information in 'readme.txt' 
##Comment line below, if you execute this script not from SVNSearcher root directory
##and use direct path then

export MY_LIBRARY_PATH=public_html/WEB-INF/lib

mvn install:install-file -DgroupId=lib -DartifactId=lucene-core -Dversion=2.0.0 -Dpackaging=jar -Dfile=$MY_LIBRARY_PATH/lucene-core-2.0.0.jar
mvn install:install-file -DgroupId=lib -DartifactId=standard -Dversion=1.0 -Dpackaging=jar -Dfile=$MY_LIBRARY_PATH/standard.jar
mvn install:install-file -DgroupId=lib -DartifactId=jstl -Dversion=1.0 -Dpackaging=jar -Dfile=$MY_LIBRARY_PATH/jstl.jar
mvn install:install-file -DgroupId=lib -DartifactId=taglibs-i18n -Dversion=1.0 -Dpackaging=jar -Dfile=$MY_LIBRARY_PATH/taglibs-i18n.jar
mvn install:install-file -DgroupId=lib -DartifactId=log4j -Dversion=1.2.8 -Dpackaging=jar -Dfile=$MY_LIBRARY_PATH/log4j-1.2.8.jar
mvn install:install-file -DgroupId=lib -DartifactId=servlet -Dversion=2.3 -Dpackaging=jar -Dfile=$MY_LIBRARY_PATH/servlet-2.3.jar
