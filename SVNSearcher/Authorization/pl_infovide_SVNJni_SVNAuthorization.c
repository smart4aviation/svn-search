#include "jni.h"
#include "pl_infovide_SVNJni_SVNAuthorization.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "svn_pools.h"
#include "svn_repos.h"

/** Function checks rights for access to given pathes for given user.
user - user who use SVNSearcher
repo - array with pathes to files in repository in format: "repoName:/path/to/file" from repository root
access - path to SVN access file
This function returns boolean array. Each item of this array suites to each item from given array 'repo'. 'true' means user has access to appropriate file */
JNIEXPORT jbooleanArray JNICALL 
Java_pl_infovide_SVNJni_SVNAuthorization_authorize 
(JNIEnv *env, jobject jobj, jstring user, jobjectArray repo, jstring access)
{
	/**initializing variables*/
	jboolean b, odp;
	/** Array with result of checking for all documents*/
	jbooleanArray result;
	/** number of documents to check*/
	int length =  (int) env->GetArrayLength( repo);
	const char *_User = env->GetStringUTFChars(user, 0);
	const char *_Access = env->GetStringUTFChars(access, 0);
	result = (jbooleanArray) env->NewBooleanArray(length);
	int l;
	const char *_Base;
	const char *_Repo;
	svn_authz_t *authz_cfg;
	svn_error_t *err;
	svn_boolean_t access_granted;
	int i;
	char* sep_place;
	const char* username;
	const char* filename;
	char* reponame;
	char* path;

	/**initializing context*/
	apr_initialize();

	filename = _Access;
	username = _User;
	apr_pool_t *pool = svn_pool_create(NULL); 

	/** Checking if given access file is a valid SVN authorization configuration file*/
	if ((err = svn_repos_authz_read(&authz_cfg, filename, TRUE, pool)))
	{
		printf("Test fail on file parsing\n");
		return NULL;
	}

/**Checking rights for each given path to file*/
	for(i = 0; i < length; i++) 
	{
		jobject myObject = env->GetObjectArrayElement(repo, (jsize)i);
		l = 0;
		l = env->GetStringLength( (jstring)myObject );
		const char *str = env->GetStringUTFChars((jstring)myObject, &b);
		if ((sep_place = strchr(str, ':')) == NULL)
		{
			reponame = NULL;
			path = (char*) str;
		}
		else
		{
			sep_place[0] = 0;
			reponame = (char*) str;
			path = sep_place + 1;
		}

		/**Calling function to check rights for one path. Function writes result boolean value to 'access_granted' */
		svn_repos_authz_check_access(authz_cfg, 
                                           reponame,
                                           path,
                                           username,
                                           svn_authz_read,
                                           &access_granted, 
                                           pool);


		if(access_granted)
			odp = TRUE;
		else
			odp = FALSE;
		/**Add one result to result array*/
		env->SetBooleanArrayRegion(result, i, 1, &odp);
		env->ReleaseStringUTFChars((jstring)myObject, str);
	}

	/**Deleting context*/
	svn_pool_destroy(pool);
	apr_terminate();

	/**Release variables*/
	env->ReleaseStringUTFChars(user, _User);
	env->ReleaseStringUTFChars(access, _Access);

	return result;

}

