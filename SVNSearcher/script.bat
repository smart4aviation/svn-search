::This script use to install all needed libraries, to compile web application.
::%MY_LIBRARY_PATH% should be seted to %SVNINDEXER_WEB_PATH%/lib
:: find more information in 'readme.txt' 
::Comment line below, if you execute this script not from SVNSearcher root directory
::and use direct path then

set MY_LIBRARY_PATH=public_html/WEB-INF/lib

CALL mvn install:install-file -DgroupId=lib -DartifactId=lucene-core -Dversion=2.0.0 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/lucene-core-2.0.0.jar
CALL mvn install:install-file -DgroupId=lib -DartifactId=standard -Dversion=1.0 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/standard.jar
CALL mvn install:install-file -DgroupId=lib -DartifactId=jstl -Dversion=1.0 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/jstl.jar
CALL mvn install:install-file -DgroupId=lib -DartifactId=taglibs-i18n -Dversion=1.0 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/taglibs-i18n.jar
CALL mvn install:install-file -DgroupId=lib -DartifactId=log4j -Dversion=1.2.8 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/log4j-1.2.8.jar
CALL mvn install:install-file -DgroupId=lib -DartifactId=servlet -Dversion=2.3 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/servlet-2.3.jar
