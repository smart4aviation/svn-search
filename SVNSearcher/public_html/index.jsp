<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true"
         import="java.io.CharArrayWriter, java.io.PrintWriter" pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/i18n-1.0" prefix="i18n" %>
   
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  
  <meta content="text/html; charset=UTF-8" http-equiv="content-type">
  <i18n:bundle baseName="text.index" localeRef="userLocale"
             scope="request"
             changeResponseLocale="true" id="bundle" />
  <i18n:bundle  baseName="text.shared" localeRef="userLocale"
             scope="request"
             changeResponseLocale="true" id="shared"/>
     
  <jsp:useBean id="searchResult" scope="session" class="pl.infovide.SVNSearcher.SearchResultBean"/>
	<title><i18n:message key="title" bundle="<%= bundle %>"/></title>
<jsp:include page="common/head.jsp" flush="true" />

<SCRIPT LANGUAGE="JavaScript"  type="text/javascript">
	function setData(){
		if (get_cookie("sortType")!="") {
			document.searchForm.sort.selectedIndex=get_cookie("sortType");
		}else{
			document.searchForm.sort.selectedIndex=0;
		}	
		if (get_cookie("reverse")!="" && get_cookie("reverse")=="true" ){
			document.searchForm.reverse.checked="checked";
		}
		
		if (get_cookie("sortEnabled")!="" && get_cookie("sortEnabled")=="true" ){
			document.forms[0].sortEnabled.value="true";
			changeObjectVisibility('sort');
		}

	
	} 
	function changeObjectVisibility(objectId) {
    	var styleObject = getStyleObject(objectId, document);
    	
    	if(styleObject) { 	
    		if (styleObject.visibility == "visible"){
				var tloIn = getStyleObject('tlo', document);
				tloIn.background="url(graphics/back.png) no-repeat"; 
				styleObject.visibility = "hidden";
				document.forms[0].sortEnabled.value="false";
				
			}else {
				styleObject.visibility = "visible";
				var tloIn = getStyleObject('tlo', document);
				tloIn.background="url(graphics/backL.png) no-repeat"; 
				document.forms[0].sortEnabled.value="true";

			}
			setSortEnabled();
    	} else {
    	}
} 
 </Script>
<script type="text/javascript" type="text/javascript"><!--

RUZEE.Borders.add({
 // '.someclass': { borderType:'simple', cornerRadius:15, height: 110 },
 // '#withbgimg2': { borderType:'simple', cornerRadius:20, shadowWidth: 4 }
});

window.onload=function(){
		if ( get_cookie("defaultAdv")=="true" ){
			window.location = "./advancedSearch.jsp";
		}

  setData();		
  RUZEE.Borders.render();
  window.focus();
};

//-->
</script>

</head>
<body>
<center>
<BR><BR><BR>
<img alt="SVNSearcher" src="graphics/logo.png">

<BR><BR><BR>
<div id ="tlo">
<table  > 
<tr >
<td ALIGN="center"  >
<B><i18n:message key="searching" bundle="<%= bundle %>"/></B>
<BR><BR>
</td>
</tr>
<tr>
<td ALIGN="center">
		<form method="get" action="./search" name="searchForm" class="search-form">

			<TABLE >
			<TR>
				<TD ALIGN="right" CLASS="small-font" ><input name="search" size="50" value="<jsp:getProperty name="searchResult" property="queryString"/>"></TD>
				<TD ALIGN="left"><input name="search" value="<i18n:message key="search" bundle="<%= shared %>"/>" type="submit"></TD>
			</TR>
			<TR>
				<TD ALIGN="left" CLASS="small-font"><A HREF="./advancedSearch.jsp"><i18n:message key="advanced" bundle="<%= shared %>"/></A>&nbsp;<A HREF="javascript:changeObjectVisibility('sort');"><i18n:message key="sort" bundle="<%= shared %>" /></A> </TD>
				<TD ALIGN="right" CLASS="small-font"><a href="help.jsp"><i18n:message key="help" bundle="<%= shared %>" /></a></TD>
			</TR>
			</TABLE>
			<div id="sort">
			<TABLE ALIGN="left">
			<TR>
				<TD ALIGN="center" CLASS="small-font" >
						<i18n:message key="sortType" bundle="<%= shared %>" />
 						<SELECT NAME="sort"  onchange="setSortType();" >
							<OPTION VALUE="DDate" ><i18n:message key="ddate" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="FileName" ><i18n:message key="filename" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DocName" ><i18n:message key="docname" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DAuthor" ><i18n:message key="dauthor" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DComment" ><i18n:message key="dcomment" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="Type" ><i18n:message key="type" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="Score" ><i18n:message key="score_res" bundle="<%= shared %>" /></OPTION>
						</SELECT>
					<i18n:message key="reverse" bundle="<%= shared %>" />
					 <input name="reverse" type="checkbox" value="true" onclick="setReverse();">
				</TD>
			</TR>
			</TABLE>
			</div>	
			
			<input name="sortEnabled" value="" type="hidden">
			<input name="start" value="0" type="hidden">
</form>

</td>
</tr>
</table>
</div>

</center>
</body>
</html>
