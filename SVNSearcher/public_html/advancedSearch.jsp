<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true"
         import="java.io.CharArrayWriter, java.io.PrintWriter" pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/i18n-1.0" prefix="i18n" %>   
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  
  <meta content="text/html; charset=UTF-8" http-equiv="content-type">
  <i18n:bundle baseName="text.advanced" localeRef="userLocale"
             scope="request"
             changeResponseLocale="true" id="bundle" />
  <i18n:bundle  baseName="text.shared" localeRef="userLocale"
             scope="request"
             changeResponseLocale="true" id="shared"/>
  <jsp:useBean id="searchResult" scope="session" class="pl.infovide.SVNSearcher.SearchResultBean"/>
  <title><i18n:message key="title" bundle="<%= bundle %>"/></title>
  <jsp:include page="common/head.jsp" flush="true" />


<SCRIPT LANGUAGE="JavaScript"  type="text/javascript">
	// This function set data to fields in search Form.
	// All data are stored in user cookies
	// and this method should be called when page is loading.
	function setData(){
		if (get_cookie("sortType")!="")
			document.searchForm.sort.selectedIndex=get_cookie("sortType");
		else
			document.searchForm.sort.selectedIndex=0;
			
		if ( get_cookie("reverse")=="true" ){
			document.searchForm.reverse.checked="checked";
		}
		if ( get_cookie("filetype")!="" ){
			document.searchForm.filetype.selectedIndex=get_cookie("filetype");
		}else{
			document.searchForm.filetype.selectedIndex=0;
		}
		if ( get_cookie("path")!="" ){
			document.searchForm.path.value=get_cookie("path");
		}
		if ( get_cookie("defaultAdv")=="true" ){
			document.searchForm.defaultAdv.checked="checked";
		}
		if ( get_cookie("filebody")=="true" ){
			document.searchForm.filebody.checked="checked";
		}
		if ( get_cookie("date")!="" ){
			document.searchForm.date.selectedIndex=get_cookie("date");
		}else{
			document.searchForm.date.selectedIndex=0;
		}

	}
	

	 
 </Script>

<script type="text/javascript" type="text/javascript"><!--

RUZEE.Borders.add({
  '.someclass': { borderType:'simple', cornerRadius:15 },
  '#withbgimg2': { borderType:'simple', cornerRadius:20, shadowWidth: 4 }
});

window.onload=function(){
 //inicializing search form fields	
 setData();
 // making rounds in seacher frame
  RUZEE.Borders.render();
  window.focus();
};

//-->
</script>

</head>


<body>


<center>
<BR><BR><BR>
<img alt="SVNSearcher" src="graphics/logo.png">
<BR>
<table>
<tr>
<td>
<div class="someclassContainer">
<div class="someclass">
<center>
<B><i18n:message key="searching" bundle="<%= bundle %>"/></B>
</center>
<BR>
			<form method="get" action="./search" name="searchForm" class="search-form">

			<TABLE>
			<TR>
				<TD ALIGN="right" CLASS="small-font"><i18n:message key="phrase" bundle="<%= bundle %>"/></TD>
				<TD ALIGN="left"><input size="40" name="search" value="<jsp:getProperty name="searchResult" property="simplePhrase"/>"></TD>
			</TR>
			<TR>
				<TD ALIGN="right" CLASS="small-font"><i18n:message key="onlybody" bundle="<%= bundle %>"/></TD>
				<TD ALIGN="left"> <input name="filebody" type="checkbox" value="true" onclick="setFileBody();" ></TD>
			</TR>
			<TR>
				<TD ALIGN="right" CLASS="small-font"><i18n:message key="urlStart" bundle="<%= bundle %>"/></TD>
				<TD ALIGN="left"><input size="40" name="path" value="" onblur="setUrlStart();"></TD>
			</TR>
			<TR>
				<TD ALIGN="right" CLASS="small-font"><i18n:message key="fileType" bundle="<%= bundle %>"/></TD>
				<TD ALIGN="left">
					<SELECT NAME="filetype" onchange="setFileType();">
						<OPTION VALUE="ALL"><i18n:message key="allFile" bundle="<%= bundle %>"/></OPTION>
						<OPTION VALUE="PDF">Adobe Acrobat PDF(*.pdf)</OPTION>
						<OPTION VALUE="CPP">C, C++ source code (*.cpp *.c *.h *.c++)</OPTION>
						<OPTION VALUE="CSS">Cascading Style Sheets (*.css)</OPTION>
						<OPTION VALUE="DLL">Dynamic Link Library  (*.dll)</OPTION>
						<OPTION VALUE="EXE">Executable file (*.exe)</OPTION>
						<OPTION VALUE="HTML">HTML source code (*.html *.htm)</OPTION>
						<OPTION VALUE="CLASS">Java Class (*.class)</OPTION>
						<OPTION VALUE="JSP">JavaServer Pages source code (*.jps *.jsf)</OPTION>
						<OPTION VALUE="JAVA">Java source code (*.java)</OPTION>
						<OPTION VALUE="JS">Java Script source code (*.js)</OPTION>
						<OPTION VALUE="MDB">Microsoft Access Database (*.mdb)</OPTION>	
						<OPTION VALUE="XLS">Microsoft Excel (*.xls)</OPTION>
						<OPTION VALUE="PPS">Microsoft PowerPoint Slideshow (*.pps)</OPTION>
						<OPTION VALUE="PPT">Microsoft PowerPoint Template (*.ppt)</OPTION>
						<OPTION VALUE="DOC">Microsoft Word (*.doc)</OPTION>
						<OPTION VALUE="PL">Perl source code (*.pl)</OPTION>
						<OPTION VALUE="PHP">PHP source code (*.php)</OPTION>
						<OPTION VALUE="PY">Python source code (*.py)</OPTION>
						<OPTION VALUE="RTF">Rich Text Format (*.rtf)</OPTION>
						<OPTION VALUE="RB">Ruby source code (*.rb)</OPTION>
						<OPTION VALUE="SH">Shell and Bash source code (*.sh *.bash)</OPTION>
						<OPTION VALUE="TXT">Text file</OPTION>
						<OPTION VALUE="XML">XML file (*.xml *.xsl *.tld *.xsd)</OPTION>
						<OPTION VALUE="ZIP">ZIP and JAR archive (*.zip *.jar)</OPTION>
					</SELECT>
				</TD>
			<tr>
				<TD ALIGN="right" CLASS="small-font"><i18n:message key="date" bundle="<%= bundle %>"/></TD>
				<td ALIGN="left">
						<SELECT NAME="date" onchange="setDate();">
						<OPTION VALUE="ALL"><i18n:message key="any" bundle="<%= bundle %>"/></OPTION>
						<OPTION VALUE="1MON"><i18n:message key="1month" bundle="<%= bundle %>"/></OPTION>
						<OPTION VALUE="3MON"><i18n:message key="3months" bundle="<%= bundle %>"/></OPTION>
						<OPTION VALUE="6MON"><i18n:message key="6months" bundle="<%= bundle %>"/></OPTION>
						<OPTION VALUE="12MON"><i18n:message key="12months" bundle="<%= bundle %>"/></OPTION>
	
					</SELECT>
				 </td>
			</tr>	
			<tr>	
				<TD ALIGN="right" CLASS="small-font"> <i18n:message key="sortType" bundle="<%= shared %>" />
				</TD> 
				<TD ALIGN="left"><SELECT NAME="sort"  onchange="setSortType();" >
							<OPTION VALUE="DDate" ><i18n:message key="ddate" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="FileName" ><i18n:message key="filename" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DocName" ><i18n:message key="docname" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DAuthor" ><i18n:message key="dauthor" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DComment" ><i18n:message key="dcomment" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="Type" ><i18n:message key="type" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="Score" ><i18n:message key="score_res" bundle="<%= shared %>" /></OPTION>
						</SELECT>
				</TD>
		    </tr>
		    <tr>	
				<TD ALIGN="right" CLASS="small-font"> <i18n:message key="reverse" bundle="<%= shared %>" />
				</TD>
				<TD ALIGN="left">  <input name="reverse" type="checkbox" value="true" onclick="setReverse();">
				</TD>
		    </tr>
				
				
			<TR>
				<TD ALIGN="right" CLASS="small-font"></TD>
				<TD ALIGN="left"><input name="search" value="<i18n:message key="search" bundle="<%= shared %>"/>" type="submit"></TD>
			</TR>
			<tr>
				<td COLSPAN="2" ALIGN="left" CLASS="small-font"  ><i18n:message key="defaultAdv" bundle="<%= bundle %>" /><input name="defaultAdv" type="checkbox" value="true" onclick="setDefault();"></TD>
			</tr>
			<tr>
				<TD  COLSPAN="2" ALIGN="right" CLASS="small-font" width="70%"><a href="help.jsp"><i18n:message key="help" bundle="<%= shared %>"/></a></TD>

			</tr>
			</TABLE>

			<input name="start" value="0" type="hidden">
			</form>
			
</div>
</div>


</td>
</tr>
</table>
</center>

</body>
</html>
