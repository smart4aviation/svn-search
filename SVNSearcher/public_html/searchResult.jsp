<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true"
         import="java.io.CharArrayWriter, java.io.PrintWriter,
         java.net.URLDecoder " pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/iv.tld" prefix="iv" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/i18n-1.0" prefix="i18n" %>
 
  <i18n:bundle baseName="text.result" localeRef="userLocale"
             scope="request"
             changeResponseLocale="true" id="bundle" />
  <i18n:bundle  baseName="text.shared" localeRef="userLocale"
             scope="request"
             changeResponseLocale="true" id="shared"/>

<jsp:useBean id="searchResult" scope="session" class="pl.infovide.SVNSearcher.SearchResultBean"/>
<% request.setCharacterEncoding("UTF-8"); %>

<html>
<head>
	<title><i18n:message key="title" bundle="<%= bundle %>"/></title>
	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
	<jsp:include page="common/head.jsp" flush="true" />
<SCRIPT LANGUAGE="JavaScript"  type="text/javascript">
	function setData(){
		if (get_cookie("sortType")!="")
			document.searchForm.sort.selectedIndex=get_cookie("sortType");
		else
			document.searchForm.sort.selectedIndex=0;
			
		if ( get_cookie("reverse")=="true" ){
			document.searchForm.reverse.checked="checked";
		}
		
		if (get_cookie("sortEnabled")=="true" ){
			document.forms[0].sortEnabled.value="true";
			changeObjectVisibility('sort2');
		}

	
	}
	 
	function changeObjectVisibility(objectId) {
    	var styleObject = getStyleObject(objectId, document);
    	
    	if(styleObject) { 	
    		if (styleObject.visibility == "visible"){
				styleObject.visibility = "hidden";
				document.forms[0].sortEnabled.value="false";
				
				var style2 = getStyleObject("resultTable", document);
				style2.top="100px";
			}else {
				styleObject.visibility = "visible";
				document.forms[0].sortEnabled.value="true";
				var style2 = getStyleObject("resultTable", document);
				style2.top="115px";
			}
			setSortEnabled();
    	} else {
    	}
} 


 </Script>

<script type="text/javascript" type="text/javascript">

RUZEE.Borders.add({
  '.ramka': { borderType:'simple', cornerRadius:8},
  '#withbgimg2': { borderType:'simple', cornerRadius:20, shadowWidth: 4 }
});

window.onload=function(){
  setData();		
  RUZEE.Borders.render();
  window.focus();
};


</script>

</head>

<body>


  <form method="get" action="./search" name="searchForm" class="search-form">
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
	<td><A HREF="index.jsp"><img alt="SVNSearcher" src="graphics/logo.png" border="0"></A></td>
	<td>
	<table>
			<tr> <td><img alt="SVNSearcher" src="graphics/pixel.bmp" border="0" width="450" height="1"></td> </tr><tr> <td>&nbsp;</td> </tr>
				
			
			<tr><td>
		<input size="35" name="search" value="<jsp:getProperty name="searchResult" property="queryString"/>"> 
		<input name="search" value="<i18n:message key="search" bundle="<%= shared %>"/>" type="submit">
		<input type="hidden" name="start" value="0">
		<input name="sortEnabled" value="" type="hidden">
		<BR><A HREF="./advancedSearch.jsp"><i18n:message key="advanced" bundle="<%= shared %>"/></A>&nbsp;|&nbsp;<A href="help.jsp"><i18n:message key="help" bundle="<%= shared %>"/></A>&nbsp;|&nbsp;<A HREF="javascript:changeObjectVisibility('sort2');"><i18n:message key="sort" bundle="<%= shared %>" /></A>
			</td>
			</tr>
			<tr>
				<td></td>
			</tr>
			<tr>
					<TD ALIGN="left" CLASS="small-font" >
					<div id="sort2">
						<i18n:message key="sortType" bundle="<%= shared %>" /> <SELECT NAME="sort"  onchange="setSortType();" >
							<OPTION VALUE="DDate" ><i18n:message key="ddate" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="FileName" ><i18n:message key="filename" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DocName" ><i18n:message key="docname" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DAuthor" ><i18n:message key="dauthor" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DComment" ><i18n:message key="dcomment" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="Type" ><i18n:message key="type" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="Score" ><i18n:message key="score_res" bundle="<%= shared %>" /></OPTION>
						</SELECT>
						<i18n:message key="reverse" bundle="<%= shared %>" />
						  <input name="reverse" type="checkbox" value="true" onclick="setReverse();">
					</div>
					</TD>
			</tr>
	</table>
	</td>
  </tr>
  <tr>
 	 <td> 
 	 </td>
					
   </tr>
 
</table>
 </form>



<div id="resultTable">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
 <tr> <td><img alt="SVNSearcher" src="graphics/pixel.bmp" border="0" width="770" height="1"></td> </tr>                     
<tr>

	<c:choose>
        	<c:when test='${searchResult.error == null}'>                
        	</c:when>
        	<c:otherwise>
        				<td>
                          <table class="search-nav-form-table-warning">
                          	<tr>
                          		 <td class="search-nav-cell" valign="top"><font face="arial,verdana,geneva,lucida" size="3"><B><i18n:message key="error" bundle="<%= shared %>"/></B></font> <c:out value="${searchResult.error}" escapeXml="false" />
                          		</td>
                          	</tr>
                          	</table>
                        </td>
          		</tr>
          		<tr>       
       		 </c:otherwise>
	</c:choose>	

	<td>
		<c:choose>
      	  	<c:when test='${searchResult.numberOfFoundDocuments == searchResult.numberOfAllDocuments}'>
                          <table class="search-nav-form-table">
        	</c:when>
        <c:otherwise>
                          <table class="search-nav-form-table-warning">
        </c:otherwise>
</c:choose>								
                            
                              <tr>
                                <td noWrap class="search-nav-cell" valign="top"><font face="arial,verdana,geneva,lucida" size="3"><B>SVN:</B></font>

            <i18n:message key="all" bundle="<%= bundle %>"/>&nbsp;<B><c:out value="${searchResult.numberOfFoundDocuments}" escapeXml="false"/></B>&nbsp;<i18n:message key="access" bundle="<%= bundle %>"/> <B><c:out value="${searchResult.numberOfAllDocuments}" escapeXml="false"/></B>        

								
								</td>
                                <td noWrap align="right">
								<i18n:message key="result" bundle="<%= bundle %>"/>&nbsp;<B><% if (searchResult.getResult().size() > 0) out.println(searchResult.getStart() + 1); else out.println("0"); %>&nbsp;-&nbsp;<%= (searchResult.getStart() + searchResult.getResult().size()) %></B>&nbsp;<i18n:message key="for" bundle="<%= bundle %>"/>&nbsp;<B><jsp:getProperty name="searchResult" property="queryString"/></B>
								(<i18n:message key="search_time" bundle="<%= bundle %>"/> <B><c:out value="${searchResult.searchTime}" escapeXml="false"/></B><i18n:message key="sec" bundle="<%= bundle %>"/>&nbsp;<i18n:message key="all_time" bundle="<%= bundle %>"/> <B><c:out value="${searchResult.allTime}" escapeXml="false"/></B><i18n:message key="sec" bundle="<%= bundle %>"/>)
								
		</td>
	</tr>
	</table>



<div class="width80p">
<c:forEach var="blogEntry" items="${searchResult.result}">
<!--bean:define id="fileName" name="blogEntry" property="fileName" scope="session"/-->
<c:set var="fileName" value="${blogEntry.fileName}" scope="session"/>
    <!--start of 'for'-->


<div class="ramka">
<a class="results-link" href="<c:out value="${blogEntry.fileName}" escapeXml="false" />"><c:out value="${blogEntry.shortFileName}" escapeXml="false" /></a> <font size="1"><i18n:message key="score" bundle="<%= bundle %>"/>:&nbsp;<c:out value="${blogEntry.hitScore}" escapeXml="false" />%</font>
	<TABLE>
	<TR>
		<TD class="icon">
			<c:choose>
					<c:when test='${blogEntry.type == "class"}'>
						<IMG SRC="icons/small/class.bmp" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Java Class (*.class)">
					</c:when>
					<c:when test='${blogEntry.type == "doc"}'>
						<IMG SRC="icons/small/doc.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Microsoft Word (*.doc)">
					</c:when>
					<c:when test='${blogEntry.type == "xls"}'>
						<IMG SRC="icons/small/xls.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Microsoft Excel (*.xls)">
					</c:when>
					<c:when test='${blogEntry.type == "ppt"}'>
						<IMG SRC="icons/small/ppt.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Microsoft PowerPoint Template (*.ppt)">
					</c:when>
					<c:when test='${blogEntry.type == "pps"}'>
						<IMG SRC="icons/small/pps.bmp" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Microsoft PowerPoint Slideshow (*.pps)">
					</c:when>
					<c:when test='${blogEntry.type == "mdb"}'>
						<IMG SRC="icons/small/mdb.jpg" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Microsoft Access Database (*.mdb)">
					</c:when>
					<c:when test='${blogEntry.type == "pdf"}'>
						<IMG SRC="icons/small/pdf.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Adobe Acrobat PDF(*.pdf)">
					</c:when>
					<c:when test='${blogEntry.type == "txt"}'>
						<IMG SRC="icons/small/txt.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Text file">
					</c:when>
					<c:when test='${blogEntry.type == "xml"}'>
						<IMG SRC="icons/small/xml.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="*.xml file">
					</c:when>
					<c:when test='${blogEntry.type == "html"}'>
						<IMG SRC="icons/small/html.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="*.html file">
					</c:when>
					<c:when test='${blogEntry.type == "htm"}'>
						<IMG SRC="icons/small/html.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="*.htm file">
					</c:when>
					<c:when test='${blogEntry.type == "dll"}'>
						<IMG SRC="icons/small/dll.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Dynamic Link Library  (*.dll)">
					</c:when>
					<c:when test='${blogEntry.type == "java"}'>
						<IMG SRC="icons/small/java.bmp" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Java source code">
					</c:when>
					<c:when test='${blogEntry.type == "zip"}'>
						<IMG SRC="icons/small/zip.bmp" WIDTH="30" HEIGHT="30" BORDER="0" ALT="ZIP and JAR archive">
					</c:when>
					<c:when test='${blogEntry.type == "cpp"}'>
						<IMG SRC="icons/small/cpp.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="C++ source code">
					</c:when>
					<c:when test='${blogEntry.type == "rb"}'>
						<IMG SRC="icons/small/rb.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Ruby source code">
					</c:when>
					<c:when test='${blogEntry.type == "py"}'>
						<IMG SRC="icons/small/py.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="Python source code">
					</c:when>
					<c:when test='${blogEntry.type == "php"}'>
						<IMG SRC="icons/small/php.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="PHP source code">
					</c:when>
					<c:when test='${blogEntry.type == "tar"}'>
						<IMG SRC="icons/small/zip.bmp" WIDTH="30" HEIGHT="30" BORDER="0" ALT="TAR archive">
					</c:when>
					<c:when test='${blogEntry.type == "tif"}'>
						<IMG SRC="icons/small/tif.bmp" WIDTH="30" HEIGHT="30" BORDER="0" ALT="*.tif file">
					</c:when>
					<c:when test='${blogEntry.type == "sh"}'>
						<IMG SRC="icons/small/bash.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="*Bash source code">
					</c:when>
					<c:when test='${blogEntry.type == "bash"}'>
						<IMG SRC="icons/small/bash.png" WIDTH="30" HEIGHT="30" BORDER="0" ALT="*Bash source code">
					</c:when>
					<c:when test='${blogEntry.type == "rtf"}'>
						<IMG SRC="icons/small/rtf.gif" WIDTH="30" HEIGHT="32" BORDER="0" ALT="Rich Text Format (*.rtf)">
					</c:when>
					<c:when test='${blogEntry.type == "exe"}'>
						<IMG SRC="icons/small/exe.bmp" WIDTH="32" HEIGHT="32" BORDER="0" ALT="Executable file (*.exe)">
					</c:when>
					<c:when test='${blogEntry.type == "js"}'>
						<IMG SRC="icons/small/js.bmp" WIDTH="32" HEIGHT="32" BORDER="0" ALT="Java Script source code (*.js)">
					</c:when>
					<c:when test='${blogEntry.type == "pl"}'>
						<IMG SRC="icons/small/pl.gif" WIDTH="32" HEIGHT="32" BORDER="0" ALT="Perl source code (*.pl)">
					</c:when>
					<c:when test='${blogEntry.type == "css"}'>
						<IMG SRC="icons/small/css.bmp" WIDTH="32" HEIGHT="32" BORDER="0" ALT="Cascading Style Sheets (*.css)">
					</c:when>
					<c:when test='${blogEntry.type == "jsp"}'>
						<IMG SRC="icons/small/jsp.png" WIDTH="32" HEIGHT="32" BORDER="0" ALT="JavaServer Pages source code (*.jps *.jsf)">
					</c:when>

					
					<c:when test='${blogEntry.type == "bmp"}'>
						<A HREF="<c:out value="${blogEntry.fileName}" escapeXml="false" />"><IMG WIDTH="100" HEIGHT="100" SRC="<c:out value="${blogEntry.fileName}" escapeXml="false" />" BORDER="0" ></A>
					</c:when>
					<c:when test='${blogEntry.type == "jpg"}'>
						<A HREF="<c:out value="${blogEntry.fileName}" escapeXml="false" />"><IMG WIDTH="100" HEIGHT="100" SRC="<c:out value="${blogEntry.fileName}" escapeXml="false" />" BORDER="0" ></A>
					</c:when>
					<c:when test='${blogEntry.type == "jpeg"}'>
						<A HREF="<c:out value="${blogEntry.fileName}" escapeXml="false" />"><IMG WIDTH="100" HEIGHT="100" SRC="<c:out value="${blogEntry.fileName}" escapeXml="false" />" BORDER="0" ></A>
					</c:when>
					<c:when test='${blogEntry.type == "gif"}'>
						<A HREF="<c:out value="${blogEntry.fileName}" escapeXml="false" />"><IMG WIDTH="100" HEIGHT="100" SRC="<c:out value="${blogEntry.fileName}" escapeXml="false" />" BORDER="0" ></A>
					</c:when>
					<c:when test='${blogEntry.type == "png"}'>
						<A HREF="<c:out value="${blogEntry.fileName}" escapeXml="false" />"><IMG WIDTH="100" HEIGHT="100" SRC="<c:out value="${blogEntry.fileName}" escapeXml="false" />" BORDER="0" ></A>
					</c:when>
					
					<c:otherwise>
					</c:otherwise>
			</c:choose>								
		</TD> <!--end of 'TD icon'-->
		<TD class="result-body">
			<c:out value="${blogEntry.fileBody}" escapeXml="false" />
		</TD>
	</TR>
	</TABLE><!--end of 'TABLE in ramka'-->

	<div class="small-description">
<c:choose>
	 	<c:when test='${blogEntry.lastModified == null}'>
		</c:when>
        <c:otherwise>
                          <i18n:message key="lastCommit" bundle="<%= bundle %>"/> <fmt:formatDate value="${blogEntry.lastModified}" pattern="yyyy-MM-dd hh:mm:ss"/>, 
	 	</c:otherwise>
</c:choose>	
	
			
<c:choose>
	 	<c:when test='${blogEntry.author == null}'>
		</c:when>
        <c:otherwise>
                          <i18n:message key="byCommit" bundle="<%= bundle %>"/> <I><A HREF="mailto:<c:out value="${blogEntry.unAuthor}" escapeXml="false" />"><c:out value="${blogEntry.author}" escapeXml="false" /></A></I>
        </c:otherwise>
</c:choose>	

<c:choose>

	 	<c:when test='${blogEntry.comment == null}'>
		</c:when>
        <c:otherwise>		<BR>
                          <i18n:message key="comment" bundle="<%= bundle %>"/> <c:out value="${blogEntry.comment}"  escapeXml="false" />
        </c:otherwise>
</c:choose>		 	
	   <BR>
      
     
	</div> <!--end of 'div small-description'-->
			<div class="small-address">
					<c:out value="${blogEntry.boldName}" escapeXml="false" />
			</div>
</div>	<!--end of 'div ramka'-->							



<!--end of 'for'-->
  </c:forEach> 
</div> <!--end of 'div width80p'-->




<center>
<iv:navigator searchResult="${searchResult}"/>
</center>
<BR><BR>

</table>  <!--end of 'table in MainTable'-->
</div>  <!--end of 'div MainTable'-->


</body>
</html>
