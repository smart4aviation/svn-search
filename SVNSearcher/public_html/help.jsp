<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true"
         import="java.io.CharArrayWriter, java.io.PrintWriter,
         java.net.URLDecoder " pageEncoding="UTF-8"  %>

<%@ taglib uri="/WEB-INF/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/i18n-1.0" prefix="i18n" %>

<i18n:bundle baseName="text.help" localeRef="userLocale"
             scope="request"
             changeResponseLocale="true" id="bundle"/>
<i18n:bundle  baseName="text.shared" localeRef="userLocale"
             scope="request"
             changeResponseLocale="true" id="shared"/>


<jsp:useBean id="searchResult" scope="session" class="pl.infovide.SVNSearcher.SearchResultBean"/>
<% request.setCharacterEncoding("UTF-8"); %>

<html>
<head>
	<title><i18n:message key="title" bundle="<%= bundle %>"/></title>
	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
	<jsp:include page="common/head.jsp" flush="true" />
	
		
<SCRIPT LANGUAGE="JavaScript"  type="text/javascript">
	function setData(){
		if (get_cookie("sortType")!="")
			document.searchForm.sort.selectedIndex=get_cookie("sortType");
		else
			document.searchForm.sort.selectedIndex=0;
			
		if (get_cookie("reverse")!="" && get_cookie("reverse")=="true" ){
			document.searchForm.reverse.checked="checked";
		}
		
		if ( get_cookie("sortEnabled")=="true" ){
			document.forms[0].sortEnabled.value="true";
			changeObjectVisibility('sortHelp');
		}
	}

	function changeObjectVisibility(objectId) {
    	var styleObject = getStyleObject(objectId, document);
    	
    	if(styleObject) { 	
    		if (styleObject.visibility == "visible"){
				styleObject.visibility = "hidden";
				document.forms[0].sortEnabled.value="false";

				var style2 = getStyleObject("mainHelp", document);
				style2.top="100px";
			}else {
				styleObject.visibility = "visible";
				document.forms[0].sortEnabled.value="true";
				var style2 = getStyleObject("mainHelp", document);
				style2.top="115px";
			}
			setSortEnabled();
			
    	} else {
    	}
} 
 </Script>


<script type="text/javascript" type="text/javascript"><!--

RUZEE.Borders.add({
  '.ramka-help-toc': { borderType:'simple', cornerRadius:8},
  '#withbgimg2': { borderType:'simple', cornerRadius:20, shadowWidth: 4 }
});

window.onload=function(){
	setData();	

  RUZEE.Borders.render();
  window.focus();
};

//-->
</script>

</head>

<body>
  <form method="get" action="./search" name="searchForm" class="search-form">
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
  	<td ><A HREF="index.jsp"><img alt="SVNSearcher" src="graphics/logo.png" border="0"/></A>
  	</td>
	<td>
		<table>
			<tr> <td><img alt="SVNSearcher" src="graphics/pixel.bmp" border="0" width="450" height="1"></td> </tr><tr> <td>&nbsp;</td> </tr>
				
			
			<tr>
				<td>
				<input size="35" name="search" value="<jsp:getProperty name="searchResult" property="queryString"/>"/> 
				<input name="search" value="<i18n:message key="search" bundle="<%= shared %>"/>" type="submit"/>
				<input name="sortEnabled" value="" type="hidden">
				<input type="hidden" name="start" value="0"/>
				<BR/><A HREF="./advancedSearch.jsp"><i18n:message key="advanced" bundle="<%= shared %>"/></A>&nbsp;|&nbsp;<a href="help.jsp"><i18n:message key="help" bundle="<%= shared %>"/></a>&nbsp;|&nbsp;<A HREF="javascript:changeObjectVisibility('sortHelp');"><i18n:message key="sort" bundle="<%= shared %>" /></A>
				</td>
			</tr>
			<tr>
				<td></td>
			</tr>
			<tr>
					<TD ALIGN="left" CLASS="small-font" >
					<div id="sortHelp">
						<i18n:message key="sortType" bundle="<%= shared %>" /> <SELECT NAME="sort" onchange="setSortType();">
							<OPTION VALUE="DDate" ><i18n:message key="ddate" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="FileName" ><i18n:message key="filename" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DocName" ><i18n:message key="docname" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DAuthor" ><i18n:message key="dauthor" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="DComment" ><i18n:message key="dcomment" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="Type" ><i18n:message key="type" bundle="<%= shared %>" /></OPTION>
							<OPTION VALUE="Score" ><i18n:message key="score_res" bundle="<%= shared %>" /></OPTION>
						</SELECT>
						<i18n:message key="reverse" bundle="<%= shared %>" />
						 <input name="reverse" type="checkbox" value="true" onclick="setReverse();">
						 </div>
					</TD>
			</tr>
		</table>
	</td>
	
  </tr>

  
  
</table>
  </form>

<div id="mainHelp">

<div class="ramka-help-toc">
	<h2 class="ramka2"><i18n:message key="contents" bundle="<%= bundle %>"/></h2>
	<A HREF="#1"><i18n:message key="terms" bundle="<%= bundle %>"/></A><BR>
	<A HREF="#2"><i18n:message key="boolean_operators" bundle="<%= bundle %>"/></A><BR>
	<A HREF="#3"><i18n:message key="fields" bundle="<%= bundle %>"/></A><BR>
	<A HREF="#4"><i18n:message key="range_searches" bundle="<%= bundle %>"/></A><BR>
	<A HREF="#5"><i18n:message key="special_characters" bundle="<%= bundle %>"/></A><BR>
	<A HREF="#6"><i18n:message key="searching_in_path" bundle="<%= bundle %>"/></A><BR>
</div>

<div class="ramka-help">
	<a name="1"></a>
	<h2 class="ramka2"><i18n:message key="terms" bundle="<%= bundle %>"/></h2>
	
	<p><i18n:message key="terms1" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="terms2" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="terms3" bundle="<%= bundle %>"/></p>
</div>
<div class="ramka-help">
	<a name="2"></a>
	<h2 class="ramka2"><i18n:message key="boolean_operators" bundle="<%= bundle %>"/></h2>
	<p><i18n:message key="boolean1" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="boolean2" bundle="<%= bundle %>"/></p>
	<pre class="ramka-example"><i18n:message key="boolean_ex" bundle="<%= bundle %>"/></pre>
	<p><i18n:message key="or" bundle="<%= bundle %>"/></p>
	<pre class="ramka-example"><i18n:message key="boolean_ex2" bundle="<%= bundle %>"/></pre>
	<p><i18n:message key="boolean_gr" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="boolean_gr2" bundle="<%= bundle %>"/></p>
	<pre class="ramka-example"><i18n:message key="boolean_ex3" bundle="<%= bundle %>"/></pre>
</div>
<div class="ramka-help">
	<a name="3"></a>
	<h2 class="ramka2"><i18n:message key="fields" bundle="<%= bundle %>"/></h2>
	<p><i18n:message key="fields1" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="fields2" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="fields3" bundle="<%= bundle %>"/></p>
	<pre class="ramka-example"><i18n:message key="fields_ex" bundle="<%= bundle %>"/></pre>
	<p><i18n:message key="fields_or" bundle="<%= bundle %>"/></p>
	<pre class="ramka-example"><i18n:message key="fields_ex2" bundle="<%= bundle %>"/></pre>
	<p><i18n:message key="fields4" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="fields5" bundle="<%= bundle %>"/></p>
	<pre class="ramka-example"><i18n:message key="fields_ex3" bundle="<%= bundle %>"/></pre>
	<p><i18n:message key="fields6" bundle="<%= bundle %>"/></p>
	<BR>
	<p><b><i18n:message key="fields_av" bundle="<%= bundle %>"/></b> </p>
	<p><i18n:message key="fields_n" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="fields_fb" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="fields_au" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="fields_co" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="fields_ty" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="fields_d" bundle="<%= bundle %>"/></p>
</div>

<div class="ramka-help">
	<a name="4"></a>
	<h2 class="ramka2"><i18n:message key="range_searches" bundle="<%= bundle %>"/></h2>
	<p><i18n:message key="r1" bundle="<%= bundle %>"/></p>
	<pre class="ramka-example"><i18n:message key="r_ex1" bundle="<%= bundle %>"/></pre>
	<p><i18n:message key="r2" bundle="<%= bundle %>"/></p>
	<pre class="ramka-example"><i18n:message key="r_ex2" bundle="<%= bundle %>"/></pre>
	<p><i18n:message key="r3" bundle="<%= bundle %>"/></p>
	<p><i18n:message key="r4" bundle="<%= bundle %>"/></p>
</div>
<div class="ramka-help">
	<a name="5"></a>
	<h2 class="ramka2"><i18n:message key="special_characters" bundle="<%= bundle %>"/></h2>
	<p><i18n:message key="special1" bundle="<%= bundle %>"/></p>
	<p>+ - &amp;&amp; || ! ( ) { } [ ] ^ " ~ * ? : \</p>
	<p><i18n:message key="special2" bundle="<%= bundle %>"/></p>
	<pre class="ramka-example">\(1\+1\)\:2</pre>
</div>

<div class="ramka-help">
  <a name="6"></a>
  <h2 class="ramka2"><i18n:message key="searching_in_path" bundle="<%= bundle %>"/></h2>
  <p><i18n:message key="path" bundle="<%= bundle %>"/></p>
  <pre class="ramka-example"><i18n:message key="path_ex" bundle="<%= bundle %>"/></pre>
  <p><i18n:message key="path_war" bundle="<%= bundle %>"/></p>
  <p><i18n:message key="path_corr" bundle="<%= bundle %>"/></p>
  <pre class="ramka-example"><i18n:message key="path_ex2" bundle="<%= bundle %>"/></pre>
  <p> <i18n:message key="or" bundle="<%= bundle %>"/> </p>
  <pre class="ramka-example"><i18n:message key="path_ex3" bundle="<%= bundle %>"/></pre>
</div>
</div>

</body>
</html>
