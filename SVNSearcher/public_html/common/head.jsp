  <link href="svnindexer.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" type="text/javascript" src="common/cssquery2-p.js"></script>
  <script type="text/javascript" type="text/javascript" src="common/ruzeeborders.js"></script>
  <SCRIPT LANGUAGE="JavaScript"  type="text/javascript">
		//getting style object - to modify CSS properties
		function getStyleObject(objectId, document) {
			if(document.getElementById && document.getElementById(objectId)) {
				return document.getElementById(objectId).style;
			} else if (document.all && document.all(objectId)) {
				return document.all(objectId).style;
			} else if (document.layers && document.layers[objectId]) {
				return getObjNN4(document,objectId);
			} else {
				return false;
			}
		} 

			//Get cookie routine by Shelley Powers 
		function get_cookie(Name) {
			var search = Name + "=" ;
			var returnvalue = "" ;
			if (document.cookie.length > 0) {
				offset = document.cookie.indexOf(search);
				// if cookie exists
				if (offset != -1) { 
					offset += search.length;
					// set index of beginning of value
					end = document.cookie.indexOf(";", offset);
					// set index of end of cookie value
					if (end == -1)
						 end = document.cookie.length;
					returnvalue=unescape(document.cookie.substring(offset, end));
				}
			}
			return returnvalue;
		}
		
		// to define time expires for data in cookies
		// default: 100 days
		function getDateExpires(){
			today = new Date();
    		today.setTime(today.getTime() + 100*24*60*60*1000);
    		return today.toGMTString();
		}
		
		// Methods below set values in cookie, from fields in 
		// searcher forms on each site.
		
		function setDefault(){
    		document.cookie = "defaultAdv=" + document.searchForm.defaultAdv.checked + "; expires=" + getDateExpires();
		}
		function setReverse(){
    		document.cookie = "reverse=" + document.searchForm.reverse.checked + "; expires=" + getDateExpires();
		}
		function setSortType(){
    		document.cookie = "sortType=" + document.searchForm.sort.selectedIndex + "; expires=" + getDateExpires();		
		}
		function setSortEnabled(){
    		document.cookie = "sortEnabled=" + document.searchForm.sortEnabled.value + "; expires=" + getDateExpires();
		}
		function setFileType(){
			document.cookie = "filetype=" + document.searchForm.filetype.selectedIndex + "; expires=" + getDateExpires();
		}
		function setUrlStart(){
			document.cookie = "path=" + document.searchForm.path.value + "; expires=" + getDateExpires();
		}
		function setFileBody(){
			document.cookie = "filebody=" + document.searchForm.filebody.checked + "; expires=" + getDateExpires();	
		}
		function setDate(){
			document.cookie = "date=" + document.searchForm.date.selectedIndex + "; expires=" + getDateExpires();		
		}
		
</Script>