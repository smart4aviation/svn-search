Quick Usage (with disabled authorization option - 'authorization=false' in 'conf.properties'):

1) Copy SVNSearcher.war to your $CATALINA_HOME/webbapps and it will be automatically unpacked.
2) Edit file $CATALINA_HOME/webbapps/SVNSearcher-1.3.0/WEB-INF/web.xml 
and set correct patch to conf.properties file (the same file which is used in SVNIndexer) in '<param-value>'.
3) Restart your Tomcat.

----------------------
Quick Usage (with enabled authorization option - - 'authorization=true' in 'conf.properties' ):

1) Export viariable $APR_PATH to directory with 'APR/include' 
2) Export viariable $MY_JAVA_PATH to directory with Java VM 
3) Export viariable $SVN_PATH to directory where you copied SVN header files 
4) Export viariable $MY_LIB_PATH to directory with modules: ibsvn_repos-1.so and libsvn_subr-1.so. Default it's /usr/lib
5) Execute 'make' in 'Authorization' directory
6) After building 'libAuthorization.so' copy this file to '$CATALINA_HOME/shared/lib'. 
7) Export viarable '$LD_LIBRARY_PATH=$CATALINA_HOME/shared/lib'. This variable must be always exported when you start Tomcat.
8) Copy SVNSearcher-1.3.0.war to your '$CATALINA_HOME/webbapps' and it will be automatically unpacked.
9) Edit file $CATALINA_HOME/webbapps/SVNSearcher-1.3.0/WEB-INF/web.xml 
   and set correct patch to conf.properties file (the same file which is used in SVNIndexer) in '<param-value>'.
10) Restart your Tomcat and make sure that Tomcat has enabled authorization option.


----------------------------
Quick "How to compile SVNIndexer-webapplication":

1) Export variable $MY_LIBRARY_PATH to '$SVNINDEXER_WEB_PATH/public_html/WEB-INF/lib' in 'script.sh' file,
or leave it default, but your current directory must be 'SVNSearcher' root directory then. 
2) Execute './script.sh' to install all needed libraries
3) Execute 'mvn package' and SVNSearcher-1.3.0.war will be created in target directory

