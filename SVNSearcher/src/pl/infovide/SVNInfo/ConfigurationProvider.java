package pl.infovide.SVNInfo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.lucene.search.BooleanQuery;

/**
 * Class takes, from properties file, needed information
 * @author kleszcz
 *
 */
public class ConfigurationProvider
{	
	public static String DEFAULT_USER="anonymous";
	private static String filename;
	private Properties prop;
	private String url;
	private String directPath;
	private String indexPath;
	private String access;
	private int maxClouseCount;
	private String scriptPath; /**<This field was used only when ruby script was method to authorization.
Currently this field is not using. If you want to use this script make sure, that 'scriptPath'property has default setting. >*/
	private boolean authorization; 
	private int resultOnPage;
	private String repListFile;
	
	private String urlRegexp;
	private String urlReplacement;
	
	public String getRepListFile() {
		return repListFile;
	}
	
	public int getResultOnPage() {
		return resultOnPage;
	}
	
	public static void setFilename(String filename) {
		ConfigurationProvider.filename = filename;
	}
	
	public String getFilename() {
		return filename;
	}
	
	public boolean isAuthorization() {
		return authorization;
	}
	
	/**
	 * This method was used only when ruby script 
	 * was method to authorization.
	 * Currently this method is not using.
	 * 
	 * If you want to use this script make sure, that 'scriptPath'
	 * property has default setting.
	 * 
	 * @return path to directory with 'svn_authz_test' ruby authorization script
	 */
	public String getScriptPath() {
		if (scriptPath != null)
			return scriptPath.trim();
		return "";
	}
	
	public String getAccess() {
		return access.trim();
	}
		
	public int getMaxClouseCount() {
		return maxClouseCount;
	}
	
	public String getIndexPath() {
		return indexPath.trim();
	}
	
	public String getDirectPath() {
		if (directPath != null)
			return directPath.trim();
		return directPath;
	}
	
	public String getUrl() {
		return url.trim();
	}


	public String getUrlRegexp() {
		return urlRegexp;
	}

	public String getUrlReplacement() {
		return urlReplacement;
	}

	/**
	 * Method takes information from .properties file
	 * @param path - path to .properties file
	 * @throws ConfProviderException Syntax error in properties file.
	 * @throws FileNotFoundException Thrown when coulden't find properties file
	 * @throws IOException low level error in reading properties file
	 */
	public void getConfiguration(String path) throws  FileNotFoundException, IOException,  ConfProviderException {
		try {
			prop = new Properties();
			FileInputStream confFile = new FileInputStream(path);
			prop.load(confFile);
			
			/*
			 * 'url' and 'repListFile' are obligated
			 */
			url = prop.getProperty("url");
			repListFile = prop.getProperty("repListFile");
			
			access = prop.getProperty("access");
			
			/*
			 * DESCRIPTION : 'directPath' is taken to check
			 * if that path really exist in local file system.
			 * if exists then looking for files *.format
			 * and if find some then is taking each one
			 * as repository to make index.
			 * 
			 * If there is no any *.format files, then ignore
			 * directPath and taking 'repListFile' to get repositories name.
			 */
			directPath = prop.getProperty("directPath");
			
			indexPath = prop.getProperty("indexPath");
			if (indexPath == null)
				indexPath="IndexFiles/";
				
			if( (! indexPath.trim().endsWith("/")) && ( ! indexPath.trim().endsWith("\\")) )
					throw new ConfProviderException("Incorrect syntax in "+getFilename()+"  -  check 'indexPath'," +
					" make sure it end \"/\" or with \"\\\\\" ");
			
			if (prop.getProperty("maxClouseCount") == null)
				maxClouseCount = 3000;
			else
				maxClouseCount = (new Integer(prop.getProperty("maxClouseCount"))).intValue();
			
			if (prop.getProperty("resultOnPage") == null)
				resultOnPage = 10;
			else
				resultOnPage = (new Integer(prop.getProperty("resultOnPage"))).intValue();

			if (prop.getProperty("authorization") == null)
				authorization = false;
			else
				authorization = (new Boolean(prop.getProperty("authorization"))).booleanValue();
	
			BooleanQuery.setMaxClauseCount(maxClouseCount);
			
			urlRegexp = prop.getProperty("urlRegexp");
			if (urlRegexp == null)
				urlRegexp = url;
			
			urlReplacement = prop.getProperty("urlReplacement");
			if (urlReplacement == null)
				urlReplacement = url;
			
		} catch (FileNotFoundException e) {
			throw e ;
		} catch (ConfProviderException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
		
	}
}