package pl.infovide.SVNInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.log4j.Logger;


/**
 * This class firstly trying to take repository list, from 'directPath' but if nothing was found, then trying from 'repositoryFileList'.
 * @author Michal Walkowski
 *
 */
public class RepositoryList
{
	private Logger logger = Logger.getLogger("svnsearcher");
	/**
	 * Main method to call in this class, to make list with repositories names.
	 * If nothing was found, then try to get repositories names from file 'repoListFile'
	 * @param directory 'directPath' from .properties file. Firstly try to find there repositories names.
	 * @return list with repositories names (from 'directPath' or if not found from 'repoListFile') 
	 * @throws ConfProviderException Syntax error in properties file.
	 * @throws FileNotFoundException Thrown when coulden't find properties file
	 * @throws IOException low level error in reading properties file
	 */
    public ArrayList<String> getDirectAccessRepositories(String directory)  throws  FileNotFoundException, IOException,  ConfProviderException{
    	ArrayList<String> list = new ArrayList<String>();
    	/*
    	 *  This part try to use 'directoryPath' (in local file system)
    	 *  to find place with repositories.
    	 */
    	try{
	        String repname;
	        /* any exception thrown here makes that
	         * control is passed to 'catch' block where
	         * repositories names are getting from 'repoListFile'.
	         */
	        ArrayList<String> repositoriesList =
	            getDirectAccessRepositoriesReq(directory);
	        if(!(repositoriesList.size() > 0)) 
	        	throw new Exception("Empty repositories list, made from 'directPath'");

	        /// if 'directoryPath' exist and it's correctly set. 
	        Collections.sort(repositoriesList);
	        for(int i = 0; i < repositoriesList.size(); i++) {
	        	repname = (((String) repositoriesList.get(i)).replaceFirst(directory, "")).trim();
	        	if((repname.startsWith("/")) || (repname.startsWith("\\")))
	        		repname = repname.substring(1).replace("\\", "/");
	        	else
	        		repname = repname.replace("\\", "/");
	        	list.add( repname.trim() );
	        }
	
	        return list;
	        /*
		     * This part is called when no 'directoryPath' was set in configuration file 
		     * (or was set incorrectly).
		     * Then 'repositoryListFile' is getting to find repository names. 
		     */
    	} catch (Exception e) {
    		
    		ConfigurationProvider prov = new ConfigurationProvider();
    		prov.getConfiguration(prov.getFilename());
    	    BufferedReader input = null;
    	    try {
    	      input = new BufferedReader( new FileReader(prov.getRepListFile()) );
    	      String line = null;
    	      while (( line = input.readLine()) != null)
    	        list.add(line);
    	    } catch (IOException ex){
    	    	throw new  IOException("Coulden't read file with repositories list: "+ex.getMessage() );
    	    }finally{
    	    	try{
    	    		input.close();
    	    	}catch (IOException exx){
    	    		logger.error("Can't close Repository List file "+prov.getRepListFile()+" :"+exx.getMessage());
    	    	}
    	    }
    		return list;
    	}
    }
    /**
     * This method try to get access to directory set as 'directPath'
     * in local file system. If this 'directPath' exists, trying
     * to find there repositories ('format' file and 'db' directory).
     * If 'directPath' doesn't exist, or doesn't have any repositories, an exception 
     * is thrown and method which called it, looking for repositories in 'repoListFile'.
     * @param folder property 'directPath' from properties file. Path to directory
     * in local file system, with repositories.
     * @return if 'folder' is correct path to place with repositories, returns
     * list with repositories names.
     */
    private ArrayList<String> getDirectAccessRepositoriesReq(String folder) {
        File sourceFiles = new File(folder);
        File[] fileList = sourceFiles.listFiles();
        ArrayList<String> result = new ArrayList<String>();
        for (int i = 0; i < fileList.length; i++) {
            if (fileList[i].isDirectory())
                result.addAll(getDirectAccessRepositoriesReq(fileList[i].toString()));
            if (fileList[i].isFile())
                if (fileList[i].toString().endsWith("format"))
                    if (!fileList[i].getParent().toString().endsWith("db"))
                        result.add(fileList[i].getParent().toString());
        }
        return result;
    }
    
}