package pl.infovide.SVNJni;

/**
 * Thrown when problem with load Authorization module occurred
 * @author Bartosz Gasparski
 *
 */
public class AuhtorizationException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public AuhtorizationException() {
		super("AuhtorizationException");
	}
	public AuhtorizationException(String mesg) {
		super(mesg);
	}
}
