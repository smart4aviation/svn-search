package pl.infovide.SVNJni;

import org.apache.log4j.Logger;

/**
 * This class load external library "libAuthorization.so" (written in "C") and also
 * calls method for authorization, for given user, access script and table with paths
 * to files in repositories. 
 * @author Przemyslaw Kleszczewski
 *
 */
public class SVNAuthorization 
{
	private static boolean LOADED;
	/**
	 * Native method in libAuthorization.so
	 * @param user - user who uses SVNSearcher
	 * @param repo - paths to files in repositories in format: "repoName:/path/to/file"
	 * @param access - path to file with authorization information for each user in SVN (default file for SVN is  'repoName/conf/authz' )
	 * @return table which matches for given table with path to files. 
	 */
	public native boolean[] authorize(String user, String[] repo, String access)  ;
	

	/**
	 * Loading library, default this library should be located in : 
	 * '$TOMCAT_HOME/shared/lib/libAuthorization.so' and environment variable during starting Tomcat
	 * should be set 'LD_LIBRARY_PATH=$TOMCAT_HOME/shared/lib/libAuthorization.so'
	 * @throws AuhtorizationException Thrown when problem with load Authorization module occurred
	 */
	public static void loadLib() throws AuhtorizationException{
		if (!LOADED){
			try{
				System.loadLibrary("Authorization");
				LOADED = true;
			}catch (UnsatisfiedLinkError e){
				throw new AuhtorizationException("Error during loading libAuthorization.so: "+e.getMessage());
			}catch(Exception e ){
				throw new AuhtorizationException("Exception during loading libAuthorization.so: "+e.getMessage());
			}
		}
	}
	
	
	/**
	 * This method call native method from library.
	 * @param user user who uses SVNSearcher
	 * @param repo paths to files in repositories in format: "repoName:/path/to/file"
	 * @param access - path to file with authorization information for each user in SVN (default file for SVN is  'repoName/conf/authz' )
	 * @return table which matches for given table with path to files. 
	 */
	public boolean[] makeAuthorization(String user, String[] repo, String access)  {
		
		return authorize(user, repo, access);
	}
}