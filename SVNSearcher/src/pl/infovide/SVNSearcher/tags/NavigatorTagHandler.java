package pl.infovide.SVNSearcher.tags;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.taglibs.standard.tag.el.core.ExpressionUtil;

import pl.infovide.SVNSearcher.SearchResultBean;

/**
 * This class use for makes navigator between result sites.
 *
 */
public class NavigatorTagHandler extends TagSupport {

	private String searchResult;
	
    public int doStartTag() throws JspException
    {
    	try
    	{
    		SearchResultBean result = (SearchResultBean)ExpressionUtil.evalNotNull("navigator", "searchResult", searchResult, 
    				SearchResultBean.class, this, pageContext);
    		
    		 //request.setCharacterEncoding("UTF-8");
    		
    		 if (result.getStart() < result.getResultOnPage()) 
    		 {
    			 if (result.getResultOnPage() < result.getNumberOfAllDocuments()) 
    			 {

    				 pageContext.getOut().print("&lt;&nbsp; Previous");
    			 }
    		 } 
    		 else 
    		 {
    			 pageContext.getOut().print(createLink(result, result.getStart() - result.getResultOnPage(), "<b>&lt;&nbsp; Previous</b>"));
    		 } 
    		 
    		 pageContext.getOut().print("&nbsp;&nbsp;");
    		 
    		
			 int numberOfPages = result.getNumberOfAllDocuments() / result.getResultOnPage();
			    
			 if (result.getNumberOfAllDocuments() % result.getResultOnPage() != 0)
			        numberOfPages++;
			    
			 int whichPage = 1 + (result.getStart() / result.getResultOnPage());
			
			 //int counter = 0;

			int startLinks = whichPage - 4;
			int endLinks = whichPage + 4;
			if (startLinks <= -1)
			    endLinks -= startLinks;

			if (startLinks < 1)
			    startLinks = 1;
			else
			    startLinks++;
			    
			if (endLinks > numberOfPages) {
			    startLinks -= (endLinks - numberOfPages);
			    endLinks = numberOfPages;
			}


			if (result.getPath() != null)
			{
				
			}
    		    
		    if (startLinks < 1)
		        startLinks = 1;
    		        
    		    for (int i = startLinks; i <= endLinks; ++i) {
    		      if (result.getResultOnPage() < result.getNumberOfAllDocuments()) {
    		        if (i == startLinks && startLinks != 1)
    		        {
    		        	pageContext.getOut().print("... ");
    		        }
    		        
    		        if (i == whichPage)
    		        {
    		        	pageContext.getOut().print("<B>" + i + "</B>&nbsp;");
    		        }
    		        else
    				{
    		        	pageContext.getOut().print(
    		        			createLink(result, (i - 1) * result.getResultOnPage(), (new Integer(i)).toString()));
    				}

    		        if (endLinks == i && endLinks != numberOfPages)
    		        	pageContext.getOut().print("...");
    		      }
    		    }

    		    pageContext.getOut().print("&nbsp;&nbsp;");
    		    
    		    if (result.isIfNextElements() == false) 
    		    {
    		    	if (result.getResultOnPage() < result.getNumberOfAllDocuments()) 
    		    	{
    		    		pageContext.getOut().print("Next&nbsp;&gt;");
    		    	}
    		    } 
    		    else 
    		    {
    		    	pageContext.getOut().print(createLink(result, result.getStart() + result.getResult().size(), "<b>Next&nbsp;&gt;</b>"));
    		    }
    		 
    	}
    	catch (Exception e)
    	{
    		throw new JspException(e);
    	}
    	
    	return SKIP_BODY;
    }

	private String createLink(SearchResultBean result, int start, String label) throws UnsupportedEncodingException {
		
		StringBuffer link = new StringBuffer();
		
		link.append("<a class=\"search-nav\" href=\"search?");
		
		link.append("search=");
		link.append(URLEncoder.encode(result.getQueryString(), "UTF-8"));
		link.append("&start=");
		link.append(start);
		link.append("&sort=");
		link.append(result.getSort());
		link.append("&reverse=");
		link.append(result.isReverse());
		
		
		
		/*if (result.getPath() != null)
		{
			link.append("&path=");
			link.append(URLDecoder.decode(result.getPath(), "UTF-8"));
		}
		
		if (result.getFileType() != null)
		{
			link.append("&filetype=");
			link.append(URLDecoder.decode(result.getFileType(), "UTF-8"));
		}*/

		link.append("\">");
		link.append(label);
		link.append("</a>&nbsp;");
		
		return link.toString();
	}

	public void setSearchResult(String searchResult) {
		this.searchResult = searchResult;
	}
    
    
}
