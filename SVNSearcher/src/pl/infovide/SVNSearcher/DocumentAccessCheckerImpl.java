package pl.infovide.SVNSearcher;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import pl.infovide.SVNInfo.ConfProviderException;
import pl.infovide.SVNInfo.ConfigurationProvider;
import pl.infovide.SVNInfo.RepositoryList;


/**
 * This class is currently not using. 
 * If you decide to use it, make sure, that property for 'svn_authz_test' path is correctly set in .properties file,
 * or has some default setting.
 * This implementation use ruby script ('svn_authz_test') for check user rights for given documents.
 * @author Przemyslaw Kleszczewski
 *
 */
public class DocumentAccessCheckerImpl implements DocumentAccessChecker {

	private InputStream pi;
	private String[] repositoryNames;
	private String parentPath;
	private String scriptPath;
	private String access;
	private ConfigurationProvider prov;

	/**
	 * Class constructor. Sets needed information to use external script ('svn_authz_test').
	 */
    public DocumentAccessCheckerImpl(String path) throws  FileNotFoundException, IOException,  ConfProviderException{
    	ArrayList<String> hits;
    	prov = new ConfigurationProvider();
    	prov.getConfiguration(path);
    	parentPath = prov.getUrl();
    	scriptPath = prov.getScriptPath();
    	access = prov.getAccess();
    	RepositoryList list = new RepositoryList();
    	hits = list.getDirectAccessRepositories(prov.getDirectPath());
    	repositoryNames = new String[hits.size()];
    	for(int i = 0; i < hits.size(); i++)
				repositoryNames[i] = new String(hits.get(i));
    }
    
    /**
     * This method parse given url to path in format: 'reposName:/path/to/file'
     * This format id needed for external ruby script ('svn_authz_test') which checks rights to documents.
     * @param url - url to file, before rights to them was checked. 
     * @return return path to file in form: 'reposName:/path/to/file'
     */
    private String parseAdress(String url) {
    	StringBuffer parse = new StringBuffer();
    	String help = url.replaceFirst(parentPath, "");
    	for(int i = 0; i < repositoryNames.length; i++) {
    		if(help.startsWith(repositoryNames[i])) {
    			parse.append(repositoryNames[i].substring(repositoryNames[i].lastIndexOf("/") + 1) + ":" + help.replaceFirst(repositoryNames[i], ""));
    			break;
    		}
    	}
    	return new String(parse);
    }
    
    /**
     * This method analyzes results returns from external ruby script ('svn_authz_test') (checking
     * access for found documents) - and build new list of documents, which access to user has rights
     * @param response - result from external script matches to 'old' list of documents
     * @param old - list of all documents found by Indexer. 
     * @return list for documents available for the user. 
     */
    private ArrayList<SearchResultEntry> authzList(StringBuffer[] response, ArrayList<SearchResultEntry> old) {
    	ArrayList<SearchResultEntry> answer = new ArrayList<SearchResultEntry>();
    	for(int i = 0; i < response.length ; i++)	{
    		if(response[i] != null)
	    		if((new String(response[i]).startsWith("+"))) {
	    			answer.add(old.get(i));
	    		}
    	}
    	return answer;
    }
    /**
     * This is main method for checking access for given user, for each document,
     * and return only these documents, which user has rights to read.
     * This method call external ruby script ('svn_authz_test').
     * @param documents - all documents found by Indexer for given query 
     * @param user - user, who use SVNSearcher
     * @return From 'documents' this method return only these, which 'user' has access to read.
     */
    public ArrayList<SearchResultEntry> checkDocuments(ArrayList<SearchResultEntry> documents, String user) {
    	int odp, i;
    	Process p;
    	String[] command = new String[documents.size() + 5];
    	command[0] = new String(scriptPath + "svn_authz_test");
    	command[1] = new String("-f");
    	command[2] = new String(access);
    	command[3] = new String("-n");
    	if(user != null)
    		command[4] = new String(user);
    	else
    		command[4] = new String("anonymous");
    	if(documents.size() > 0) {
    		StringBuffer[] response = new StringBuffer[documents.size() + 1];
    		response[0] = new StringBuffer();
    		for(i = 5; i < command.length; i++ )
    			command[i] = parseAdress(documents.get(i-5).getFileName()).trim();
    		try {
    			p = Runtime.getRuntime().exec( command );
    			pi = p.getInputStream();
				for(i = 0; (odp = pi.read()) > 0; ) {
					if(((char) odp) == '\n') {
						if(i < response.length)
							response[++i] = new StringBuffer();
					}
					else
						response[i].append((char)odp);
				}
				return authzList(response, documents);
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
        return new ArrayList<SearchResultEntry>();
    }
}
