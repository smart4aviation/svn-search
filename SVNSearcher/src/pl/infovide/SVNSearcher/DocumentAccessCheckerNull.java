package pl.infovide.SVNSearcher;

import java.util.ArrayList;

/**
 * This class is currently not using. 
 * You can use this implementation if you want to not filter any documents.
 * Using this class is equals to disable authorization option
 * (in properties file: authorization=false') 
 */
public class DocumentAccessCheckerNull implements DocumentAccessChecker {
    public DocumentAccessCheckerNull() {
    }
    
    /**
     * This method doesn't check any rights for user.
     */
    public ArrayList<SearchResultEntry> checkDocuments(ArrayList<SearchResultEntry> documents, String user) {
        return documents;
    }
}
