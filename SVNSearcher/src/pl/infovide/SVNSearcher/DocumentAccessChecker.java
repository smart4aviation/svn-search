package pl.infovide.SVNSearcher;

import java.util.ArrayList;

import pl.infovide.SVNJni.AuhtorizationException;

/**
 * This interface check which documents from all found by Indexer,
 * can be read by given user (using SVNSearcher).
 */

public interface DocumentAccessChecker {
	/**
	 * This method requires list with all documents found by searcher, and user who use SVNSearcher.
	 * Return only these documents, from all given, which the user has rights to read in SVN.
	 * @param documents all documents found by searcher for query
	 * @param user user who uses SVNSearcher.
	 * @return from given documents returns only these, which given user can read in SVN.
	 * @throws AuhtorizationException Thrown when problem with load Authorization module occurred
	 */
    public ArrayList<SearchResultEntry> checkDocuments(ArrayList<SearchResultEntry> documents, String user) throws AuhtorizationException;
}
