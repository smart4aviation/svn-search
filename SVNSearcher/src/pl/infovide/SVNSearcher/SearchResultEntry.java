package pl.infovide.SVNSearcher;

import java.text.ParseException;
import java.util.Date;

import javax.swing.Scrollable;



import org.apache.log4j.Logger;
import org.apache.lucene.document.DateTools;


/**
 * Class stores one item from searching
 */
public class SearchResultEntry {
    private Logger logger = Logger.getLogger("svnsearcher");
    private String fileName;    /**< Path to file */
    private String boldName;    /**< File name */
    private String fileBody;    /**< Text content of file */
    private String author;		/**< Author of last commit*/
    private String comment;		/**< Comment to last commit*/
    private Date lastModified;	/**< Date of last commit */
    private String type;		/**< File extension */
    private float hitScore;     /**< soundness of found document */
    int number;					/**< number in result list*/
    private String shortFileName; /**< Short file name */
    
    public SearchResultEntry(){
    	
    }
    public SearchResultEntry(String s, int i){
    	fileName = s;
    	number = i;
    }
    
    public void setNumber(int number) {
		this.number = number;
	}
    
    public int getNumber() {
	   return number;
   	} 
    
    public String getBoldName() {
    	if (boldName != null)
    		return boldName;
    	return "";
	}
    
    public void setBoldName(String boldName) {
		this.boldName = boldName;
	}
    
    public String getAuthor() {

    	return author;
	}
    
    public String getUnAuthor() {
    	if (author != null)
    		return author.replaceAll("<b>", "").replaceAll("</b>", "");
    	return author;
    }
    
    public void setAuthor(String author) {
		this.author = author;
	}
    
    public String getComment() {
    	 if (comment != null) 
    		return comment.replaceAll("\n", "<BR>");
    	return comment; 	
	}
    
    public void setComment(String comment) {
		this.comment = comment;
	}
    
    public Date getLastModified() {
    	if (lastModified == null)
    		logger.error("Null is getting for 'last modification date', file: "+fileName);
    	return lastModified;
	}
    
    public void setLastModified(String date) {
    	try {
    		if (date == null ){
    			logger.error("Null is setting for 'last modification date', file: "+fileName);
    			lastModified = null;
    		}
    		else 
    			lastModified = DateTools.stringToDate(date);
		} catch (ParseException e) {
			logger.error("Coulden't parse date: \""+date+"\", reason:\n"+e.getMessage());
			//e.printStackTrace();
		}
	}
    
    public String getFileName() {
        return fileName;
    }
    
    public String getFileBody() {
    		return fileBody;
    }
    
    public float getHitScore() {
       float temp = hitScore * 10000;
       int a  = (int) temp;
       temp = a;
       return temp/100;
     
    }
    
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public void setFileBody(String fileBody) {
        this.fileBody = fileBody;
    }
    
    public void setHitScore(float hitScore) {
        this.hitScore = hitScore;
    }

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getShortFileName() {
		return shortFileName;
	}

	public void setShortFileName(String shortFileName) {
		this.shortFileName = shortFileName;
	}
}
