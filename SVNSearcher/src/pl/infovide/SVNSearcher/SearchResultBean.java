package pl.infovide.SVNSearcher;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class type "bean" to store results of one operation of searching index.
 * All fields stored it this class are parsed by other class to present in browser.
 */
public class SearchResultBean implements Serializable {
    private ArrayList<SearchResultEntry> result;    /**< Storing documents found in index. */
    private int start = 0;                      /**< Number of item which starts 'result' in this object */
    private String queryString;                 /**< Query content - final query content with '+Name' or  'Type' */
    private boolean ifNextElements;             /**< Information about exists next items on next pages */
    private int numberOfAllDocuments;           /**< Amount of all documents which user can read*/
    private int resultOnPage = 10;                   /**< Amount of position showed per on page */
    private int numberOfFoundDocuments;			/**< Amount of all found in index documents (before authorization) */
    private String path;						/**< Path - extra information to query -  which described string which must exists in document's file name  */
    private String fileType;					/**< File type - extra information to query - which described extension documents to find*/
    private String sort;					/**< filed's name which is using to sort results*/
    private boolean reverse;				/**< to sort in order or contrary*/
    private String simplePhrase;			/** < Field 'phrase' from Advanced Searching site, or phrase in "'(' ')'" in other situation  */
    private String error;				/** <Error message displayed on web site, when user constructs incorrect query> */
    private float searchTime;
    private float allTime;
    /**
     * Class constructor. Initialize new table for storing results of searching
     * 
     */ 
    public SearchResultBean() {
        result = new ArrayList<SearchResultEntry>();
    }
    
    /**
     * Getting amount of all found documents (before authorization)
     * @return  amount of all found documents (before authorization)
     */
    public int getNumberOfFoundDocuments() {
		return numberOfFoundDocuments;
	}
    
    /**
     * Setting amount of all found documents (before authorization)
     * @param numberOfFoundDocuments
     */
    public void setNumberOfFoundDocuments(int numberOfFoundDocuments) {
		this.numberOfFoundDocuments = numberOfFoundDocuments;
	}
    
    /**
     * Sets table storing results of searching
     * @param result New table with results of searching index
     */
    public void setResult(ArrayList<SearchResultEntry> result) {
        this.result = result;
    }

    /**
     * Gets table storing results of one searching
     * @return Returns table storing results of one searching
     */
    public ArrayList<SearchResultEntry> getResult() {
        return result;
    }
    
    /**
     * Set number of item which starts results. 
     * @param start New starting item number
     */
    public void setStart(int start) {
        this.start = start;
    }
    
    /**
     * Get number of item which starts results. 
     * @return number of item which start this results
     */
    public int getStart() {
        return start;
    }
    
    /**
     * Method sets new query value
     * @param queryString new query value
     */
    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }
    
    /**
     * Method gets query value
     * @return query value
     */
    public String getQueryString() {
    	if (queryString == null)
    		return "";
    	 
        return queryString.replaceAll("\"", "&quot;");
    }
    
    /**
     * Method sets information about exists next items on next pages. 
     * @param ifNextElements new information about exists next items on next pages.
     */
    public void setIfNextElements(boolean ifNextElements) {
        this.ifNextElements = ifNextElements;
    }
    
    /**
     * Method gets information about exists next items on next pages. 
     * @return information about exists next items on next pages. 
     */
    public boolean isIfNextElements() {
        return ifNextElements;
    }
    
    /**
     * Sets amount of all documents which user can read
     * @param numberOfAllDocuments new amount of all documents which user can read
     */
    public void setNumberOfAllDocuments(int numberOfAllDocuments) {
        this.numberOfAllDocuments = numberOfAllDocuments;
    }
    
    /**
     * Gets amount of all documents which user can read
     * @return amount of all documents which user can read
     */
    public int getNumberOfAllDocuments() {
        return numberOfAllDocuments;
    }
    
    /**
     * Sets amount of position showed per on page
     * @param resultOnPage new amount of position showed per on page
     */
    public void setResultOnPage(int resultOnPage) {
        this.resultOnPage = resultOnPage;
    }
    
    /**
     * Gets amount of position showed per on page
     * @return amount of position showed per on page
     */
    public int getResultOnPage() {
        return resultOnPage;
    }
    /**
     * Gets path which described string which must exists in document's file name
     * @return path which described string which must exists in document's file name
     */
	public String getPath() {
		return path;
	}
	/**
	 * Sets path which described string which must exists in document's file name
	 * @param path which described string which must exists in document's file name
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * Gets file type which described extension documents to find
	 * @return file type which described extension documents to find
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * Sets file type which described extension documents to find
	 * @param file type which described extension documents to find
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
	/**
	 * Returns field name which is defined by user on web site.
	 * If no field name was set, default "DDate" is returned.
	 * @return  Returns field name which is defined by user on web site. If no field name was set, default "DDate" is returned.
	 */
	public String getSort() {
		if (sort == null)
			return "DDate";
		return sort;
	}

	public void setReverse(boolean reverse) {
		this.reverse = reverse;
	}

	public boolean isReverse() {
		return reverse;
	}

	public void setSimplePhrase(String simplePhrase) {
		this.simplePhrase = simplePhrase;
	}

	public String getSimplePhrase() {
		if (simplePhrase != null)
			return simplePhrase.replaceAll("\"", "&quot;");;
		return "";
	}

	/**
	 * Set error message displayed on web site, when user construct incorrect query 
	 * @param error Message from thrown exceptions. 
	 */
	public void setError(String error) {
		error = error.replaceAll("<", "&lt;");
		error = error.replaceAll(">", "&gt;");
		error = error.replaceAll("\n", "<BR>");
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public void setSearchTime(float searchTime) {
		this.searchTime = searchTime;
	}

	public float getSearchTime() {
		return searchTime;
	}

	public void setAllTime(float allTime) {
		this.allTime = allTime;
	}

	public float getAllTime() {
		return allTime;
	}

}
