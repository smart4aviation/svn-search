package pl.infovide.SVNSearcher;

import java.io.IOException;
import java.io.PrintWriter;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.lucene.search.BooleanQuery;

import java.util.Date;
import pl.infovide.SVNInfo.ConfigurationProvider;

/**
 * Class works as a servlet. It calls method for search index, to find
 * suitable positions.
 */
public class Searcher extends HttpServlet {
	public static final long serialVersionUID = 1L;
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8"; /**< "Content-type" response. */
    private static Searcher INSTANCE;
    private IndexSearcher searcher;     /**< Object searching in index.> */
    private int resultOnPage;           /**< Number of items showed per one page.> */
	private String urlRegexp;	/** < Base url in file path, which will be replaced by 'urlReplacement' >*/
	private String urlReplacement; /** < Url for replace 'urlRegexp' > */
	private SearchResultBean searchResultBean;
    
    private static Logger logger = Logger.getLogger("svnsearcher");
    
    /**
     * Method calls during initialization of servlet.
     * @param config Servlet configuration
     * @throws ServletException throws during error in initializing servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
        /// @li Reading configuration file
        try {
            String path = config.getInitParameter("confPath");
            ConfigurationProvider.setFilename(path);
            ConfigurationProvider prov = new ConfigurationProvider();
            prov.getConfiguration(path);
            
            String indexDirectory = prov.getIndexPath();
            resultOnPage = prov.getResultOnPage();
            urlRegexp = prov.getUrlRegexp();
            urlReplacement = prov.getUrlReplacement();

            /// @li Creating objet to search index
            searcher = new IndexSearcher(indexDirectory, path);
            INSTANCE = this;
        } catch (Exception e) {
        	logger.fatal("Servlet not initilize correctly: "+e.getMessage());
        	throw new ServletException("Servlet not initilize correctly: "+e.getMessage());

        }
    }
    
    /**
     * This is main method, it makes all procedure of searching index and parse results.
     * It's call during send to servlet "GET" method. 
     * @param request Request send to servlet
     * @param response Servlet response
     * @throws ServletException Exception during occur error.
     * @throws IOException Exception during occur error - when coulden't send redirect on other page.
     */
    @SuppressWarnings("unchecked")
	public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, IOException {
        
    	setSearchResultBean(new SearchResultBean());
    	long before = System.currentTimeMillis();
    	// wrong access 
    	if (request == null || request.getParameterMap().size() < 2 ||  request.getParameter("search") == null || request.getParameter("start") == null ){
        	logger.fatal("Searcher can be accessed only from SVNSearcher web sites" );
			getSearchResultBean().setError("Searcher can be accessed only from SVNSearcher web sites");
			setRBeanWhenError( getSearchResultBean(),  "",  "",  resultOnPage,  0 );
        	request.getSession().setAttribute("searchResult", getSearchResultBean());
            response.sendRedirect("searchResult.jsp");            
            return;
        }
    	
    	request.setCharacterEncoding("UTF-8");
        //response.setCharacterEncoding("UTF-8");
   	
    	boolean reverse = false ;
    	if (request.getParameter("reverse") != null )
    		reverse= (Boolean.valueOf(request.getParameter("reverse")));
    	String sort ="DDate";
    	if (request.getParameter("sort") != null && ! (request.getParameter("sort").equals("")))
    		sort = request.getParameter("sort");
    	
    	getSearchResultBean().setSort(sort);
    	getSearchResultBean().setReverse(reverse);
    	
        PrintWriter out = response.getWriter();
        
        response.setContentType(CONTENT_TYPE);
        
        logger.info("USER INFO: \n"+
        		"Remote user: " + request.getRemoteUser()+"\n"+
        		"User principal: " + request.getUserPrincipal()+"\n"+
        		"Auth type: " + request.getAuthType());

        int start = computeStart(request.getParameter("start"));
        /// @li Getting query content
        String query = null;
        //
        
        try {
        	query = URLDecoder.decode(request.getParameter("search"), "UTF-8");
        } catch(IllegalArgumentException e){
        	logger.fatal("Incorrect query syntax: "+e.getMessage() );
			getSearchResultBean().setError("Incorrect query syntax: "+e.getMessage());
			setRBeanWhenError( getSearchResultBean(),  "",  "",  resultOnPage,  start );
        	request.getSession().setAttribute("searchResult", getSearchResultBean());
            response.sendRedirect("searchResult.jsp");            
            out.close();
            return;
        }

        StringBuffer finallQuery = null;
        /*Creating final query - (from advanced site) from all advanced fields   */
        if (query != null)
        	finallQuery = constructQueryString(request, getSearchResultBean()); 
        
        /// Back to main page when query is empty
        if (finallQuery.toString() == null || finallQuery.toString().equals("") || finallQuery.toString().trim().equals("") ) {
            response.sendRedirect("./");
            out.close();
            return;
        }
        /*Getting phrase from all query */
        String simpleQuery = makePhraseFromQuery(query);
       
        Object[] res = null;
        /// @li Searching index
        try {
            res = searcher.search(finallQuery.toString(), start, resultOnPage, request.getRemoteUser(), sort, reverse);
        }catch (OutOfMemoryError e){
        	//e.printStackTrace();
        	getSearchResultBean().setError("Server isn't prepared for that amount of results.\n Try query which gives less results. More info in logs.");
        	logger.error("Server isn't prepared for that amount of results.\n Try query which gives less results or increase JVM memory.\n "+e.getMessage());
        	setRBeanWhenError( getSearchResultBean(),  query,  simpleQuery,  resultOnPage,  start );
        	request.getSession().setAttribute("searchResult", getSearchResultBean());
        	response.sendRedirect("searchResult.jsp");            
        	out.close();
        	return;
        }
        catch (BooleanQuery.TooManyClauses e) {
        	logger.error("SVN Searcher configuration isn't prepared for this amount of clauses.\n "+
        			"Increas \'maxClouseCount\' property in your properties file");
        	getSearchResultBean().setError("SVN Searcher configuration isn't prepared for this amount of clauses.\n "+
        			"Increas \'maxClouseCount\' property in your properties file");
        	setRBeanWhenError( getSearchResultBean(),  query,  simpleQuery,  resultOnPage,  start );
        	request.getSession().setAttribute("searchResult", getSearchResultBean());
            response.sendRedirect("searchResult.jsp");            
            out.close();
            return;
        }catch (RuntimeException e){
	        logger.error("Please use other criterion of sort, this one is not served in this index.\n "+e.getMessage());
	        getSearchResultBean().setError("Please use other criterion of sort, this one is not served in this index.\n "+e.getMessage());
	        setRBeanWhenError( getSearchResultBean(),  query,  simpleQuery,  resultOnPage,  start );
	        request.getSession().setAttribute("searchResult", getSearchResultBean());
	        response.sendRedirect("searchResult.jsp");            
	        out.close();
	        return;
	        	
        }  catch (IndexSearcherParseException e) {
        	getSearchResultBean().setError("Error during parsing query: "+e.getMessage());
        	logger.fatal("Error during parsing query: "+e.getMessage());
        	setRBeanWhenError( getSearchResultBean(),  query,  simpleQuery,  resultOnPage,  start );
            request.getSession().setAttribute("searchResult", getSearchResultBean());
            response.sendRedirect("searchResult.jsp");            
            out.close();
            return;
        // all other exceptions are logged only for application administrator. SVNSearcher user see only one type of message.
        }catch (Exception e) {
        	getSearchResultBean().setError("SVNSearcher isn't configured correctly. Please contact with administrator - more details in SVNSearcher logs.  ");
        	logger.fatal(e.getMessage());
        	setRBeanWhenError( getSearchResultBean(),  query,  simpleQuery,  resultOnPage,  start );          
        	request.getSession().setAttribute("searchResult", getSearchResultBean());
            response.sendRedirect("searchResult.jsp");            
            out.close();
            return ;     
        }
           
        ArrayList<pl.infovide.SVNSearcher.SearchResultEntry> result = (ArrayList<pl.infovide.SVNSearcher.SearchResultEntry>)res[0];
        Boolean ifIsNextElements = (Boolean)res[1];
        Integer numberOfDocuments = (Integer)res[2];
        Integer numberOfFoundDocuments = (Integer)res[4];
        //Float searchTime = (Float)res[3];

        //cleaning special characters
        	String[]  tmpQuery = simpleQuery.split(" ");
        	ArrayList<String> withNoSpecial = new ArrayList<String>();
        	for (String string : tmpQuery) {
        		string = string.replaceAll(" ", "");
        		string = string.replaceAll("\\{", "");
        		string = string.replaceAll("\\}", "");
        		string = string.replaceAll("\\[", "");
        		string = string.replaceAll("\\]", "");
        			
				if ( string.length() != 0)
					withNoSpecial.add(string);
			}
        
        // end cleaning
        	String [] splitedQuery = new String[withNoSpecial.size()];
        	splitedQuery =  withNoSpecial.toArray(splitedQuery);
        
        ArrayList<String> boldStrings = new ArrayList<String>();
        
        boolean inQuote = false;
        String boldString = "";
        boolean ifMinusChar = false;
        
        logger.debug("User query analisis");
        
        //for (int j = 0; j < splitedQuery.length; j++) {
       // 	logger.debug("-> Splitted query (" + j + "):" + splitedQuery[j]);
		//}

        /// @li Analyze user query     
        // eg: +Name:/clients/lafarge +Type:XLS +(lafarge)
        for (int i = 0; i < splitedQuery.length; ++i) {
        	
        	if (splitedQuery[i] == null || splitedQuery[i].length() == 0  || splitedQuery[i].equals(""))
        	{
        		continue;
        	}
        	if( splitedQuery[i].equals("\t") || splitedQuery[i].equals("\n") || splitedQuery[i].equals(" ") || splitedQuery[i].equals("\r") || splitedQuery[i].equals("\b") || splitedQuery[i].equals("\f"))
        		continue;
        
          splitedQuery[i] = splitedQuery[i].trim();

        	if (inQuote == false && (
        			splitedQuery[i].equals("AND") == true ||
        			splitedQuery[i].equals("OR") == true ||
        			splitedQuery[i].equals("NOT") == true ||
        			splitedQuery[i].equals("TO") == true )
        		)
                    continue;
            
            logger.debug("Splitted query (" + i + "):" + splitedQuery[i]);
            
            if (splitedQuery[i].length() >= 3) 
            {
                if (splitedQuery[i].charAt(0) == '\"' && (splitedQuery[i].charAt(splitedQuery[i].length() - 1) == '\"')) 
                {
                    boldStrings.add(splitedQuery[i].substring(1, splitedQuery[i].length() - 1));
                    ifMinusChar = false;
                    continue;
                }
            }
            
            if (inQuote == false) 
            {
                if (splitedQuery[i].charAt(0) == '-') 
                {
                    ifMinusChar = true;
                    splitedQuery[i] = splitedQuery[i].replace('-', ' ').trim();
                    continue;
                }
                else if (splitedQuery[i].charAt(0) == '+') 
                {
                    ifMinusChar = false;
                    splitedQuery[i] = splitedQuery[i].replace('+', ' ').trim();
                    continue;
                }
            }
            
            if (splitedQuery[i].charAt(0) == '\"') {
                inQuote = true;
                boldString = splitedQuery[i].replace('\"', ' ').trim();
                continue;
            }
            if (splitedQuery[i].indexOf(':') >= 0) {
            	if(!(splitedQuery[i].indexOf("\\:") >= 0)){
	                inQuote = true;
	                boldString = splitedQuery[i].substring(splitedQuery[i].indexOf(':'), splitedQuery[i].length());
	                continue;
            	}
            }
            if (splitedQuery[i].charAt(splitedQuery[i].length() - 1) == '\"') {
                inQuote = false;
                if (boldString.length() > 0)
                    if (boldString.charAt(boldString.length() - 1) != ' ')
                        boldString += " ";
                
                boldString += splitedQuery[i].substring(0, splitedQuery[i].length() - 1);
                
                if (ifMinusChar == false)
                    boldStrings.add(boldString);
                ifMinusChar = false;
                continue;
            }
            
            if (inQuote == true) {
                boldString += " " + splitedQuery[i] + " ";
                continue;
            }
            
            if (splitedQuery[i].length() != 0 && splitedQuery[i].charAt(0) == '(')
                splitedQuery[i] = splitedQuery[i].substring(1, splitedQuery[i].length());

            if (splitedQuery[i].length() != 0 && splitedQuery[i].charAt(splitedQuery[i].length() - 1) == ')')
                splitedQuery[i] = splitedQuery[i].substring(0, splitedQuery[i].length() - 1);
            
            if (splitedQuery[i].length() != 0 && splitedQuery[i].charAt(0) == '[')
                splitedQuery[i] = splitedQuery[i].substring(1, splitedQuery[i].length());

            if (splitedQuery[i].length() != 0 && splitedQuery[i].charAt(splitedQuery[i].length() - 1) == ']')
                splitedQuery[i] = splitedQuery[i].substring(0, splitedQuery[i].length() - 1);
            
            if (splitedQuery[i].length() != 0 && splitedQuery[i].charAt(0) == '{')
                splitedQuery[i] = splitedQuery[i].substring(1, splitedQuery[i].length());

            if (splitedQuery[i].length() != 0 && splitedQuery[i].charAt(splitedQuery[i].length() - 1) == '}')
                splitedQuery[i] = splitedQuery[i].substring(0, splitedQuery[i].length() - 1);
            
            if (ifMinusChar == false)
                boldStrings.add(splitedQuery[i]);
                                    
            ifMinusChar = false;
        }
        
        logger.debug("Documents content shrinking");
        /// this variable decide to put '...' on the end of each file body, or not.
        boolean isEndOfBody = false;
        /// @li Documents content shrinking
        for (int i = 0; i < result.size(); ++i) {
            isEndOfBody = false;
            SearchResultEntry entry = result.get(i);
            String fileBody = entry.getFileBody();
            
            String name = entry.getFileName().replaceAll(urlRegexp, urlReplacement);
            entry.setFileName(name);
            
            int from = 0, to = 0;
            if(fileBody != null){
            	Pattern pattern = null;
            	Matcher matcher = null;
            	if(boldStrings.size() > 0){
            		try {
            			pattern = Pattern.compile("(?i)\\b" + boldStrings.get(0) + "\\b");
            			matcher = pattern.matcher(fileBody);
            		}catch (PatternSyntaxException e){
            			logger.error("Incorrect query syntax: "+e.getMessage() );
            			getSearchResultBean().setError("Incorrect query syntax: "+e.getMessage());
            		}
		            if (pattern != null && matcher != null && matcher.find() == true) {
		                if (matcher.start() + 300/2 >= fileBody.length()){
		                    to = fileBody.length();
		                    isEndOfBody = true;
		                }
		                else
		                    to = matcher.start() + 300/2;  
		                if (matcher.start() - 300/2 <= 0){
		                    from = 0;
		                    if (  (to+300/2 - matcher.start()) >= fileBody.length() ) {
		                    		to = fileBody.length();
		                    		isEndOfBody = true;
		                    } else{
		                    	to += 300/2 - matcher.start();
		                    }
		                }
		                else {
		                	from = matcher.start() - 300/2;
		                	if(matcher.start() + 300/2 >= fileBody.length()){
		                		from -= 300/2 - ( fileBody.length() - matcher.start());
		                		if (from < 0)
		                			from = 0;
		                	}
		                }
		            } else {
		                from = 0;
		                if (fileBody.length() <= 600/2){
		                	to = fileBody.length();
		                	isEndOfBody = true;
		                }
		                else
		                	to = 600/2;
		            }
            	} else {
            		if(300 <= fileBody.length())
            			to = 300;
            		else {
            			to = fileBody.length();
            			isEndOfBody = true;
            		}
            	}
	
	            fileBody = fileBody.substring(from, to);
	            fileBody = fileBody.replaceAll("&", "&amp;");
	            fileBody = fileBody.replaceAll("<", "&lt;");
	            fileBody = fileBody.replaceAll(">", "&gt;");
	            fileBody = fileBody.replaceAll("\"", "&quot;");
	            fileBody = fileBody.replaceAll("'", "&apos;");
	        
	 
     
	// @INFO: this part of code, can be not optimize.
	            /// @li Bolder for key word.
	            for (int ii = 0; ii < boldStrings.size(); ++ii) {            
	                StringBuffer temp = new StringBuffer(fileBody.length());
	                
	                try {
	                    pattern = Pattern.compile("(?i)\\b" + boldStrings.get(ii) + "\\b");
	                } catch (PatternSyntaxException e) {
	                    continue;
	                }
	                matcher = pattern.matcher(fileBody);
	                
	                int start2 = 0;
	                while(true) {
	                    if (matcher.find() == false) {
	                        if (start2 != fileBody.length())
	                            temp.append(fileBody.substring(start2, fileBody.length()));
	                        start2 = fileBody.length();
	                        break;
	                    }
	                    
	                    temp.append(fileBody.substring(start2, matcher.start()));
	                    temp.append("<b>" + fileBody.substring(matcher.start(), matcher.end()) + "</b>");
	                    start2 = matcher.end();   
	                }
	                
	                fileBody = temp.toString();
	            }
	            
	            /*
	        	 * Start - Making endLines in equal intervals ('lineLenght') 
	        	*/
	        	 fileBody = fileBody.replaceAll("\n", " ");
	        	 fileBody = fileBody.replaceAll("([ \t\n\r\f]*<BR>)+", "");
	        	          
	        	 int lineLenght = 70;
	        	 for (; lineLenght < fileBody.length(); lineLenght+=70){
	        	        	 
	        	        	 /// how long can be word for find space after them at the end of each line.
	        	        	 int maxWordLength = 20;
	        	        	 int li2 = 0;
	        	        	/// looking for space - line should be ended when space or ';' or ','
	        	        	/// but if 'maxWordLength' is shorter than iterator on last word in line, then the word is ended 
	        	        	/// with no space on the end.
	        	        	for (; li2 < maxWordLength; li2++){		
	        	        		if (fileBody.length() > (li2+lineLenght+1) ) {
	        	        			char checkedChar = fileBody.charAt(li2+ lineLenght);
	        	        			if ( checkedChar == ' ' || checkedChar == ';' || checkedChar== ',' ) {
	        	        				lineLenght = lineLenght + li2;
	        	        				break;
	        	        			}
	        	        		}
	        	        	}
	        	        	 String beforeString = fileBody.substring(0, lineLenght);
	        	        	 String afterString = fileBody.substring(lineLenght);
	        	        	 fileBody = beforeString + "<BR>"+afterString;
	        	 }
	        	/*
	        	 * End
	        	 */             
	            
	           if (! isEndOfBody){
	            	fileBody = fileBody + "...";
	            }
	            entry.setFileBody(fileBody);
	        }
            else {
            	entry.setFileBody("");
            }
            String pom = name;
            pom = pom.toLowerCase();
            String auth = entry.getAuthor();
            if (auth != null)
            	auth = auth.toLowerCase();
            entry.setBoldName(name);
            for (int ii = 0; ii < boldStrings.size(); ++ii){
            	String pom2 = boldStrings.get(ii).toLowerCase();
                if(pom.lastIndexOf(pom2) > 0)
                	entry.setBoldName(name.substring(0, pom.lastIndexOf(pom2))+"<b>"+name.substring(pom.lastIndexOf(pom2),pom.lastIndexOf(pom2) + pom2.length()) + "</b>" + name.substring((pom.lastIndexOf(pom2) + pom2.length())));
                pom2 = boldStrings.get(ii).toLowerCase();
                if(auth != null && auth.equals(pom2))
                	entry.setAuthor("<b>" + entry.getAuthor() + "</b>");
                
            }
        }
        /// @li Creating Bean with all results 
        if (res[4] != null)
        	getSearchResultBean().setSearchTime((Float)res[3]);
        long after = System.currentTimeMillis();
        getSearchResultBean().setAllTime(new Float((float)(after - before) / 1000f))  ;
        getSearchResultBean().setSimplePhrase(simpleQuery);
        getSearchResultBean().setResult(result);       
        getSearchResultBean().setStart(start);
        getSearchResultBean().setResultOnPage(resultOnPage);
        getSearchResultBean().setIfNextElements(ifIsNextElements);
        getSearchResultBean().setNumberOfAllDocuments(numberOfDocuments.intValue());
        getSearchResultBean().setNumberOfFoundDocuments(numberOfFoundDocuments.intValue());
        /// @li Adding bean to session.
        request.getSession().setAttribute("searchResult", getSearchResultBean());
        /// @li Send redirect to user's browser.
        logger.debug("Redirect to searchResult.jsp");
        response.sendRedirect("searchResult.jsp");

        out.close();
    }

    /**
     * Simple method to make integer from String
     * @param startString parsing String
     * @return integer parsed from given String
     */
	private int computeStart(String startString) {
		int start = 0;
        
        try 
        {
            start = Integer.parseInt(startString);
        } catch (NumberFormatException e) 
        {
            start = 0;
        }
		return start;
	}
    /**
     * Method called during send to servlet "POST" method.
     * It brings control to 'doGet' method
     * @param request Request send to servlet
     * @param response Servlet response
     * @throws ServletException Exception during occur error.
     * @throws IOException Exception during occur error.
     */
    public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        
        doGet(request, response);
    }
    
    /**
     * This method checks if 'query' comes from Advanced Searching site.
     * If yes and advanced options was used, then add to query special characters ("+Name" or "+Type" and put 
     * 'query phrase' into '(' ')' ).
     * Result of this parsing is written as 'QueryString' in result bean.
     * @param request Request for the servlet
     * @param searchResultBean Bean which stores result and information about one searching process
     * @return Final query which is parsed query if request was from AdvancedSearching site, or returns 'search' field if there was no advanced option used.
     */
    private StringBuffer constructQueryString(HttpServletRequest request, SearchResultBean searchResultBean)
    {
    	StringBuffer finallQuery = new StringBuffer();

        String query = request.getParameter("search");
        String path = request.getParameter("path");
        String fileType = request.getParameter("filetype");
        String date = request.getParameter("date");
        String fileBody = request.getParameter("filebody");
   /*     
        if (query != null && !(query.equals("")))
        logger.info("Query: \"" + query + "\"");  
        if (path != null && !(path.equals("")))
        	logger.info("Path: \"" + path + "\"");
        if (fileType != null && !(fileType.equals("")))
        	logger.info("FileType: \"" + fileType + "\"");
    */
        boolean advancedSearch = false;
        
        if(fileBody != null && fileBody.equals("true"))
        	advancedSearch = true;
        
        /*
         * Parsing Date to format : +Date[yyyymmdd TO yyymmdd]
         */
        if(date != null && !date.equals("") && !date.equals("ALL")){
        	Date today = new Date(System.currentTimeMillis());
        	java.text.SimpleDateFormat todaySimple = new java.text.SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z" );
        	String todayS = todaySimple.format(today);
        	String[] tableToday = todayS.split(" ");
       	
        	int y =Integer.parseInt( tableToday[0].substring(0,4) );
        	int m = Integer.parseInt( tableToday[0].substring(5,7) );
        	int d = Integer.parseInt( tableToday[0].substring(8,10) );
        	
        	int yFrom = y;
        	int mFrom = m;
        	
        		if (date.equals("1MON")){
        			mFrom = m - 1;
        			if (mFrom <= 0){
        				mFrom = 12 + mFrom;
        				yFrom -= 1;
        			}
        		}
        		else if (date.equals("3MON")){
        			mFrom = m - 3;
        			if (mFrom <= 0){
        				mFrom = 12 + mFrom;
        				yFrom -= 1;
        			}	
        		}
        		else if (date.equals("6MON")){
        			mFrom = m - 6;
        			if (mFrom <= 0){
        				mFrom = 12 + mFrom;
        				yFrom -= 1;
        			}	
        		}
        		else if (date.equals("12MON")){
        			mFrom = m - 12;
        			if (mFrom <= 0){
        				mFrom = 12 + mFrom;
        				yFrom -= 1;
        			}
        		}
        		String dToday = (""+d).length() <2 ? ("0"+d) : (""+d); 
        		String mToday = (""+m).length() <2 ? ("0"+m) : (""+m); 
        		String mFromS = (""+mFrom).length() <2 ? ("0"+mFrom) : (""+mFrom); 
        		
        		
        		String fromString = ("" +yFrom) + mFromS + dToday  ;
        		String todayString = ("" +y) + mToday + dToday ;
            	finallQuery.append(" +Date:[" + fromString+" TO "+todayString+"] ");
            	advancedSearch = true;
        }
        
        if ((path != null) && !((path.trim()).equals("")))
        {	path = path.trim();
        	finallQuery.append(" +Name:" + path);
        	advancedSearch = true;
        	searchResultBean.setPath(path);
        }
        else
        {
        	/* Cleanup empty string from parameter */
        	searchResultBean.setPath(null);
        }
        
        if ((fileType != null) && (!fileType.equals("")))
        {
        	if (!fileType.equals("ALL"))
        	{
            	finallQuery.append(" +Type:" + fileType);
            	advancedSearch = true;
            	searchResultBean.setFileType(fileType);
        	}
        }
        else
        {
        	/* Cleanup empty string from parameter */
        	searchResultBean.setFileType(null);
        }
        
        if (advancedSearch && query != null && ! ( query.trim().equals("")))
        {
        	if (fileBody != null && fileBody.equals("true") ){
        		finallQuery.append(" +FileBody:(" + query + ")");
        	} else
        		finallQuery.append(" +(" + query + ")");        	
        }
        else
        {
        	finallQuery.append(query);
        }
        
        searchResultBean.setQueryString(finallQuery.toString().trim());
        
        logger.info("FinallQuery: \"" + finallQuery.toString() + "\"");
    	
        return finallQuery;
    }
    
    /**
     * This method parse 'phrase' (equals to text from 'search' field in web site form) from given query.
     * This 'phrase' is needed for field 'search' in AdvancedSearching site and for find (and bold) key words
     * in found files.
     * @param search Text from 'search' field in web site form.
     * @return If there was no advanced options used, then returns given 'search'. In other was takes 'phrase' between '(' and ')'.
     */
    private String makePhraseFromQuery(String search){
    	int lstart = search.indexOf("(");
    	int lend = search.lastIndexOf(")");
    	String simplePhrase ="";
    	if (lstart!= -1 && lend != -1 && lstart<=lend){
    		simplePhrase = search.substring(lstart+1, lend);
    	}
    	else {
    		/*String[] allQuery = search.split(" ");
    		for (String string : allQuery) {
				int ind = string.indexOf(':');
				if (ind == -1){
					if (simplePhrase.length() != 0)
						simplePhrase += (" "+ string);
					else
						simplePhrase += string;
				}
			} */
    		simplePhrase = search;
    	}
    	logger.info("Phrase: \"" + simplePhrase + "\"");
    	return simplePhrase;
    }
    
    /**
     * This method makes procedure setting needed information
     * for result Bean, when error occurred.
     * @param searchResultBean Result bean
     * @param query Final query 
     * @param simpleQuery Phrase 
     * @param resultOnPage 
     * @param start
     */
    private void setRBeanWhenError(SearchResultBean searchResultBean, String query, String simpleQuery, int resultOnPage, int start ){
    	searchResultBean.setSimplePhrase(simpleQuery);
        searchResultBean.setQueryString(query);
        searchResultBean.setStart(start);
        searchResultBean.setResultOnPage(resultOnPage);
        searchResultBean.setIfNextElements(false);
        searchResultBean.setNumberOfAllDocuments(0);
        searchResultBean.setNumberOfFoundDocuments(0);	
    }

	public static void setErrorInBean(String error){
		Searcher.INSTANCE.getSearchResultBean().setError(error);
		logger.error(error+"You can run JVM with more memory to serve this count of documents.");
	}

	private void setSearchResultBean(SearchResultBean searchResultBean) {
		this.searchResultBean = searchResultBean;
	}

	private SearchResultBean getSearchResultBean() {
		return searchResultBean;
	}
}
