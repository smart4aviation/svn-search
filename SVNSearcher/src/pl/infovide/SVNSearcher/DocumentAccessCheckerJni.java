package pl.infovide.SVNSearcher;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import pl.infovide.SVNInfo.ConfProviderException;
import pl.infovide.SVNInfo.ConfigurationProvider;
import pl.infovide.SVNInfo.RepositoryList;
import pl.infovide.SVNJni.AuhtorizationException;
import pl.infovide.SVNJni.SVNAuthorization;


/**
 * This class check access to documents found by Indexer.
 * This implementation use class SVNAuthorization with external
 * library (written in "C") to decided if user has rights to read each of documents or not.
 * 
 * This class also uses RepositoryList class for getting repositories names from properties given
 * from user ('repoListFile' or 'directPath').
 * @author Przemyslaw Kleszczewski
 *
 */
public class DocumentAccessCheckerJni implements DocumentAccessChecker {

	private Logger logger = Logger.getLogger("svnsearcher");
	
	private String[] repositoryNames;
	private String parentPath;
	private SVNAuthorization auth;
	private String access;
	private ConfigurationProvider prov;
	/**
	 * Class constructor. Sets needed information to use external library (SVNAuthorization).
	 * @param path Path to property file
	 * @throws AuhtorizationException Thrown when problem with load Authorization module occurred
	 * @throws ConfProviderException Syntax error in properties file.
	 * @throws FileNotFoundException Thrown when coulden't find properties file
	 * @throws IOException low level error in reading properties file
	 */
    public DocumentAccessCheckerJni(String path) throws AuhtorizationException, FileNotFoundException, IOException,  ConfProviderException {
    
    	ArrayList<String> hits;
    	auth = new SVNAuthorization();
    	SVNAuthorization.loadLib();
    	prov = new ConfigurationProvider();
    	prov.getConfiguration(path);
    	parentPath = prov.getUrl();
    	access = prov.getAccess();
    	RepositoryList list = new RepositoryList();
    	
    	/*
    	 * This method requires repositories names from 'repoListFile' or from 'directPath' 
    	 * Each one, found in Index files, document must be from repository that name is in 'directPath' or in 'repListFile'
    	 *  
    	 * @TODO better way is getting, by this method, repositories names from Index files
    	 * and not requiring 'repoListFile' or 'directPath' in SVNSearcher (web application).
    	 */
    	/// @li getting repositories names found in 'directPath' or from 'repListFile'
    	hits = list.getDirectAccessRepositories(prov.getDirectPath());
    	repositoryNames = new String[hits.size()];
    	for(int i = 0; i < hits.size(); i++)
				repositoryNames[i] = new String(hits.get(i));
    }
    
    /**
     * This method parse given url to path in format: 'reposName:/path/to/file'
     * This format id needed for external library which checks rights to documents (SVNAuthorization.c)
     * @param url - url to file, before rights to them was checked. 
     * @return return path to file in form: 'reposName:/path/to/file'
     */
    private String parseAdress(String url) 
    {
    	StringBuffer parse = new StringBuffer();
    	String help = url.replaceFirst(parentPath, "");
    	/// @li if given address ('url') has repository name and after them has "/"
    	for(int i = 0; i < repositoryNames.length; i++) {
    		if(help.startsWith(repositoryNames[i])
    				&& (help.substring(repositoryNames[i].length(), repositoryNames[i].length()+1)).equals("/")
    				) 
    		{
    			parse.append(repositoryNames[i].substring(repositoryNames[i].lastIndexOf("/") + 1) + ":" + help.replaceFirst(repositoryNames[i], ""));
    			break;
    		}
    	}
    	return new String(parse); 
    }
    
    /**
     * This method analyzes results returns from external library - SVNAuthorization.c (checking
     * access for found documents) - and build new list of documents, which access to user has rights
     * @param response - result from external library SVNAuthorization.c, matches to 'old' list of documents
     * @param old - list of all documents found by Indexer. 
     * @return list for documents available for the user. 
     */
    private ArrayList<SearchResultEntry> authzList(boolean[] response, ArrayList<SearchResultEntry> old) {
    	ArrayList<SearchResultEntry> answer = new ArrayList<SearchResultEntry>();
    	for(int i = 0; i < response.length ; i++)	{
	    	if(response[i] == true) {
	    		answer.add(old.get(i));
	   		}
    	}
    	return answer;
    }
    
    /**
     * This is main method for checking access for given user, for each document,
     * and return only these documents, which user has rights to read.
     * This method call external library SVNAuthorization.c.
     * @param documents - all documents found by Indexer for given query 
     * @param user - user, who use SVNSearcher
     * @return From 'documents' this method return only these, which 'user' has access to read.
     * @throws AuhtorizationException Thrown when problem with load Authorization module occurred
     */
    public ArrayList<SearchResultEntry> checkDocuments(ArrayList<SearchResultEntry> documents, String user) throws AuhtorizationException {
    	try {
    		
	    	String[] repo; /* paths to files. Will be parse to format: 'repoName:/path/to/file' */
	    	boolean[] response;
	    	int i = 0;
	    	if(user != null)
	    		user = new String(user);
	    	else
	    		/// @li probably anonymous user
	    		user = ConfigurationProvider.DEFAULT_USER;
	    	if(documents.size() > 0) {
	    		repo = new String[documents.size()];
	    		for(i = 0; i < repo.length; i++ ){
	    			logger.debug("DOC: " + documents.get(i).getFileName());
	    			repo[i] = parseAdress(documents.get(i).getFileName()).trim();
	    			logger.debug("SVN: " + repo[i]);
	    		}
	    		if(repo.length > 0){
	    			/// calling method from external library. 
	    			response = auth.makeAuthorization(user, repo, access);
					return authzList(response, documents);
	    		}
	    		else
	    			throw new Exception("Empty list to check access for each document");
	    	}
    	} catch (Exception e) {
    		throw new  AuhtorizationException("AuhtorizationException: "+e.getMessage());
    	}
        return new ArrayList<SearchResultEntry>();
    }
}
