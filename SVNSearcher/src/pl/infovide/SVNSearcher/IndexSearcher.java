package pl.infovide.SVNSearcher;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;

import pl.infovide.SVNInfo.ConfProviderException;
import pl.infovide.SVNInfo.ConfigurationProvider;
import pl.infovide.SVNJni.AuhtorizationException;


/**
 * This class searches in index, to find documents for given query.
 * This class also call authorization method to verify which documents should be
 * returned. This class return only that amount of documents which are needed
 * to show in one page. 
 *
 */
public class IndexSearcher {
    private String indexDirectory;      /**< Directory with index files. >*/
    private String path; /**< Path to .properties file > */
    private Logger logger = Logger.getLogger("svnsearcher");


    /**
     * Class constructor. It initializes information needed for indexer.
     * 
     * @param indexDirectory Path to directory with index files.
     */
    public IndexSearcher(String indexDirectory, String path) {
        this.indexDirectory = indexDirectory;
        this.path = path;

    }
    
    /**
     * Main method of this class. It search index, to find documents for given query.
     * 
     * @param queryString query content
     * @param start Number of page, for which gives the results. 
     * @param resultOnPage Number of items showed per one page.
     * @return Table with 5 items: First item (0) it's table ArrayList&lt;SearchResultEntry&gt;
     * which kept found documents (only these for which user has read rights).
     * Second item (1) it's Boolean value which says if there exist next elements
     * after these given in ArrayList.
     * Third item (2) it's Integer value which says what was number of documents
     * with read access for given user.
     * Fourth item (3) it's Float value which says what was time of searching index
     * Fifth item (4) it's Integer value says what was number of all documents found in this
     * query before authorization for the user.
     *  
     * @throws IndexSearcherException Exception throws during error in searching index
     * @throws IndexSearcherParseException Exception throws during error with parsing given query.
     * @throws RuntimeException when sort type isn't exist in index.
     * @throws ConfProviderException Syntax error in properties file.
	 * @throws FileNotFoundException Thrown when coulden't find properties file
	 * @throws IOException low level error in reading properties file
	 * @throws AuhtorizationException Thrown when problem with load Authorization module occurred
     */
    public Object[] search(String queryString, int start, 
                       int resultOnPage, String user, String sortType, boolean reverse) throws IndexSearcherException, 
                                                IndexSearcherParseException, RuntimeException,  FileNotFoundException,
                                                IOException,  ConfProviderException, AuhtorizationException,
                                                OutOfMemoryError {
        
        Object[] res = new Object[5];
        ConfigurationProvider conf = new ConfigurationProvider();
        conf.getConfiguration(conf.getFilename());
        
        Float searchTime = null;
        Integer numberOfDocuments = null;
        ArrayList<SearchResultEntry> result ;//= new ArrayList<SearchResultEntry>();
        boolean ifNextElements = false;
        
        ifNextElements = true;
        /// @li Creating object to search index.
        org.apache.lucene.search.Searcher searcher = null;
        try {
            searcher = 
                    new org.apache.lucene.search.IndexSearcher(indexDirectory);
        } catch (IOException e) {
        	if (searcher != null)
        		searcher.close();
            throw new IndexSearcherException("Coulden't create object to search index: " + 
                                             e.getMessage());
        }catch (OutOfMemoryError e){
        	if (searcher != null)
        		searcher.close();
        	throw e;
        }
        
      
        /// @li Creating analyzer and parser for query.
        Analyzer analyzer = new SimpleAnalyzer();
        Query query = null;
	        String[] fields = new String[6];
	        fields[0] = new String("Name");
	        fields[1] = new String("FileBody");
	        fields[2] = new String("Author");
	        fields[3] = new String("Comment");
	        fields[4] = new String("Type");
	        fields[5] = new String("Date");
  
	        /// @li Creating query for index        
	        BooleanClause.Occur[] flags = {BooleanClause.Occur.SHOULD,
	        		BooleanClause.Occur.SHOULD, 
	        		BooleanClause.Occur.SHOULD, 
	        		BooleanClause.Occur.SHOULD,
	        		BooleanClause.Occur.SHOULD,
	        		BooleanClause.Occur.SHOULD};
	        try {
	            query = MultiFieldQueryParser.parse(queryString, fields, flags, analyzer);
	        } catch (RuntimeException e){
	        	searcher.close();
	        	throw e;
	        }
	        catch (ParseException e) {
	        	searcher.close();
	            throw new IndexSearcherParseException(e.getMessage());
	        }catch (OutOfMemoryError e){
	        	searcher.close();
	        	throw e;
	        }
        
        Hits hits = null;
        // Preparing 'sort' object
        // INFO: Sort works correct only with 'true' option
        SortField userType = new SortField(sortType, true);
        SortField defaultField = new SortField("DDate", true);
        
        SortField[] sortFields = new SortField[]{userType, defaultField};
        Sort sort = new Sort(sortFields);


        /// @li Calling query 
        long before = System.currentTimeMillis();
        try {
        	if (sortType.equals("Score"))
        		hits = searcher.search(query);
        	else
        		hits = searcher.search(query, sort);
        	logger.info("hits length (found docs.):" +hits.length() );
        }catch (BooleanQuery.TooManyClauses e) {
        	searcher.close();
        	throw e;
        }catch (IOException e) {
        	searcher.close();
            throw new IndexSearcherException("Error during searching in index: " + 
                                            e.getMessage());
        }catch (OutOfMemoryError e){
        	if( hits == null || hits.length() == 0){
        		searcher.close();
        		throw e;
        	}
        	Searcher.setErrorInBean("Not all results could be taken from index!\n" +
					"All found docs. was "+hits.length()+" but there is not enough memory to serve it.\n"
					);
        }
        long after = System.currentTimeMillis();
        searchTime = new Float((float)(after - before) / 1000f);
        
        /// @li Getting found documents (path and number) from index and put them to temporary result variable.
        ArrayList<SearchResultEntry> tempResult ;
        try {
        	if (sortType.equals("Score")){
        		tempResult = getTempResult(hits, !reverse);
        	}else
        		tempResult = getTempResult(hits, reverse);
        	
        }catch(IndexSearcherException e) {
        	searcher.close();
        	throw e;
        }catch (OutOfMemoryError e){
        	searcher.close();
        	throw e;
        }
    
       
        Integer numberOfFoundDocuments = new Integer(tempResult.size());
            /// @li Checking rights for fund documents, and gets only these, where user has read rights
       try{
        if(conf.isAuthorization()){
            		DocumentAccessChecker checker = new DocumentAccessCheckerJni(path);
            		result = checker.checkDocuments(tempResult, user);
          }
        else
        	   result = tempResult;
       } catch (OutOfMemoryError e ){
    	    searcher.close();
       		throw e;
       }
        numberOfDocuments = new Integer(result.size());
            
        tempResult = new ArrayList<SearchResultEntry>(resultOnPage);
            /// @li 'i' variable is initialize with value of number of staring document,
            /// from the query
        int i = start;
        int counter = 0;
            /// from documents for the user gets results for one page
       try{
        for(;i < result.size() && counter < resultOnPage; i++ , counter++ ){
               String fileBody = new String();
               String author = new String();
               String comment = new String();
               String date = new String();
               String type = new String();
               float score = 0.0f;
               try {
                   Document doc = hits.doc(result.get(i).getNumber());
                   fileBody = doc.get("FileBody");
                   author = doc.get("Author");
                   comment = doc.get("Comment");
                   date = doc.get("DDate");
                   type = doc.get("Type");
                   score = hits.score(result.get(i).getNumber());
               } catch (IOException e) {
            	   searcher.close();
                    throw new IndexSearcherException("Error during getting document from index.");
               }
               SearchResultEntry entry = new SearchResultEntry();
               entry.setFileName(result.get(i).getFileName());
               entry.setFileBody(fileBody);
               entry.setAuthor(author);
               entry.setComment(comment);
               entry.setLastModified(date);
               entry.setHitScore(score);
               entry.setType(type);
               entry.setShortFileName(shorten(entry.getFileName()));
               tempResult.add(entry);
        }
       }catch (OutOfMemoryError e){
    	   logger.error("Out of memory during getting "+(i-start)+"th document on page.\n These is not enough memory to present results");
    	   searcher.close();
      	   throw e;
       }

        if (i < result.size())
               ifNextElements = true;
        else
               ifNextElements = false;
        /// @li Closing index
        try {
            searcher.close();
        } catch (IOException e) {
            throw new IndexSearcherException("Error during closing index: " + 
                                             e.getMessage());
        }
        
        
        
        /// @li Sets result table with suitable results and returns it.
        res[0] = tempResult;
        res[1] = ifNextElements;
        res[2] = numberOfDocuments;
        res[3] = searchTime;
        res[4] = numberOfFoundDocuments;
        return res;
    }
    
    /**
     * From path to files takes only file name.
     * @param fileName full path to file with url
     * @return file name (after last '/');
     */
    private String shorten(String fileName)
    {
    	return fileName.substring(fileName.lastIndexOf("/")+1);
    }
    /**
     * INFO: This method is using only because option 'reverse' in 'Sort'
     * class of 'Lucene' doesn't work correct.
     * This methods takes temporary results in order or reverse.
     * @param hits Result of searching
     * @param reverse info if takes results from end to beginning or from beginning to end
     * @return  List with Entry of results
     * @throws IndexSearcherException
     */
    private ArrayList<SearchResultEntry> getTempResult(Hits hits, boolean reverse) throws IndexSearcherException, OutOfMemoryError{
    	 ArrayList<SearchResultEntry> tempResult = 
             new ArrayList<SearchResultEntry>(hits.length()+10);
    	 if (hits != null && hits.length() != 0){
    		 int i = 0;
    		 if (!reverse ){
    			 i = hits.length()-1;
    		 }
    		 logger.info("Getting FileNames for all found documets...");
    		 try {
    			 while (i >=0 && i < hits.length()) {
    				 tempResult.add(new SearchResultEntry(hits.doc(i).get("FileName"), i) );
    				 if (!reverse)
    					 i--;
    				 else
    					 i++;
    			 }
    		 } catch (IOException e) {
				 throw new IndexSearcherException("Error during getting documents from index: "+e.getMessage());
			 }catch(OutOfMemoryError e){
				 if (tempResult.size() == 0)
					 throw e;
				Searcher.setErrorInBean("Not all results could be taken from index!\n" +
						"All found documents was "+hits.length()+" but there is not enough memory to serve it. Only "+tempResult.size()+" items taken.");
										 
			 }
    		 logger.info("Done");
    	 }
         return tempResult;	
    }
}

/**
 * Exception throws during searching index.
 * @author Fryderyk Mazurek
 *
 */
class IndexSearcherException extends Exception {

    private static final long serialVersionUID = 1L;

    public IndexSearcherException() {
        super("IndexSearcherException");
    }

    public IndexSearcherException(String mesg) {
        super(mesg);
    }
}

/**
 * Exception throws during parsing query.
 * @author Fryderyk Mazurek
 *
 */
class IndexSearcherParseException extends Exception {

    private static final long serialVersionUID = 1L;

    public IndexSearcherParseException() {
        super("IndexSearcherParseException");
    }

    public IndexSearcherParseException(String mesg) {
        super(mesg);
    }
}
