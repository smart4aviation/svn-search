package pl.infovide.SVNInfo;

/**
 * This class extends Exception class. Object of this class is returned when some error,
 * connected with properties file, occurred.
 * @author Bartosz Gasparski
 *
 */

public class ConfProviderException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public ConfProviderException() {
		super("Configuration in properties file: "+ new ConfigurationProvider().getFilename()+" is not correct.");
	}
	public ConfProviderException(String mesg) {
		super(mesg);
	}
}
