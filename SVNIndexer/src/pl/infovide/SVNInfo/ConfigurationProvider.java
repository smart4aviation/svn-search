package pl.infovide.SVNInfo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.apache.lucene.search.BooleanQuery;

/**
 * Class takes, from properties file, needed information
 * @author kleszcz
 *
 */
public class ConfigurationProvider
{
	public static ConfigurationProvider INSTANCE;
	private static String filename;
	private Properties prop;
	private String user;
	private String password;
	private String url;
	private String directPath;
	private String indexPath;
	private int maxClouseCount;
	private int mergeFactor;
	private int maxMergeDocs;
	private int maxFieldLength;

	private String repListFile;
	
	public String getRepListFile() {
		return repListFile;
	}
	
	
	public static void setFilename(String filename) {
		ConfigurationProvider.filename = filename;
	}
	
	public String getFilename() {
		return filename;
	}
	
	public int getMergeFactor() {
		return mergeFactor;
	}
	
	public int getMaxMergeDocs() {
		return maxMergeDocs;
	}
	
	public int getMaxClouseCount() {
		return maxClouseCount;
	}
	
	public String getIndexPath() {
		return indexPath.trim();
	}
	
	public String getDirectPath() {
		if (directPath != null){
			return directPath.trim();
		}
		return directPath;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getUrl() {
		return url.trim();
	}
	
	public String getUser() {
		return user.trim();
	}
	
	/**
	 * Method takes information from .properties file
	 * @param path - path to .properties file
	 * @throws ConfProviderException Syntax error in properties file.
	 * @throws FileNotFoundException Thrown when coulden't find properties file
	 * @throws IOException low level error in reading properties file
	 */
	public void getConfiguration(String path) throws FileNotFoundException, IOException,  ConfProviderException {
		try {
			// this is not final implementation:
			setMaxFieldLength(5000); 
			
			prop = new Properties();
			FileInputStream confFile = new FileInputStream(path);
			prop.load(confFile);
			
			/*
			 * url and repListFile are obligated when you start indexer first time (and don't use 'directPaht')
			 * Next times if 'repListFile' is empty, only existed repository in index will be updated.
			 */
			url = prop.getProperty("url");
			repListFile = prop.getProperty("repListFile");

			user = prop.getProperty("user");
			if (user == null)
				user="anonymous";
			
			password = prop.getProperty("password");
			if (password == null)
				password="anonymous";
			
			
			/*
			 * DESCRIPTION : 'directPath' is taken to check
			 * if that path really exist in local file system.
			 * if exists then looking for files *.format
			 * and if find some then is taking each one
			 * as repository to make index.
			 * 
			 * If there is no any *.format files, then ignore
			 * directPath and taking 'repListFile' to get repositories name.
			 */
			directPath = prop.getProperty("directPath");

			
			indexPath = prop.getProperty("indexPath");
			if (indexPath == null)
				indexPath = "IndexFiles/"; //default in SVNIndexer directory.
			
			if( (! indexPath.trim().endsWith("/")) && ( ! indexPath.trim().endsWith("\\")) )
				throw new ConfProviderException("Incorrect syntax in "+getFilename()+"  -  check 'indexPath'," +
						" make sure it end \"/\" or with \"\\\\\" and has no space!");
			
		
			if (prop.getProperty("maxClouseCount") == null)
				maxClouseCount = 3000;
			else
				maxClouseCount = (new Integer(prop.getProperty("maxClouseCount"))).intValue();
			
			
			if (prop.getProperty("mergeFactor") == null)
				mergeFactor = 50;
			else
				mergeFactor = (new Integer(prop.getProperty("mergeFactor"))).intValue();
			
			if (prop.getProperty("maxMergeDocs") == null)
				maxMergeDocs = 100000;
			else
				maxMergeDocs = (new Integer(prop.getProperty("maxMergeDocs"))).intValue();
			BooleanQuery.setMaxClauseCount(maxClouseCount);
			
			INSTANCE = this;
			
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} catch (ConfProviderException e) {
			throw e;
		}
		
	}


	public void setMaxFieldLength(int maxFieldLength) {
		this.maxFieldLength = maxFieldLength;
	}


	public int getMaxFieldLength() {
		return maxFieldLength;
	}

}