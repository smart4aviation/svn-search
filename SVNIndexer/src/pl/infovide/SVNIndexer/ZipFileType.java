package pl.infovide.SVNIndexer;

import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicParseException;

import org.apache.log4j.Logger;

import de.schlichtherle.io.File;
import de.schlichtherle.io.ArchiveDetector;
import java.io.ByteArrayInputStream;

import java.io.IOException;

/**
 * This class reads content of all files and directories ZIP archive.
 * Calls method to recognize file type and parse each of recognized files.
 * 
 * @author Bartosz Gasparski
 *
 */
public class ZipFileType extends AbstractIndexedFileType {
	protected StringBuffer buffer; /**< Buffer for all text from archive. */
	private Logger logger = Logger.getLogger("svnindexer");
	private long size = 0;
	
	private String archTempName = "TmpSVNIndexerArchiwum.zip"; /**< Path to file (default file will be created in
	application directory) where library can create temporary root of parsing archive.
	If you add absolute or direct path, all sub directories must already exist!  */
	private FinderFileTypeInterface finderFileType;
	 /**
	  * Base constructor
	  * @param filePath Path to file with archive. It's needed to log
	  * with correctly path.
	  */
    public ZipFileType(String filePath) {
    	super(filePath);
    	finderFileType = LoaderFinderFTypeClass.FOUND_CLASS;
    }
	
    /**
     * This method needs 'byte[] data', which had to be set before calling this method.
     * Reads all archive and put all Strings from each parsed file to one 'body'.
     * This body will be connected to all 'zip' archive file.
     * 
     * @return String from all parsed files from archive.
     * @throws GettingBodyException throws when problem with getting body of ZIP document occurred.
     * @throws ClassNotFoundException When implementation need external class to load and coulden't find it
     * @throws IOException during low level problem with access to read or write files.
     */
	public String getBody() throws GettingBodyException, IOException, ClassNotFoundException {
		size = 0;
		ByteArrayInputStream in = null;
		String virtualPath="";
		try{
		buffer  = new StringBuffer(""); /*buffer, for all strings, got from all files in archives */
		  /*Virtual path to files in archive. */
		 File archiwum = null;
		
	  
		   /// 'DEFAULT' option doesn't serve 'tar' archives. 
		   /// 'ALL' option serves more than 'zip' and 'jar', but not implemented yet.
		 File.setDefaultArchiveDetector(ArchiveDetector.DEFAULT);
		 
		 ///'TrueZip' library interface: creating virtual archive root
		 /// it's necessary to browse directories and files in archive. 
	     archiwum = new File(archTempName);
	     archiwum.deleteAll();
	     ///'TrueZip' library interface need InputStream to create temporary file
	     in = new ByteArrayInputStream(data);
	     
	     archiwum.catFrom(in);
	     in.close();
	   
	     
	     if (archiwum == null || (!archiwum.exists())  ){
	    	 
	    	 throw new GettingBodyException("TrueZip temporary archive file ("+archTempName+") coulden't be created, read or written");
	     }
	     
	     virtualPath = makeVirtualDirectPath(archiwum.getAbsolutePath());
	     try {
	    	 goIntoDir(archiwum);
	     } catch (OutOfMemoryError e){
	    	 logger.error("All archive content is not taken from: "+filePath+", because of memory error occurred: "+e.getMessage());
				
	     }
	      if (buffer.equals(""))
	    	  buffer = null;
	      logger.info("Summary got archive content size: "+((long)size/1024)+"KB");
	      archiwum.deleteAll();
	      File.umount();
	      File.umount(true);
	  
	      return buffer.toString();  
	 }catch (ClassNotFoundException e){
		 throw e;
	 }catch (IOException e){
		 throw new IOException("TrueZip library coulden't read file in archive:"+virtualPath+ " \n"+e.getMessage());
	 }catch (Exception e){
			 throw new GettingBodyException("TrueZip library coulden't read file in archive:"+virtualPath+ " \n"+e.getMessage());
				 
	 } finally{
		 if (in != null)
			 in.close();
	     File.umount();
	     File.umount(true);
	 }
	}
	/**
	 * This method browse all archive recurrence.
	 * Another 'zip' archive is recognized as a directory.
	 * 
	 * @param dir it's abstract directory in archive.
	 */
	private void goIntoDir(File dir) throws IOException, ClassNotFoundException, OutOfMemoryError{
		
		logger.debug("Going into archive directory: "+dir.getAbsolutePath());
		String mainPath = dir.getAbsolutePath();	 
		String[] members = dir.list();
		File memFileOrDir = null;
		if (members != null) {
		 for (String mem : members) {
			 try {
				 memFileOrDir = new File (mainPath+"/"+mem);
				 size +=memFileOrDir.length();
				 if (memFileOrDir.isDirectory()){
					 goIntoDir(memFileOrDir);
				 }
				 else
					 parseFile(memFileOrDir);
				
			 }catch (ClassNotFoundException e){
				 throw e;
			}
			 catch (IOException e){
				 throw e;
			}
			 catch (OutOfMemoryError e){
				 throw e;
			}finally{
				
				 memFileOrDir.deleteAll();
			}
		 }
		}
	}
	/**
	 * This method calling method recognizing file type and parsing this type of file.
	 * After it adds String body from this file to one buffer for all archive
	 * @param file abstract file in archive.
	 */
	private void parseFile(File file) throws IOException, ClassNotFoundException, OutOfMemoryError{
		String directPath = makeVirtualDirectPath(file.getAbsolutePath());
		logger.debug("Parsing archive file: "+file.getAbsolutePath());
		java.io.ByteArrayOutputStream streamFromFile = null;
		String body = null;
		try {
			
			
			streamFromFile = new  java.io.ByteArrayOutputStream();  
			file.catTo(streamFromFile);
			byte[] dataFile = streamFromFile.toByteArray();
			
			streamFromFile.close();
			String fileType = null;
			try {
				fileType = finderFileType.recogniseFileType(directPath, dataFile)[1];
			} catch ( MagicException e ) {
				logger.error("Internal error in external library to find File Type: "+e.getMessage());
			} catch  (MagicParseException e){
				logger.error("Internal error in configuration for external library to find File Type: "+e.getMessage());
			}
		
			AbstractIndexedFileType indexedFileType = AbstractIndexedFileType.getIndexedFileType(fileType, directPath, config);	
		
			///file type is served and isn't empty
			if (indexedFileType != null && dataFile != null && dataFile.length != 0)  {
				indexedFileType.setData(dataFile);
				///@li parsing content file
				logger.debug("Parsing abstract file body");
				body = indexedFileType.getBody();
				
			} else{
				logger.debug("Omitted file in archvie (not supported or not recognized type): "+directPath);
			}
			if (body != null) {
				buffer.insert(0, body);
				buffer.insert(0, " ");
				//buffer +=" "+ body;
			}		
			//else
			//	logger.warn("Empty body from file: "+directPath);

		} catch (OutOfMemoryError e){
			throw e;
		}
		 catch(IOException e ){
				throw e;
			}
		catch (ClassNotFoundException e){
			 throw e;
		}
		catch (GettingBodyException ex){
				logger.error("Omitted file in archvie: "+directPath +": "+ex.getMessage());
		}finally{
			streamFromFile.close();
			
		}
	}
	/**
	 * This method creates virtual path to files and directories in archive,
	 * that it's look like file or directory has path from SVN root.
	 * @param fileAbsolutPath path on your local file system. To see more read about 'archTempName'
	 * @return path to file started from SVN root, through archive name and sub directories in archive.
	 */
	private String makeVirtualDirectPath(String fileAbsolutPath){
		if (fileAbsolutPath == null)
			return null;
		
		int realPathStart = fileAbsolutPath.indexOf(archTempName);
		String virtualDirectPath = fileAbsolutPath.substring(realPathStart+archTempName.length());
		virtualDirectPath = filePath+virtualDirectPath;
		virtualDirectPath = virtualDirectPath.replace("\\", "/");

		return virtualDirectPath;
	}
	
}
