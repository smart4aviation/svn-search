package pl.infovide.SVNIndexer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.lang.OutOfMemoryError;

import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicParseException;

import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.io.SVNRepository;

import org.apache.log4j.Logger;

import pl.infovide.SVNInfo.ConfigurationProvider;

/**
 * This is implementation of interface to serve files (and directories).
 * This class also use external libraries, defined in xml configuration file.
 * @author Fryderyk Mazurek
 */
public class GetterNeededFiles implements FinderFilesListener {
	private FileIndex index;	/**< Index manager >*/
	private HashMap<String, String> config;	/**< Map: fileType -> Class name suitable to parse this type>*/
	private Logger logger = Logger.getLogger("svnindexer");
	private ConfigurationProvider prov;
	private FinderFileTypeInterface finderFileType; 
	/**
	 * Class constructor. It initializes class components.
	 * @param index Object representing index (prepared to use).
	 * @param config HaspMap storing filesType -> className, to parse this type.
	 * @throws ClassNotFoundException When implementation need external class to load and coulden't find it - to load class to find file type
	 */
	public GetterNeededFiles(FileIndex index, HashMap<String, String> config) throws ClassNotFoundException{
		this.index = index;
		this.config = config;
		prov = ConfigurationProvider.INSTANCE;
		try{
			LoaderFinderFTypeClass.loadClass(ConfigReader.RECOGNIZER_CLASS);
			finderFileType =  LoaderFinderFTypeClass.FOUND_CLASS; 
		}catch (ClassNotFoundException e){
			throw e;	
		}
	}
	
	/**This method: 1) checking if given path (fileName) isn't directory.
	 * 				2) getting file content (bytes)
	 * 				3) Using external library to recognize file type
	 * 				4) Using external library to get text content file
	 * 				5) Adding this file to index, with all information got from
	 * 				libraries and from SVN.
	 * @param revision Revision for updating file
	 * @param svnAddress URL address to SVN repository.
	 * @param fileName file (or directory) name e.g.: '/directoryPathInSVN/fileName.ext'
	 * @param repository Object representing repository (prepared to use).
	 * @param latestRevision Latest revision for updating repository
	 * @throws ClassNotFoundException When implementation need external class to load and coulden't find it - to load class to find file type or to get body from supported file type
	 * @throws IOException during low level problem with access to read or write index files.
	 * @throws SVNException Exception if some error in preparing connection occurred. 
	 */
	public void getFileName(String svnAddress, String fileName, SVNRepository repository, long revision, String comment, long latestRevision) throws ClassNotFoundException, IOException, SVNException {
		String extension = null; /* extension got from external library or from file path  */
		String fileType = null; /* fileType is a group of extension used for AbstractIndexFileType - to parse content*/
		ByteArrayOutputStream fileStream = null; /*file stream from current repository file */
		SVNProperties fileProperties = null; /*file information */
		String repRoot = null; /*to keep getRepositoryRoot(false).getPath() */
		boolean result = false; /* If adding entry finished with success or not */
		String name = null; /* Full name of added file */
		String body = null; /*text content file (if not possible to get it, then 'null') */
		String address = prov.getUrl();
		address = address.substring(0, (address.length()-1)); //cutting last '/'
		int i1 = svnAddress.indexOf("://");
		int i2 = svnAddress.indexOf("/", i1 + 3);
		
		if (i2 != -1)
			svnAddress = svnAddress.substring(0, i2);  // makes svnAddress='http://host.name.com'
		byte[] fileData = null;
		try {
			
			fileProperties = new SVNProperties();
			fileStream = new ByteArrayOutputStream();
			
			if (repository.checkPath(fileName, revision) == SVNNodeKind.DIR || repository.checkPath(fileName, latestRevision) == SVNNodeKind.DIR){
				logger.info("Getting directory from repository: " + fileName);	
				return;
			}
			else if (repository.checkPath(fileName, revision) == SVNNodeKind.FILE || repository.checkPath(fileName, latestRevision) == SVNNodeKind.FILE) {
				logger.info("Getting file from repository " + ( revision!=latestRevision ? ("(changed in revision: "+revision+"): ") : ": " )+fileName);	
				
				try{
				/// @li Getting file from SVN repository
					repository.getFile(fileName, latestRevision, fileProperties, fileStream);
					
					fileData = fileStream.toByteArray();
					
				}catch (OutOfMemoryError e){
					logger.error("File content is not taken from: "+fileName+", because of memory error occurred during getting file from rep."+  getSize(fileData)+"\n"+e.getMessage());
				} catch (Exception e){
					logger.error("Error during indexing file: "+fileName +": "+e.getMessage() );
				}catch (Error e){
					logger.fatal("Fatal error occurred, file: "+fileName+" could not be indexed, Reason: "+e.getMessage());
				}finally {	
					fileStream.close();
				}

				/// @li using external library library to find 'fileType' and 'extension'
				logger.debug("Using FinderFileTypeInterface to define file type for: "+fileName);
				String[] resultRecognising = null;
				try {
					resultRecognising = finderFileType.recogniseFileType(fileName, fileData);
					extension = resultRecognising[0];
					fileType = resultRecognising[1];
					logger.debug("FinderFileTypeInterface found extension: "+extension+ " and file type: "+fileType+" for file: "+fileName);
				} catch (MagicException e ){
					logger.error("Internal error in external library to find File Type: "+e.getMessage());
				} catch (MagicParseException e){
					logger.error("Internal error in configuration for external library to find File Type: "+e.getMessage());
				}catch (OutOfMemoryError e){
					logger.error("File content is not taken from: "+fileName+", because of memory error occurred during recognizing file type."+  getSize(fileData)+"\n "+e.getMessage());
				}
				
				/// @li Trying to find suitable class for parsing this type of file.
				logger.debug("Trying to find suitable class for parsing this type of file");
				AbstractIndexedFileType indexedFileType = AbstractIndexedFileType.getIndexedFileType(fileType, fileName, config);
				
				try {
					///file type is served and isn't empty
					if (indexedFileType != null && fileData != null && fileData.length != 0)  {
						indexedFileType.setData(fileData);
						///@li parsing content file
						logger.debug("Parsing file body");
						body = indexedFileType.getBody();
					}
					/// coulden't get 'body' (text content file)
				} 	catch (GettingBodyException e) {
					logger.error("Couldent get file content: " + fileName+": "+e.getMessage());
				} 
				
				repRoot = repository.getRepositoryRoot(false).getPath();
				name = svnAddress + repRoot + fileName;
				///try add entry with all information about commit 
				try {
						result = index.addEntry(name, body, 
								new Long(fileProperties.getStringValue("svn:entry:committed-rev")), true, name.replace(address, ""),
								fileProperties.getStringValue("svn:entry:last-author"), comment,
								parseDate(fileProperties.getStringValue("svn:entry:committed-date")),
								extension);

				//if coulden't get all information about commit 
				}catch (IOException e){
					throw e;
				}catch (NullPointerException e ){
					logger.error("Coulden't get all information about commit for: " + fileName+
							"\n This file coulden't be indexed: "+e.getMessage());
				}catch (OutOfMemoryError e)	 {
					logger.error("Out of memory error occurred, file: "+fileName+" not indexed"+  getSize(fileData)+", Reason: "+e.getMessage());
				}
				//checking if added to index (with 'body' or not)
				if (result == true){
					if (body != null)
						logger.info("Indexed file: " +  fileName +(extension != null ? (", as type: "+extension) :", with no type" )+  getSize(fileData));
					else
						logger.warn("File: "+fileName+" indexed without content"+(extension != null ? (", as type: "+extension) :", with no type" )+  getSize(fileData));
				} 
				body = null;
			} else{
				logger.warn("Path: "+fileName+" is unknown SVN type");
			}
		}catch(ClassNotFoundException e) {
			throw e;
		}  
		catch(SVNException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}catch (Exception e) {
			logger.error("Error during indexing file: "+fileName +" "+ getSize(fileData)+" "+e.getMessage() );
		}
		catch (OutOfMemoryError e)	 {
			logger.error("Out of memory error occurred, file: "+fileName+" could not be indexed"+  getSize(fileData)+" Reason: "+e.getMessage());
		}catch (Error e){
			logger.fatal("Fatal error occurred, file: "+fileName+" could not be indexed, Reason: "+e.getMessage());
		}
	}
	
	/**
	 * Parsing date from repository format to SVNIndexer format.
	 * @param date date taken from SVN repository
	 * @return date in SVNIndexer format
	 */
	public String parseDate(String date){
		String ndate = date.replace("T", "");
		ndate = ndate.replace("-", "");
		ndate = ndate.replace(":", "");
		ndate = ndate.replace(".", "");
		return ndate;
	}
	
	/**
	 * This method should serves directories similar to method 'getFileName'
	 * for files. 
	 * Currently this method is not using (both directories and files are
	 * serving in 'getFileName').
	 */
	public void getDirectoryName(String svnAddress, String directoryName, SVNRepository repository) {
	}
	
	private String getSize(byte[] fileD){
		try{
			if (fileD != null && fileD.length != 0)
				return  (", size: "+((long)(fileD.length/1024))+"KB" );
		}catch(Exception e){
			logger.error("Error in getting file size: "+ e.getMessage());
		}
		return "";
	}
}
