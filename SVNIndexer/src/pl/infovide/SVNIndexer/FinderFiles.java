package pl.infovide.SVNIndexer;

import java.io.IOException;
import java.util.ArrayList;

import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import pl.infovide.SVNIndexUpdater.IndexUpdater;

import org.apache.log4j.Logger;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;

/**
 * Main usage of this class is preparing and closing connection
 * to SVN server.
 * 
 * There is one more usage (in method 'crawlRepository') to
 * browse SVN tree, from given directory, and for each file
 * call special 'listener' to serve it. 
 * This function is using when Indexer find object, which was copied
 * from other path. Then this origin path is being 'crawl' (searching recurrently)
 * 
 * 
 * @author Fryderyk Mazurek
 *
 */
public class FinderFiles {
	static {
		SVNRepositoryFactoryImpl.setup(); 	/// @li initializing connections type: svn and svn+ssh
		DAVRepositoryFactory.setup();		/// @li initializing connections type DAV (svn + http/https)
		FSRepositoryFactory.setup();		/// @li initializing connections type file://
	}
	
	protected SVNRepository repository;	/**< Object recalls to SVN repository. */
	protected String url;					/**< Url address SVN repository. */
	protected String name;				/**< User name - to SVN repository */
	protected String password;			/**< Password - for this User name, to SVN repository */
	
	private long latestRevision;		/**< Last revision number. */
	protected String root;				/**< Main repository directory. */
	protected Logger logger = Logger.getLogger("svnindexer");
	
	/**
	 *  Class constructor. It initializes class components.
	 *  Remember, that if you want to index all repository,
	 *  SVNIndexer should be working on user with rights to read all parts of repository.
	 *  There is obligated that given user has rights to main '/' in SVN repository.
	 * 
	 * @param url URL address of SVN repository
	 * @param name User name, who login to SVN repository (it's not obligatory, when authorization with certificate).
	 * @param password Password , for user name, to SVN repository (it's not obligatory, when authorization with certificate).
	 */
	public FinderFiles(String url, String name, String password) {
		this.url = url;
		this.name = name;
		this.password = password;
	}
	
	/**
	 * Getting repository object
	 * @return Getting repository object
	 */
	public SVNRepository getRepository() {
		return repository;
	}
	
	/**
	 * 
	 * This method initializing objects needed to make connection with SVN repository.
	 * IMPORTANT: Latest Revision is get from repository only once - only in this method. It synchronizes getting files from the same revision.
	 * @throws SVNException Exception if some error in preparing connection occurred. 
	 * Reason can be incorrect user name or password.
	 */
	public long prepareConnection() throws SVNException {
		try {
			if (repository != null)
				closeConnection();
			/// @li Making objects needed to initialize connection to SVN repository.
			repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(url));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(name, password);
			repository.setAuthenticationManager(authManager);
			/// @li Latest Revision is get from repository only once - only in this method. It synchronizes getting files from the same revision.
			setLatestRevision(repository.getLatestRevision());
			root = repository.getRepositoryRoot(false).getPath();
			return getLatestRevision();
		} catch (SVNException e) {
			throw e;
		}		
	}

	/**
	 * This method closes connection to SVN repository.
	 * @throws SVNException Exception throws when closing connection error occurred.
	 */
	public void closeConnection() {
			if (repository != null) {
				repository.closeSession();
				repository = null;
			}
	}
	

	/**
	 * This method is using when Indexer find object, which was copied
	 * from other path. Then this origin 'path' is being 'crawl' (searching recurrently)
	 * 
	 * Method browse directory given as 'path' and for each file calls 'listener'
	 * to serve each of these files.
	 * @param path Path to directory in SVN repository, which has to be search.
	 * @param listener Object which serve files (and directories - but not implemented yet). To see more read about "FinderFilesListener" interface.
	 * @throws ClassNotFoundException When implementation need external class to load and coulden't find it
	 * @throws IOException during low level problem with access to read or write index files.
	 */
	
	public void crawlRepository(String path, FinderFilesListener listener, IndexUpdater indexUpdater) throws ClassNotFoundException, IOException, OutOfMemoryError {
		
			/// @li Getting files list, from current directory (path)
			ArrayList<SVNDirEntry> elementsInDir = new ArrayList<SVNDirEntry>();
			try {
				repository.getDir(path, getLatestRevision(), true, elementsInDir);
			}catch (SVNException e) {
					logger.error("Coulden't browse files in directory: "+path+ " (path not updated), " + e.getMessage());
					/// omitting this part of sub directory
					return ;
			}
			/// @li this method is not implemented in current version SVNIndexer
			//listener.getDirectoryName(url, path, repository);
			for (int i = 0; i < elementsInDir.size(); ++i) {
				/// @li For each file in given directory
				if (elementsInDir.get(i).getKind() == SVNNodeKind.FILE) {
					String filePath;
					//logger.info("it's file");
					int i1 = elementsInDir.get(i).getURL().getPath().indexOf(root);
					if (i1 == 0)
						/// @li if path for file starts from main repository directory, then cut it from the path. 
						filePath = elementsInDir.get(i).getURL().getPath().substring(root.length(), elementsInDir.get(i).getURL().getPath().length());	
					else
						filePath = elementsInDir.get(i).getURL().getPath();
					/// @li serve each file (find type, get body and add to index )
					
					try {
						indexUpdater.delete(filePath);
						listener.getFileName(url, filePath, repository, elementsInDir.get(i).getRevision(), null, latestRevision);
					}catch (SVNException e){
						logger.error("File: "+filePath+" coulden't be indexed: "+e.getMessage());
					}catch (ClassNotFoundException e){
						throw e;
					}catch (IOException e){
						throw e;
					}
				}
				else if (elementsInDir.get(i).getKind() == SVNNodeKind.DIR)
					/// @li For each directory call this method recurrently
					crawlRepository(path.equals("") ? elementsInDir.get(i).getName() : path + "/" + elementsInDir.get(i).getName(), listener, indexUpdater);
			}				
		
	}

	private void setLatestRevision(long latestRevision) {
		this.latestRevision = latestRevision;
	}

	public long getLatestRevision() {
		return latestRevision;
	}
}


