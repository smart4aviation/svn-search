package pl.infovide.SVNIndexer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.Region;

/**
 * This class use to getting text content from XLS files (Microsoft Excel),
 * using external library
 * @author Fryderyk Mazurek
 *
 */
public class XlsFileType extends AbstractIndexedFileType {
	/**
	 * Class constructor, it calls base constructor.
	 * @param filePath path to file in SVN repository
	 */
	public XlsFileType(String filePath) {
    	super(filePath);
    }
	
	/**
	 * @return this method returns text content file, from 'XLS' files.
	 * @throws GettingBodyException throws when problem with getting body of MS Excel document occurred.
	 */
	public String getBody() throws GettingBodyException {
		try {
			/// @li Trying to read file content
			ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
			HSSFWorkbook excelDoc = new HSSFWorkbook(inputStream);
			StringBuffer buffer = new StringBuffer();
			/// @li Getting datasheets, rows and cells content
			int numberOfSheets = excelDoc.getNumberOfSheets();
		
			for (int i = 0; i < numberOfSheets; ++i) {
				HSSFSheet sheet = excelDoc.getSheetAt(i); 
				
				int firstRowNumber = sheet.getFirstRowNum();
				int lastRowNumber = sheet.getLastRowNum();
				
				for (int ii = firstRowNumber; ii < lastRowNumber; ++ii) {
					HSSFRow row = sheet.getRow(ii);
					if (row == null)
						continue;
					
					short firstCellNumber = row.getFirstCellNum();
					short lastCellNumber = row.getLastCellNum();
					
					for (short iii = firstCellNumber; iii < lastCellNumber; ++iii) {
						HSSFCell cell = row.getCell(iii);
						if (cell == null)
							continue;
						
						int cellType = cell.getCellType();
						if (cellType != HSSFCell.CELL_TYPE_STRING)
							continue;
						
						HSSFRichTextString richText = cell.getRichStringCellValue();
						buffer.append(richText.getString() + " ");
					}
				}
				
				int numberOfMergedRegions = sheet.getNumMergedRegions();
				for (int ii = 0; ii < numberOfMergedRegions; ++ii) {
					Region mergedRegion = sheet.getMergedRegionAt(ii);
					
					short columnFrom = mergedRegion.getColumnFrom();
					int rowFrom = mergedRegion.getRowFrom();
					
					HSSFRow row = sheet.getRow(rowFrom);
					HSSFCell cell = row.getCell(columnFrom);
					
					int cellType = cell.getCellType();
					if (cellType != HSSFCell.CELL_TYPE_STRING)
						continue;
					
					HSSFRichTextString richText = cell.getRichStringCellValue();					
					buffer.append(richText.getString() + " ");					
				}
			}			
			
			/// @li Return result
			return buffer.toString();
		}catch (OutOfMemoryError e)	 {
			throw new GettingBodyException("POI library coulden't read 'XLS' file! Out of memory error occurred Reason: \n"+
					e.getMessage());
		} catch (IOException e) {
		    throw new GettingBodyException("POI library coulden't read 'XLS' file! Reason: \n"+
					e.getMessage());
		}catch (Exception e) {
		    throw new GettingBodyException("POI library coulden't read 'XLS' file! Reason: \n"+
					e.getMessage());
		}
		
	}
}
