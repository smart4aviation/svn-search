package pl.infovide.SVNIndexer;

import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import org.apache.log4j.Logger;

import pl.infovide.SVNInfo.ConfigurationProvider;

/**
 * This class use to manage index files.
 * @author Fryderyk Mazurek
 * @author Przemyslaw Kleszczewski
 */
public class FileIndex {
    protected Directory directory;		/**< Directory where will be created new index.*/
    protected Analyzer analyzer;		/**< Analyzer needed to analyse files content. */
    protected IndexWriter writer;		/**< Object to modify index content. */
	protected ConfigurationProvider provider;/**< Configuration of application*/
	protected String indexDirectory;
	protected int dateLong;
	private Logger logger = Logger.getLogger("svnindexer");

	/**
	 * Method preparing to indexing files.
	 * @throws throws IOException when coulden't create index files.
     */
	public boolean prepareIndex(String indexDirectory) throws IOException
	{
		dateLong = 17;
		this.indexDirectory = indexDirectory;
		provider = ConfigurationProvider.INSTANCE;
		try {
			directory = FSDirectory.getDirectory(indexDirectory, false);
			analyzer = new SimpleAnalyzer();
		} catch (IOException e) {
			
			throw new IOException("Error occured until creating new index: "+e.getMessage());
			
		}
		return true;
	}
	
	/**
	 * Preparing indexWriter to write to index.
	 */
	private void prepareWriter() throws IOException {
	try {
		try {
			logger.debug("Writer preparing");
			if (writer != null )
				closeWriter();
			writer = new IndexWriter(directory, analyzer, false);
			writer.setMergeFactor(provider.getMergeFactor());
			writer.setMaxMergeDocs(provider.getMaxMergeDocs());
			writer.setUseCompoundFile(true);
			writer.setMaxFieldLength(provider.getMaxFieldLength());
		} 
		catch (IOException e) {
				
			   if (IndexReader.isLocked(directory)) {
				   logger.info("Trying to unlock directory in PreparingIndex...");
				     IndexReader.unlock(directory);
				     	if (writer != null )
				     		closeWriter();
				     	writer = new IndexWriter(directory, analyzer, false);
						writer.setMergeFactor(provider.getMergeFactor());
						writer.setMaxMergeDocs(provider.getMaxMergeDocs());
						writer.setUseCompoundFile(true);
						writer.setMaxFieldLength(provider.getMaxFieldLength());
				     
				   }
		}
	}catch (OutOfMemoryError e)	 {
		logger.error("Out of memory error occurred, during preparing indexWriter, Reason: "+e.getMessage());
		e.printStackTrace();
			
	}
	}
	
	/**
	 * This method adds new row to index
	 * @TODO - if there is no 'body' file, then we can try reduce revision of this file, to try index one more.
	 * @param name File or repository name.
	 * @param body File content, which will be indexed.
	 * @param file If we add new row to index or repository name (true - new row to file, false - repository name)
	 * @param fname File name 
	 * @return True if operation success, false if not.
	 * @throws IOException when occurred problem with writing to index files. 
	 */
	public boolean addEntry(String name, String body, Long revision, boolean file, String fname)  throws IOException {
		IndexSearcher searcher = null;	
		try {
			logger.info("Adding repo "+name+" with revision "+revision+" to index...");
			prepareWriter();
			TermQuery query = new TermQuery(new Term("FileName", name));
			searcher = new IndexSearcher(indexDirectory);
			Hits hit = searcher.search(query);
			if(hit.length() == 0) {
				Document document = new Document();	 
				if(file){
					document.add(new Field("FileName", name, Field.Store.YES, Field.Index.UN_TOKENIZED));
					if(fname != null){
						document.add(new Field("Name", fname, Field.Store.YES, Field.Index.TOKENIZED));
						document.add(new Field("DocName", cutFileName(fname), Field.Store.YES, Field.Index.UN_TOKENIZED));
						
						
					}
				}
				else{
					document.add(new Field("RepositoryName", name, Field.Store.YES, Field.Index.UN_TOKENIZED));
				
					//these empty Fields are needed to initialized them, that future sort on this
					//fields will be possible (even if no files with these fields will be indexed).
					document.add(new Field("DAuthor", "", Field.Store.YES, Field.Index.UN_TOKENIZED));
					document.add(new Field("DComment", "", Field.Store.YES, Field.Index.UN_TOKENIZED));
					document.add(new Field("DDate", "", Field.Store.YES, Field.Index.UN_TOKENIZED));
					document.add(new Field("Type", "", Field.Store.YES, Field.Index.UN_TOKENIZED));
				}
				document.add(new Field("Revision", revision.toString(), Field.Store.YES, Field.Index.UN_TOKENIZED));
				if(body != null)
					document.add(new Field("FileBody", body, Field.Store.YES, Field.Index.TOKENIZED));

				writer.addDocument(document);
			} else{
				logger.fatal("Repository: "+name+" is already in index! "+hit.length()+" repo(s). found for this name! repo skiped");
				return false;
			}
		
		} catch (IOException e) {
			throw e;
		} finally{
			if (searcher != null)
				searcher.close();
			if (writer != null) {
				logger.debug("Closing writer for repo :"+name);
				closeWriter();
			}
		}
		return true;
	}
	
	/**
	 * Alternative method which enables indexing author, date of last modification, comment of last commit
	 * @param name - url to file
	 * @param body - text content of file
	 * @param revision - revision to index
	 * @param file - if we index file, or repository name. (True - file)
	 * @param fname - file name with path, but started from '/' svn, not url.
	 * @param author - author last commit.
	 * @param comment - comment to last commit 
	 * @param date - date of last change.
	 * @param type - file extension. It's extension found in external library or (if failed) from file name.
	 * This parameter is not necessary but precipitates searching with file type option.
	 * @return True if operation success, false if not.
	 * @throws IOException when occurred problem with writing to index files. 
	 * @throws NullPointerException When information got from SVN log are not completed
	 */
	public boolean addEntry(String name, String body, Long revision, boolean file, String fname, String author, String comment, String date, String type) throws IOException, NullPointerException {
		IndexSearcher searcher = null;
		try {
			logger.info("Adding file "+name+" with revision "+revision+" to index...");
			
			prepareWriter();
			
			//needed date format: yyyymmddHHMMSSsss
			if(date != null && dateLong < date.length())
				date = date.substring(0, dateLong); //be careful! 
			else if (date != null)
				date = date.substring(0, date.length());//be careful! 
			// checking if document(s) with this FileName is/are already in index. There shoulden't be any docs. with this file name. In other way it's internal error in SVNIndexer
			TermQuery query = new TermQuery(new Term("FileName", name));
			searcher = new IndexSearcher(indexDirectory);
			Hits hit = searcher.search(query);
			if(hit.length() == 0) {
				Document document = new Document();	 
				if(file){
					
					document.add(new Field("FileName", name, Field.Store.YES, Field.Index.UN_TOKENIZED));
					if(fname != null){
						document.add(new Field("Name", fname, Field.Store.YES, Field.Index.TOKENIZED));
						document.add(new Field("DocName", cutFileName(fname), Field.Store.YES, Field.Index.UN_TOKENIZED));
					}
					
					if(author != null){
						document.add(new Field("Author", author, Field.Store.YES, Field.Index.TOKENIZED));
						document.add(new Field("DAuthor", author, Field.Store.YES, Field.Index.UN_TOKENIZED));
					}
					if(comment != null){
						document.add(new Field("Comment", comment, Field.Store.YES, Field.Index.TOKENIZED));
						document.add(new Field("DComment", comment, Field.Store.YES, Field.Index.UN_TOKENIZED));
					}
					if(date != null){
						document.add(new Field("Date", date.substring( 0, 8 ), Field.Store.YES, Field.Index.UN_TOKENIZED));
						document.add(new Field("DDate", date, Field.Store.YES, Field.Index.UN_TOKENIZED));
					}
					if(type != null)
						document.add(new Field("Type", type, Field.Store.YES, Field.Index.UN_TOKENIZED));
				}
				else
					document.add(new Field("RepositoryName", name, Field.Store.YES, Field.Index.UN_TOKENIZED));
				
				document.add(new Field("Revision", revision.toString(), Field.Store.YES, Field.Index.UN_TOKENIZED));
				if(body != null)
					document.add(new Field("FileBody", body, Field.Store.YES, Field.Index.TOKENIZED));
				writer.addDocument(document);
			} else {
				logger.fatal("Document: "+name+" is already in index! "+hit.length()+" doc(s). found for this FileName! File skiped");
				return false;
			}
			
		}//When information got from SVN log are not completed. After it this file will not be indexed, but indexer still works.
		catch (NullPointerException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} finally{
			if (searcher != null)
				searcher.close();
			if (writer != null) {
				logger.debug("Closing writer for file :"+name);
				closeWriter();
			}
		}
		return true;
	}
	
	/**
	 * Closing indexWriter
	 */
	private void closeWriter() throws IOException {
		try {
			if (writer != null) {
				logger.debug("Closing IndexWriter...");
				writer.close();
				writer = null;
				logger.debug("Closing success");
			}
		
		}catch (IOException e) {
			throw new IOException("IOException during closing indexWriter: " + e.getMessage());
		}catch (Exception e){
			e.printStackTrace();
			logger.fatal("Exception occurred, during closing indexWriter:\n");	
			throw new IOException("Exception during closing indexWriter, index Corrupted: " + e.getMessage());
		}catch (OutOfMemoryError e)	 {
			logger.error("Out of memory error occurred, during closing indexWriter, indexed file may be not added to index, Reason: "+e.getMessage());
			e.printStackTrace();
		}finally {
			   if (IndexReader.isLocked(directory)) {
				   logger.info("Trying to unlock directory in Closing Writer...");
				     IndexReader.unlock(directory);
				   }
		}

	}
	
	/**
	 * This method optimize index files to get more quicker and precise results.
	 */
    public void optimize() throws IOException {
    	 //logger.info("Optimalizing...");
    	 logger.info("Empty Optimalization. Not using currently.");
    	 /*
    	  * Optymalization in Apache.Lucene crashes with huge repositories.
    	  * (starts, makes index files size bigger and when OutOfMemory error occurred,
    	  * finishes with this bigger index size).
    	  */
//    	try {
//    	    prepareWriter();
//    	    writer.optimize();
//    	    closeWriter();
//    	} catch (IOException e) { 
//    		logger.fatal("IOException occurred, during optimizing indexWriter:\n");	
//    		throw e;
//    	}catch (OutOfMemoryError e)	 {
//			logger.error("Out of memory error occurred, during optimizing indexWriter, Reason: "+e.getMessage());
//			e.printStackTrace();
//				
//		}finally {
//				
//			   if (IndexReader.isLocked(directory)) {
//				   logger.info("Trying to unlock directory in Optimize Writer...");
//				     IndexReader.unlock(directory);
//				   }
//		}
    	logger.info("Optimalization complited");
    }
    
    /**
     * This method set length of kept date.
     * @param dateLong new length of kept date.
     */
    public void setDateLong(int dateLong) {
		this.dateLong = dateLong;
	}
    /**
     * This method cut file name from given path or URL
     * @param path path or URL to file
     * @return file name - after last '/' or '\'
     */
    private String cutFileName(String path){
    	if (path == null)
    		return null;
    	int from= 0;

    	path = path.toLowerCase();
    	if (path.lastIndexOf('/') != -1)
    		from = path.lastIndexOf('/');
    	else if (path.lastIndexOf('\\') != -1)
    		from = path.lastIndexOf('\\');
    	if ((from+1) < path.length())
    		return path.substring(from+1);
    	return path.substring(from);
    }
}
