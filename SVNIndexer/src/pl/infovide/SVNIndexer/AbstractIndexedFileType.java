package pl.infovide.SVNIndexer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.io.IOException;

/**
 * Abstract class describing how to get text from file. It provides only method which
 * has to be implemented to get text content of the file.
 * This class has also static method, which attribute to each file type - class from 
 * configuration xml file. This found class will be able to get text content of the file.
 * 
 * @author Fryderyk Mazurek
 *
 */
public abstract class AbstractIndexedFileType {
	protected String filePath; /**<File name, which from text content will be taken.*/
	protected byte[] data;	/**< Buffer, where content file to parse must be written. */
	protected static HashMap<String, String> config; /**<Map: fileType -> Class name suitable to parse this type */
	/**
	 * Class constructor. It initialize variable which describes SVN path for each
	 * parsed file.  
	 * 
	 */
    public AbstractIndexedFileType(String filePath) {
    	this.filePath = filePath;
    }
    
    /**
     * Abstract method which must be implemented in extended class.
     * 
     * @return Text content file
     * @throws GettingBodyException Exception during getting text content file.
     * @throws ClassNotFoundException When implementation need external class to load and coulden't find it
     * (fe. Class to read zip use all other class to read supported file types)
     * @throws IOException during low level problem with access to read or write files.
     */
    public abstract String getBody() throws GettingBodyException, ClassNotFoundException, IOException;
    
    /**
     * This method writes binary content file to buffer.
	 *
     * @param data Binary content file.
     */
    public void setData(byte[] data) {
    	this.data = data;
    }
    
    /**
     * Static method returning object AbstractIndexedFileType, which for given,
     * as a parameter, fileType search in 'ConfigReader.CONFIG_XML_FILE' class name, suitable to
     * get text content file.
     * 
     * 
     * @param fileType type of the file, recognized by JMimeMagic or (if it failed) from file name.
     * @param filePath Path to the file.
     * @param config Database which map: fileType on class name suitable to parse this type of file.
     * @return Object class AbstractIndexedFileType serve to getting text content file.
     * @throws ClassNotFoundException When implementation need external class to load and coulden't find it
     * 
     */
    public static AbstractIndexedFileType getIndexedFileType(String fileType, String filePath, HashMap<String, String> config) throws ClassNotFoundException{  	
    	AbstractIndexedFileType.config = config;
    	if (fileType != null) {
    			///@li Trying to find suitable class name - to get text content file.
    			String className = config.get(fileType);
    			if (className != null) {
    				Class<?> fileTypeClass = null;		
    				/// @li Getting this class
    				try {
    					fileTypeClass = Class.forName(className);
    				} catch (ClassNotFoundException e) {
    					throw new  ClassNotFoundException ("Coulden't find class "+className+" for type: "+ fileType+
						", check your configuration in "+ConfigReader.CONFIG_XML_FILE+", \n"+e.getMessage());
    				}		
    				Object instance = null;
    				/// @li Getting constructor of this class and try to create new object.
    				Constructor[] constructors = fileTypeClass.getConstructors();
    				for (int i = 0; i < constructors.length; ++i) {
    					Class[] parameters = constructors[i].getParameterTypes();
    					if (parameters.length != 1)
    						continue;			
    					if (parameters[0].isInstance(new String()) == false)
    						continue;
    					try {
    						instance = constructors[i].newInstance(new Object[] { filePath });	
    						break;
    					} catch (InstantiationException e) {
    						throw new  ClassNotFoundException ("Coulden't load class: "+ className+" for type: "+fileType+
    								", Reason: "+ e.getMessage()+", "+
    								"check your configuration in "+ConfigReader.CONFIG_XML_FILE);
   						
    					} catch (IllegalAccessException e) {
    						throw new  ClassNotFoundException ("Coulden't load class: "+ className+" for type: "+fileType+
    								", Reason: "+ e.getMessage()+", "+
    								"check your configuration in "+ConfigReader.CONFIG_XML_FILE);
    					} catch (IllegalArgumentException e) {
    						throw new  ClassNotFoundException ("Coulden't load class: "+ className+" for type: "+fileType+
    								", Reason: "+ e.getMessage()+", "+
    								"check your configuration in "+ConfigReader.CONFIG_XML_FILE); 
    					} catch (InvocationTargetException e) {
    						throw new  ClassNotFoundException ("Coulden't load class: "+ className+" for type: "+fileType+
    								", Reason: "+ e.getMessage()+", "+
    								"check your configuration in "+ConfigReader.CONFIG_XML_FILE);
    					}
    				}
    				/// @li Checking type of this object and return it as a result
    				if (instance instanceof AbstractIndexedFileType)
    					return (AbstractIndexedFileType)instance;
    				else {
    					throw new  ClassNotFoundException ("Class: "+ className+", "+
    							"is not instance of pl.infovide.SVNIndexer.AbstractIndexedFileType class, "+
    							"check your configuration in "+ConfigReader.CONFIG_XML_FILE);
    				}
    			}
    	} 	
    	return null;
    }
}
/**
 * Class extends Exception. This Exception is thrown during getting text content file.
 * 
 * @author Fryderyk Mazurek
 *
 */
class GettingBodyException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public GettingBodyException() {
		super("GettingBodyException: Coulden't get file body");
	}
	public GettingBodyException(String mesg) {
		super(mesg);
	}
}
