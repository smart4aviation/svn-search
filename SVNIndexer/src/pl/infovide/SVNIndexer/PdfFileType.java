package pl.infovide.SVNIndexer;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.PDFTextStripper;

/**
 * This class use to getting text content from PDF file (Portable Document Format),
 * using external library
 * 
 * @author Fryderyk Mazurek
 *
 */
public class PdfFileType extends AbstractIndexedFileType {
	/**
	 * Class constructor, it calls base constructor.
	 * @param filePath path to file in SVN repository
	 */
    public PdfFileType(String filePath) {
    	super(filePath);
    }
    
	/**
	 * @return this method returns text content file, from 'PDF' files.
	 * @throws GettingBodyException throws when problem with getting body of PDF document occurred.
	 */
	public String getBody() throws GettingBodyException {
		PDDocument doc = null;
		ByteArrayInputStream inputStream = null;
		try {
			/// @li Trying to read file content
			inputStream = new ByteArrayInputStream(data);
			PDFTextStripper textStripper = new PDFTextStripper();
			doc = PDDocument.load(inputStream);
			String body = textStripper.getText(doc);
			return body;
	
		}catch (OutOfMemoryError e)	 {
			throw new GettingBodyException("POI library coulden't read 'PDF' file! Out of memory error occurred Reason: \n"+
					e.getMessage());
		} 
		catch (Exception e){
		    throw new GettingBodyException("PDFBox library coulden't read 'PDF' file! Reason: \n"+
		    		e.getMessage());
		}finally {
			try{
				if (doc != null)
					doc.close();
				if (inputStream != null)
					inputStream.close();
			}catch (IOException e){
				 throw new GettingBodyException("PDFBox library coulden't read 'PDF' file! Reason: \n"+
				    		e.getMessage());
			}
		}	
	}
}
