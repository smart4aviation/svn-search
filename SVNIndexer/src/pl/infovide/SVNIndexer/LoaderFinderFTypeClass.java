package pl.infovide.SVNIndexer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * This class load class implemented FinderFileTypeInterface.
 * Name of the class is taken from configuration xml file,
 * from tag <FinderFileType> <class>
 * 
 * @author Bartosz Gasparski
 *
 */
public class LoaderFinderFTypeClass {
	public static FinderFileTypeInterface FOUND_CLASS;
/**
 * Call this method to load class implementation for FinderFileTypeInterface.
 * @param className Name of the class
 * @return instance of the class
 * @throws ClassNotFoundException When implementation need external class to load and coulden't find it - This exception is thrown during all problems with finding suitable class
 */
	public static void loadClass(String className) throws ClassNotFoundException {
		if (className != null && !(className.equals("")) ){
			Class<?> finderTypeClass = null;		
			/// @li Getting this class
			try {
				finderTypeClass = Class.forName(className);
			} catch (ClassNotFoundException e) {
				throw new  ClassNotFoundException("Coulden't find class for FinderFileType: "+className+", "+
						"check your configuration in "+ConfigReader.CONFIG_XML_FILE+ " "+e.getMessage(), e);
			}			
			Object instance = null;		
			/// @li Getting constructor of this class and try to create new object.
			Constructor[] constructors = finderTypeClass.getConstructors();
			for (int i = 0; i < constructors.length; ++i) {
				Class[] parameters = constructors[i].getParameterTypes();
				if (parameters.length != 0)
					continue;
				try {
					instance = constructors[i].newInstance();	
					break;
				} catch (InstantiationException e) {
					throw new  ClassNotFoundException("Coulden't find class for FinderFileType: "+className+", "+
							"check your configuration in "+ConfigReader.CONFIG_XML_FILE+ " "+e.getMessage(), e);				
				} catch (IllegalAccessException e) {
					throw new  ClassNotFoundException("Coulden't find class for FinderFileType: "+className+", "+
							"check your configuration in "+ConfigReader.CONFIG_XML_FILE+ " "+e.getMessage(), e);
			
				} catch (IllegalArgumentException e) {
					throw new  ClassNotFoundException("Coulden't find class for FinderFileType: "+className+", "+
							"check your configuration in "+ConfigReader.CONFIG_XML_FILE+ " "+e.getMessage(), e);
			
				} catch (InvocationTargetException e) {
					throw new  ClassNotFoundException("Coulden't find class for FinderFileType: "+className+", "+
							"check your configuration in "+ConfigReader.CONFIG_XML_FILE+ " "+e.getMessage(), e);
			
				}
			}		
			/// @li Checking type of this object and return it as a result
			if (instance instanceof FinderFileTypeInterface) {
				FOUND_CLASS = (FinderFileTypeInterface)instance;
				return ;
			}

			else {
				throw new  ClassNotFoundException("Class: "+ className+", "+
						"is not implementation of pl.infovide.SVNIndexer.FinderFileTypeInterface, "+
						"check your configuration in "+ConfigReader.CONFIG_XML_FILE);

			}
		}	
		throw new  ClassNotFoundException("No class name was found, "+
				"check your configuration in "+ConfigReader.CONFIG_XML_FILE);

	}
}
