package pl.infovide.SVNIndexer;

import java.util.HashMap;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Class use to reading configuration xml file which stores file types and
 * connected with them class names, which are are to read text content file.
 * @author Fryderyk Mazurek
 *
 */
public class ConfigReader {
	public static String RECOGNIZER_CLASS; /**< Class name of the implementation FinderFileTypeInterface  */
	public static String CONFIG_XML_FILE ="config.xml" ; /**< File with information about
	external libraries to parsing files and finding files types.*/
	
	/**
	 * Class constructor. It initialize class component.
	 */
	public ConfigReader(){
	}
	
	/**
	 * Method returns HashMap which stores file types and suitable to them class names.
	 * These class are able to read text content file of these file types.
	 * This method reads xml file. 
	 * @return HashMap<String,String> Resulting HashMap. 
	 * @throws ConfigReaderException Error with syntax in xml files
	 */
	public HashMap<String, String> getConfig() throws ConfigReaderException {
		/// @li Trying to open xml file i starting interprets.
		try {
			XMLReader reader = XMLReaderFactory.createXMLReader();
		
			InputSource source = new InputSource(this.getClass().getClassLoader().getResourceAsStream(CONFIG_XML_FILE));
			
			ConfigHandler handler = new ConfigHandler();
			reader.setContentHandler(handler);
			reader.parse(source);
			
			/// @li Returns result
			return handler.getFileTypes();
		} catch (Exception e) {
			throw new ConfigReaderException("Coulden't parse "+ConfigReader.CONFIG_XML_FILE +" file! Incorrect syntax: "+e.getMessage());

		}
	}
}

/**
 * Class use to interprets configuration xml file (storing fileTypes and class names)
 * @author Fryderyk Mazurek
 *
 */
class ConfigHandler extends DefaultHandler {
	private int state = 0;		/**<Curently object state, which in the parser is.*/
	private int entryState = 0;	/**<Curently amount got fields of file type and adequate to them class names. */
	
	private String currentExtension;	/**< Currently value of file type. */
	private String currentClassName;	/**< Currently value of class name. */
	
	private HashMap<String, String> fileTypes = new HashMap<String, String>(); /**< HashMap, which store result of all parse.*/
	
	/**
	 * Class constructor. It initialize class components.
	 */
	public ConfigHandler() throws SAXException { 
		currentExtension = "";
		currentClassName = "";
	}
	
	/**
	 * This method is called during starting new type of node. It gets currently 
	 * object state and set new value depended of node name.
	 */
	public void startElement(String uri, String localName, String qName, Attributes atts) { 		
		if (state == 0 && qName.compareToIgnoreCase("config") == 0)
			state = 1; // after main node: config
		else if (state == 1 && qName.compareToIgnoreCase("fileTypes") == 0)
			state = 2; // after node: filesTypes
		else if (state == 2 && qName.compareToIgnoreCase("fileType") == 0)
			state = 3; // after node: fileType
		else if (state == 3 && qName.compareToIgnoreCase("extension") == 0)
			state = 4; // waiting for value from node: extension
		else if (state == 3 && qName.compareToIgnoreCase("class") == 0)
			state = 5; // waiting for value from node: class
		else if (state == 1  && qName.compareToIgnoreCase("FinderFileType") == 0)
			state = 6; //going to node: class for FinderFileType
		else if (state == 6 && qName.compareToIgnoreCase("class") == 0) // in node: class  in FinderFileType node 
			state = 7;
	}

	/**
	 * This method is called during ending each node. It gets currently object state
	 * and sets new value depended of node name.
	 */
	public void endElement(String uri, String localName, String qName) {
		if (state == 4 && qName.compareToIgnoreCase("extension") == 0)
			state = 3; // back to node: fileType
		else if (state == 5 && qName.compareToIgnoreCase("class") == 0)
			state = 3; // back to node: fileType
		else if (state == 3 && qName.compareToIgnoreCase("fileType") == 0)
			state = 2; // back to node: filesTypes
		else if (state == 2 && qName.compareToIgnoreCase("fileTypes") == 0)
			state = 1; // back to node: config
		else if (state == 2 && qName.compareToIgnoreCase("FinderFileType") == 0)
			state = 1; // back to main node: config
		else if (state == 7 && qName.compareToIgnoreCase("class") == 0)
			state = 2; // back to node: FinderFileType
			
	}
	
	/**
	 * This method is called during getting text value from node. It checks
	 * currently state (node) and if find node with fileType (called here "extension"),
	 * or with class name, gets this information and writes to fields.
	 * If found a pair (extension and class name) write it to result map.
	 * 
	 */
	public void characters(char[] ch, int start, int length)  {
		/// @li Getting currently state for object.
		/// @li Writing data from to fields.
		if (state == 7){
			ConfigReader.RECOGNIZER_CLASS = new String(ch, start, length);
		}	
		if (state == 4 || state == 5) {
			String entry = new String(ch, start, length);
			if (state == 4) {
				currentExtension = entry.toLowerCase();
				entryState++;
			}
			else if (state == 5) {
				currentClassName = entry;
				entryState++;
			}
			/// @li Writing data to resulting map, if find pair (extension and class name)
			if (entryState == 2) {
				fileTypes.put(currentExtension, currentClassName);
				
				/// @li Clear temporary data fields.
				currentExtension = "";
				currentClassName = "";
				entryState = 0;
			}
		}
	}
	
	/**
	 * This method returns Map storing: file types and suitable to them class names.
	 * @return Table HashMap.
	 */
	public HashMap<String, String> getFileTypes() {
		return fileTypes;
	}	
}

