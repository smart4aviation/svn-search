package pl.infovide.SVNIndexer;

import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicParseException;

/**
 * This interface is using to defined file type and extension.
 * In SVNIndexer we use two definitions:
 * 'extension' - fe: java, txt, doc, cpp, xls, xml etc. - all 
 * formats (compiled and open text)
 * 'fileType' - group of extension parsed in one class.
 * For example for extensions: txt, java, html, xml, fileType is 'txt'
 * For extension 'doc', fileType is 'doc'
 * For extension 'xls', fileType is 'xls'
 * For extension 'ppt', fileType is 'ppt'
 * 
 * If you add some new library to parsing some type of files,
 * you must define, in class implemented this interface,
 * what extensions are connected with your fileType.
 * 
 * Class implemented this interface should also log some situations,
 * when extension was taken from fileName (analysing content file - failed).
 * 
 * @author Bartosz Gasparski
 *
 */
public interface FinderFileTypeInterface {

	/**
	 * As describes this interface, this method recognizes
	 * what extension of the file it is, and what fileType
	 * is connected with this extension.
	 * 
	 * 
	 * @param fileName - it's needed in case parsing content file - failed.
	 * @param fileData - content of the recognizing file
	 * @return Table[0]  real file type (fe: java, C, txt, php, sh, xml, etc); Table[1] - group of file, to 
	 * find class parsing this type (fe: txt for java, C, txt, php, sh, xml, etc ; doc for doc; xls for xls, etc).
	 * @throws  MagicParseException temporary (not finally) type of Exception during problem with configuration in external library to find file type
	 * @throws MagicException temporary (not finally) type of Exception when occurred internal error in external library to find file type
	 */
	public String[] recogniseFileType(String fileName, byte[] fileData) throws MagicParseException, MagicException ;
	
}
