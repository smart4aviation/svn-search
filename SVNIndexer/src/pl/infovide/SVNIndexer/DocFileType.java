package pl.infovide.SVNIndexer;

import java.io.ByteArrayInputStream;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.usermodel.Section;

/**
 * This class use to getting text content file from Microsoft Word (doc) files.
 * @author Fryderyk Mazurek
 *
 */
public class DocFileType extends AbstractIndexedFileType {
		
	/**
	 * Class constructor. It class base constructor 
	 * @param filePath Path to file parsed in this instance.
	 */
	public DocFileType(String filePath) {
    	super(filePath);
    }
	
	/**
	 * Method returns text content file.
	 * @throws GettingBodyException throws when problem with getting body of MS Word document occurred.
	 */
	public String getBody() throws GettingBodyException {
		try {
			/// @li Trying to read content file.
			ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
		    
			HWPFDocument doc = new HWPFDocument(inputStream);
			Range r = doc.getRange();
		   
		    StringBuffer bodyBuffer = new StringBuffer();
		    /// @li Getting strings from section paragraphs of document
		    for (int x = 0; x < r.numSections(); x++) {
		    	Section s = r.getSection(x);
		    	for (int y = 0; y < s.numParagraphs(); y++) {
		    		Paragraph p = s.getParagraph(y);
		    		for (int z = 0; z < p.numCharacterRuns(); z++) {
		    			CharacterRun run = p.getCharacterRun(z);
		    			String text = run.text();
		    			bodyBuffer.append(text);
		    		}
		    		bodyBuffer.append("\n");
		    	}
		    }
		    
		    String result = bodyBuffer.toString();
		    byte[] resultBytes = result.getBytes();
		    /// @li Deleting unnecessary signs from got result. All these unnecessary signs result from incorrectly working of POI library.
		    for (int i = 0; i < resultBytes.length; ++i) {
		    	if (resultBytes[i] == 161 || resultBytes[i] == 198 || resultBytes[i] == 202 ||
		    		resultBytes[i] == 163 || resultBytes[i] == 209 || resultBytes[i] == 211 ||
		    		resultBytes[i] == 166 || resultBytes[i] == 172 || resultBytes[i] == 175 ||
		    		resultBytes[i] == 177 || resultBytes[i] == 230 || resultBytes[i] == 234 ||
		    		resultBytes[i] == 179 || resultBytes[i] == 241 || resultBytes[i] == 243 ||
		    		resultBytes[i] == 182 || resultBytes[i] == 188 || resultBytes[i] == 191 ||
		    		resultBytes[i] == 165 || resultBytes[i] == 140 || resultBytes[i] == 143 ||
		    		resultBytes[i] == 185 || resultBytes[i] == 156 || resultBytes[i] == 159 ||
		    		resultBytes[i] == 191 ||
		    		resultBytes[i] == '\n' || resultBytes[i] == '\t'		    		
		    	) // list of omitted signs.
		    		continue;
		    	
		    	if (resultBytes[i] >= 0 && resultBytes[i] <= 31)
		    		resultBytes[i] = ' ';
		    	else if (resultBytes[i] >= 32 || resultBytes[i] <= 127)
		    		continue;
		    	else if ((int)(resultBytes[i] + 256) >= (int)128)
		    		resultBytes[i] = ' ';
		    }
		    result = new String(resultBytes);
		    
		    /// @li Returns result.
		    return result;
		}
		catch (OutOfMemoryError e)	 {
			throw new GettingBodyException("POI library coulden't read 'DOC' file! Out of memory error occurred Reason: \n"+
					e.getMessage());
		}
		/// @li Getting all exceptions result from incorrectly working of POI library.
		catch (Exception e) { 
			throw new GettingBodyException("POI library coulden't read 'DOC' file! Reason: \n"+
			e.getMessage()
			);
		}
	}
}
