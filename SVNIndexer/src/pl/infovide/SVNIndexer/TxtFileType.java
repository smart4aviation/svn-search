package pl.infovide.SVNIndexer;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;

/**
 * This class use to getting text content from TXT file 
 * @author Fryderyk Mazurek
 *
 */
public class TxtFileType extends AbstractIndexedFileType {
	private Logger logger = Logger.getLogger("svnindexer");
	/**
	 * Class constructor, it calls base constructor.
	 * @param filePath path to file in SVN repository
	 */
    public TxtFileType(String filePath) {
    	super(filePath);
    	
    }
    
	/**
	 * @return this method returns text content file, from 'TXT' files.
	 */
	public String getBody() {
		String result = null;
		CharsetDetector detector = new CharsetDetector();
		detector.setText(data);
		CharsetMatch charsetMatch = detector.detect();
		if (charsetMatch == null){
			logger.debug("Charset detector hasn't found matched charset, 'body' got from String constructor");
			return (new String(data));
		}
		try {
			logger.debug("TextCharset found charset: "+charsetMatch.getName());
			result = charsetMatch.getString();
		} catch (OutOfMemoryError e)	 {
			logger.error("Problem with getting String by Charset detector: "+"Out of memory error occurred Reason: \n"+
					e.getMessage());
			return (new String (""));
		}catch (IOException e) {
			logger.debug("Problem with getting String by Charset detector: "+ e.getMessage());
			return (new String(data));
		}catch (Exception e) {
			logger.debug("Problem with getting String by Charset detector: "+ e.getMessage());
			return (new String(data));
		}
		return result;
	}
}
