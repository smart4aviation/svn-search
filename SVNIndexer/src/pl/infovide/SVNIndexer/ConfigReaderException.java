package pl.infovide.SVNIndexer;

/**
 * This class extends Exception class. Object of this class is returned when some error,
 * connected with parsing xml file, occurred.
 * @author Fryderyk Mazurek
 *
 */
public class ConfigReaderException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ConfigReaderException() {
		super("ConfigReaderException for file: "+ConfigReader.CONFIG_XML_FILE);
	}
	public ConfigReaderException(String mesg) {
		super(mesg);
	}
}