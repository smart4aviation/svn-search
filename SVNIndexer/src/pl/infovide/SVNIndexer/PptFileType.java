package pl.infovide.SVNIndexer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.OutOfMemoryError;

import org.apache.poi.hslf.extractor.PowerPointExtractor;

/**This class use to getting text content from PPT file (Microsoft PowerPoint),
 * using external library
 * @author Fryderyk Mazurek
 *
 */
public class PptFileType extends AbstractIndexedFileType {

	/**
	 * Class constructor, it calls base constructor.
	 * @param filePath path to file in SVN repository
	 */
    public PptFileType(String filePath) {
    	super(filePath);
    }
	
	/**
	 * @return this method returns text content file, from 'PPT' files.
	 */
	public String getBody() throws GettingBodyException {
		PowerPointExtractor extractor = null;
		try {
			/// @li Trying to read file content
			ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
			extractor = new PowerPointExtractor(inputStream);
			String result = extractor.getText() + "\n" + extractor.getNotes();
			return result;
		} catch (IOException e) {
		    throw new GettingBodyException("POI library coulden't read 'PPT' file! Reason: \n"+
					e.getMessage());
		}catch (OutOfMemoryError e)	 {
			throw new GettingBodyException("POI library coulden't read 'PPT' file! Out of memory error occurred Reason: \n"+
					e.getMessage());
		}
		catch (Exception e) {
		    throw new GettingBodyException("POI library coulden't read 'PPT' file! Reason: \n"+
					e.getMessage());
		}finally{
			try{
				if (extractor != null)
					extractor.close();
			}catch (IOException e ){
				 throw new GettingBodyException("POI library coulden't read 'PPT' file! Reason: \n"+
							e.getMessage());
			}
		}
	}
}
