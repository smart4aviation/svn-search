package pl.infovide.SVNIndexer;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.ibm.icu.text.CharsetDetector;

import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;
/**
* This class use JMimeMagic library to recognize file type. Thanks to it, file extension
* can be guessed even if file name doesn't have it. MimeMagic can be upgrated in xml file: 'Magic.xml'
* where all files format information is kept. This is internal file in MimeMagic open-source library.
* MimeMagic gives possibility to add new information about new format files.
* There are also some problems with identifing MS Office's documents.
* 'PPT' and 'PPS' are recognized as 'DOC', so then extension is taken from file name.
* Syntax of 'Magic.xml' file is based on 'MagicNumber' linux/unix file, but it's much more simple.
* 
* This class also use 'logger' to inform if it wasn't sure in identifing file type (for example 'doc' are impossible
* to unambiguously ascertain).
* @see {http://sourceforge.net/projects/jmimemagic/}
* @author Bartosz Gasparski
*/ 
public class JMimeMagicFinder implements FinderFileTypeInterface {

	private Logger logger = Logger.getLogger("svnindexer");
	public HashMap<String, String> supportedFTypes;
	public JMimeMagicFinder(){
		// this is not final implementation
		supportedFTypes = new HashMap<String, String>();
		supportedFTypes.put("java", "java");
		supportedFTypes.put("c", "cpp");
		supportedFTypes.put("cpp", "cpp");
		supportedFTypes.put("c++", "cpp");
		supportedFTypes.put("h", "cpp");
		supportedFTypes.put("rb", "rb");
		supportedFTypes.put("pl", "pl");
		supportedFTypes.put("py", "py");
		supportedFTypes.put("php", "php");
		supportedFTypes.put("html", "html");
		supportedFTypes.put("htm", "html");
		supportedFTypes.put("xsl", "xml");
		supportedFTypes.put("xsd", "xml");
		supportedFTypes.put("xml", "xml");
		supportedFTypes.put("tld", "xml");
		supportedFTypes.put("text", "txt");
		supportedFTypes.put("txt", "txt");
		supportedFTypes.put("bash", "bash");
		supportedFTypes.put("sh", "sh");
		supportedFTypes.put("shell", "sh");
		supportedFTypes.put("js", "js");
		supportedFTypes.put("javascript", "js");
		supportedFTypes.put("css", "css");
		supportedFTypes.put("jsp", "jsp");
		supportedFTypes.put("jsf", "jsp");

		
	}
	
	/** 
	 *  This method recognize real content of file and also return type of file to parse in POI class.
	 *  All text extension like java, C ,h , CPP, sh ,etc will be parsed by the same class for TXT.
	 *  'Description' field in MimeMagic is used to store group of type (fe: TXT for all not compiled types)
	 *  
	 * @param fileName  name of the analyzing file. If can't find FileType from MimeMagic
	 * 					then extension is taken from fileName.
	 * @param fileData  content of the file, to parse in MimeMagic and guess what type it is.
	 * @return Table[0]  real file type (fe: java, C, txt, php, sh, xml, etc); Table[1] - group of file, to 
	 *  find class parsing this type (fe: txt for java, C, txt, php, sh, xml, etc ; doc for doc; xls for xls, etc).
	 * @throws  MagicParseException temporary (not finally) type of Exception during problem with configuration in external library to find file type
	 * @throws MagicException temporary (not finally) type of Exception when occurred internal error in external library to find file type
	 
	 */
	public String[] recogniseFileType(String fileName, byte[] fileData) throws MagicParseException, MagicException, OutOfMemoryError {
		
		String[] extAndType = new String[2];
		String extension = null;
		String fileType = null;
		MagicMatch mimeMagic = null;
		String extFromName =  getExtFromFileName( fileName);
		/*
		 * If file is not empty, MimeMagic is use to guess what type of file it is
		 * 
		 */
		if (fileData != null && fileData.length != 0 ){
			try {

				mimeMagic = Magic.getMagicMatch(fileData);
				extension = mimeMagic.getExtension();
				fileType = mimeMagic.getExtension();
				
			}catch (MagicParseException me) {
				/// it means internal error in MimeMagic - configuration error in magic.xml
				throw me;

			}catch (MagicMatchNotFoundException me) {
				/*
				 * called when file type isn't recognized.
				 * And MimeMagic coulden't find nothing suitable
				 * from 'magic.xml' file.
				 * 'fileType' and 'extension' are getting from filename extension.
				 */
				extension = extFromName;
				fileType = extension;
				if (extFromName != null && supportedFTypes.containsKey(extFromName)){
					extension = supportedFTypes.get(extFromName);
					fileType="txt";
				}
				extAndType[0] = extension;
		    	extAndType[1] = fileType;
		    	logger.debug("MimeMagic can't parse file type: " + fileName +", file type got from filename extension: "+extension);				
		    	return  extAndType;
				
			}catch (MagicException me) {
				/// this situation should have never happen
				/// it means internal error in MimeMagic
				throw me;
				
			} catch (OutOfMemoryError e)	 {
				logger.error("File extension is not taken from file content for: "+fileName+", because of memory error occurred "+e.getMessage());
				extension = extFromName;
				fileType = extension;
				if (extFromName != null && supportedFTypes.containsKey(extFromName)){
					extension = supportedFTypes.get(extFromName);
					fileType="txt";
				}
				extAndType[0] = extension;
		    	extAndType[1] = fileType;
		    	logger.debug("MimeMagic got from file name: extension:"+extension+" and fileType:"+fileType+" for empty file:"+fileName);
				return extAndType;
			}
			
		/*
		 * if file is empty, get 'extension' from 'fileName' only.
		 */
		}else {
			extension = extFromName;
			fileType = extension;
			if (extFromName != null && supportedFTypes.containsKey(extFromName)){
				extension = supportedFTypes.get(extFromName);
				fileType="txt";
			}
			extAndType[0] = extension;
	    	extAndType[1] = fileType;
	    	logger.debug("MimeMagic got from file name: extension:"+extension+" and fileType:"+fileType+" for empty file:"+fileName);
			return extAndType;
			
		}
		/*
		 *  if MimeMagic coulden't find correct extension and didn't throw exception..
		 */
		logger.debug("Checking for more details for file extension");
		if (mimeMagic.getExtension() == null || mimeMagic.getExtension().equals("???") || mimeMagic.getExtension().equals("") ){
			extension = extFromName;
			fileType = extension;
		}
		/*
		 * Microsoft Office's file sometimes coulden't be exactly recognize.
		 * For example 'ppt' and 'pps' files are recognized as 'doc' files
		 * and other error situations take place.
		 * Then 'extension' is getting from file name if only there is extension in file name.
		 */
		if(mimeMagic.getExtension().equals("doc") && ( extFromName != null)){
			extension = extFromName;
			fileType = extension;
		}
		
		/*
		 * When text group file is found (all source code
		 * and not compiled content) 'fileType' is set as 'txt'.
		 */
		if (mimeMagic.getDescription().equals("txt")){
			fileType = "txt";
			
			if (extFromName != null && supportedFTypes.containsKey(extFromName)){
				extension = supportedFTypes.get(extFromName);
			}
		}
		
		
    	extAndType[0] = extension;
    	extAndType[1] = fileType;
    	logger.debug("MimeMagic got extension:"+extension+" and fileType:"+fileType+" for file:"+fileName);
		return extAndType;
	}
	
	/**
	 *  This simple method cut extension from file name
	 *  after last '.' This method is used when getting file
	 *  extension, from content file by MimeMagic, failed.
	 * @param fileName - file name for find extension after last '.'
	 * @return return string after last '.' (in lower case) or null when last '.' was not found.
	 */
	private String getExtFromFileName(String fileName){
		String ext = null;
		int li1 = fileName.lastIndexOf(".");
    	if (li1 != -1) {
    		if (li1 + 1 < fileName.length()) {
    				ext = fileName.substring(li1 + 1, fileName.length()).toLowerCase();
    				if (ext.indexOf('/') != -1 || ext.indexOf("\\") != -1 ||  ext.indexOf("\\\\") != -1 )
    					ext = null;			
    		}
    	}
    	return ext;
		
	}
}
