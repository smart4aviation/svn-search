package pl.infovide.SVNIndexer;

import org.tmatesoft.svn.core.io.SVNRepository;
import java.io.IOException;
import org.tmatesoft.svn.core.SVNException;
/**
 * This interface is using to serve each file (and directory), after as 
 * it was qualified to being indexed.
 * 
 * @author fmazurek
 *
 */
public interface FinderFilesListener {
	/**
	 * This method must get file content (having path to file, and prepared repository object),
	 * recognize file type (using external libraries), try to get text body (using external libraries),
	 * get other file properties (author, last commit date) and add all these information to index
	 * 
	 * @param svnAddress SVN repository address
	 * @param fileName path to file in SVN repository
	 * @param repository Object represented repository prepared to use 
	 * @param revision revision for updating file.
	 * @param comment comment got from last commiter.
	 * @param latestRevision Latest revision for updating repository
	 * @throws ClassNotFoundException When implementation need external class to load and coulden't find it
	 * @throws IOException during low level problem with access to read or write files.
	 * @throws SVNException Error during connect to SVN repository
	 */
	public void getFileName(String svnAddress, String fileName, SVNRepository repository, long revision, String comment, long latestRevision) throws ClassNotFoundException, IOException, SVNException;
	
	/**
	 * This method should serve directories, similar to file, but it's not using yet.
	 * @param svnAddress SVN repository address
	 * @param directoryName path to directory in SVN repository
	 * @param repository Object represented repository prepared to use 
	 */
	public void getDirectoryName(String svnAddress, String directoryName, SVNRepository repository);
}
