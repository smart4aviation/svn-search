package pl.infovide.SVNIndexUpdater;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.tmatesoft.svn.core.SVNException;
import pl.infovide.SVNIndexer.ConfigReader;
import pl.infovide.SVNIndexer.FileIndex;
import pl.infovide.SVNIndexer.ConfigReaderException;
import pl.infovide.SVNIndexer.FinderFiles;
import pl.infovide.SVNInfo.RepositoryList;



/**
 * This class manage repository list.
 * Creates new repository list from file or from given path ('directPath') in local files system
 * and adding them to index
 * Also manage updating existing repositories in index
 * @author Przemyslaw KLeszczewski
 *
 */
public class IndexRepositoryList
{
	private String indexPath;
	private IndexUpdater updater;
	private FileIndex index;
	private FinderFiles finder;
	private Logger logger = Logger.getLogger("svnindexer");
	IndexSearcher indexSearcher = null;
	/**
	 * Class constructor
	 * @param indexPath path to index files
	 */
	public IndexRepositoryList(String indexPath) {
		this.indexPath = indexPath.trim();
	}
	
	/**
	 * This method find all existed repositories in index (also empty - new added - repositories)
	 * @return created list from all existed repositories in index
	 * @throws IOException during low level problem with access to read or write files.
	 */
	private Hits getIndexedRepository() throws IOException {
		
		try {
			indexSearcher = new IndexSearcher(indexPath);
			Query query = new WildcardQuery(new Term("RepositoryName", "*"));
			Hits hits = indexSearcher.search(query);
			return hits;
		} catch (IOException e) {
			throw e;
		} 
	}
	
	/**
	 * This method manage in updating all repositories existed in index
	 * @param user - username
	 * @param password - password for given username
	 * @throws ClassNotFoundException When implementation need external class to load and coulden't find it
	 * @throws IOException during low level problem with access to read or write files.
	 * @throws ConfigReaderException Error with syntax in xml files
	 */
	public void updateIndexedRepositories(String user, String password) throws IOException, ConfigReaderException, ClassNotFoundException {
		Hits repositoryList ;
		/*
		 * ConfigReader use file, which name is defined in ConfigReader class.
		 */
		ConfigReader configReader = new ConfigReader();
		
		try{
			HashMap<String, String> config = configReader.getConfig();
			repositoryList = getIndexedRepository();
			updater = new IndexUpdater(indexPath, config);
			
		}// Critical type of Exception - SVNIndexer ends after them. 
		catch(IOException e){
			throw e;
		}catch (ConfigReaderException e){
			throw e;
		}
		catch (ClassNotFoundException e){
			throw e;
		}
		logger.info("SVNIndexer has "+repositoryList.length()+" repository(ies) in index to update.");
		// indexing all found in index repositories
		int i = 0;
		try {
			for(; i < repositoryList.length(); i++) {
			
				 logger.info("Starting indexing repository: "+repositoryList.doc(i).get("RepositoryName"));
				 long result;
				 result = 	 updater.update(repositoryList.doc(i).get("RepositoryName"), user, password);
				 if (result != 0 && result != -1 )
					 logger.info("Repository "+repositoryList.doc(i).get("RepositoryName")+" indexed to revision: "+result);
			// these errors only break indexing current repository
			}
		}catch (BooleanQuery.TooManyClauses e){
				logger.error("Repository: "+(repositoryList.doc(i).get("RepositoryName"))+" not updated: "+"SVNIndexer configuration isn't prepared for this amount of clauses.\n "+
        			"Increas \'maxClouseCount\' property in your properties file");
		}catch (SVNException e) {
				logger.fatal("Repository: "+(repositoryList.doc(i).get("RepositoryName"))+" not updated: "+e.getMessage());
				
		}catch (ClassNotFoundException e){
				 // this is kind of fatal exception, SVNIndexer end works after it.
				 throw e;
		}catch (IOException e) {
				 // this is kind of fatal exception, SVNIndexer end works after it.
				 throw e;
		}finally{
				if (indexSearcher != null)
					indexSearcher.close();
		}
			
	}
	
	
	/**
	 * This method firstly takes repository list.
	 * After it method adds new repositories to index (only repositories names, without other information)
	 * Revision, of new added repository, is set on 0, that during updating get all information from 'log'.
	 * @param url - URL address, must have "/" at the end
	 * @param user - username
	 * @param password - password
	 * @throws IOException during low level problem with access to read or write files.
	 */
	public void setIndexRepository(String url, String user, String password, String directPath) throws IOException{
		Hits hits = null;
		String name;
		IndexSearcher indexSearcher = null;
			RepositoryList list = new RepositoryList();
			/// @li Firstly trying to get repositories list.
			ArrayList<String> repList = list.getDirectAccessRepositories(directPath);
		try{	
			index = new FileIndex();
			index.prepareIndex(indexPath);
			
			
		}catch (IOException e){
			throw e;
		}
			/*
			 * For each one repository name, got from directory 'directPath'
			 * or from names in 'repListFile' 
			 */
		try{
			
			while(! repList.isEmpty() ) {
				logger.debug("Set repository to index");
				indexSearcher = new IndexSearcher(indexPath);
				/// @li checking if repository name was taken from 'directPath'or from 'repoFileList'
				if(directPath != null && repList.get(0).startsWith(directPath))
					name = repList.get(0).replace(directPath, "");
				else
					name = repList.get(0);
				Query query = new TermQuery(new Term("RepositoryName", url + name.trim()));
				hits = indexSearcher.search(query);
				
				if(hits.length() < 1){//there is no repository in index with this name
					long revResult = -1;
					try {
						
						logger.info("Repository: "+(url+""+name)+" was not before in index." );
						 ///trying if this repository really exists (can connect to them)
						finder = new FinderFiles(url + repList.get(0), user, password);
						revResult = finder.prepareConnection();
						 ///if yes, then add it's name and url to index
						index.addEntry(url + repList.get(0), null, 0L, false, null);
						repList.remove(0);
					} catch (SVNException  e) {
						logger.error("Problem with connecting to repository: "+e.getMessage());	
						logger.warn("Repository: '"+repList.remove(0)+"' isn't added to index");
					} finally{
						if (finder != null && revResult != -1){		
								finder.closeConnection();
						}
					}
					
				}else if( hits.length() == 1 ){// there is repository in index with this name
					logger.info("Repository: "+(url+""+name)+" was in index." );
					repList.remove(0); //removed from list to checked
				}if (indexSearcher != null){
					indexSearcher.close();
					indexSearcher = null;
				}
			}
			logger.info("Ended checking repository list.");	
				
		}
		catch(IOException e) {
			throw e;
		} finally {
			
			if (indexSearcher != null){
				indexSearcher.close();
				indexSearcher = null;
			}
			if (index != null) {
				logger.info("Optimalizing index after add information about repos names and urls...");	
				index.optimize();
					
			}
		}
	}
}