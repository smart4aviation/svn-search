package pl.infovide.SVNIndexUpdater;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 * Class preparing index to use
 * @author Przemyslaw Kleszczewski
 *
 */
public class Index
{
	private Directory directory; /** < Directory for index files */
	private Analyzer analyzer;
	private IndexWriter writer;
	Logger logger = Logger.getLogger("svnindexer");
	
	/**
	 * Checking if index already exist in given path. If not, then creates new empty index
	 * @param indexPath - path to index files
	 * @throws IOException when coulden't find existed index and create new.	
	 */
	public void prepareIndex(String indexPath) throws IOException {
		File directory = new File(indexPath);
	    if ( directory.exists() ) {
	    	if(IndexReader.indexExists(indexPath)){
	    		logger.info("Existed index was found.");
	    		if (IndexReader.isLocked(FSDirectory.getDirectory(indexPath, false))) {
	  				   logger.info("Trying to unlock directory while preparingIndex...");
	  				     IndexReader.unlock(FSDirectory.getDirectory(indexPath, false));
	    		}
	    		return ;
	    	}
	    	else {
	    		logger.info("Existed index was not found. Creating new index ...");
	    		createNewIndex(indexPath);
	    	}
	    } else {
	        if (! directory.mkdirs() ) 
	        	throw new IOException("Couldent create directory in path: "+indexPath);
	        logger.info("Index directory was not found. Creating new index directory...");
	        createNewIndex(indexPath);
	    }
	}
	
	/**
	 * Creates new empty index in directory given as argument
 	 * @param indexDirectory path to directory, where index files will be created
	 * @throws IOException when coulden't create new index.	
 	 */

	private void createNewIndex(String indexDirectory) throws IOException {
		try {
			directory = FSDirectory.getDirectory(indexDirectory.trim(), true);
			analyzer = new SimpleAnalyzer();
	    	writer = new IndexWriter(directory, analyzer, true);
	    	writer.setUseCompoundFile(true);

		} catch (IOException e) {
			throw e;
		}finally{
	    	writer.close();
	    	writer = null;
		}
		logger.info("New empty index created.");
	}
}