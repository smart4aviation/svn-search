package pl.infovide.SVNIndexUpdater;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Searcher;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.io.SVNRepository;

import com.sun.org.apache.xalan.internal.xsltc.runtime.Hashtable;

import pl.infovide.SVNIndexer.FileIndex;
import pl.infovide.SVNIndexer.FinderFiles;
import pl.infovide.SVNIndexer.FinderFilesListener;
import pl.infovide.SVNIndexer.GetterNeededFiles;

/**
 * Class to update index files
 * Update is based on log from SVN and information about revision kept in index
 * @author Przemyslaw Kleszczewski
 *
 */
public class IndexUpdater
{
	private TreeMap<String, String> looked; /**< Map with files already checked (filePath -> pathType)>*/
	private Query query; /**<Query using in searching files>*/
	private Hits hits; /**<Special list with result of the query>*/
	private Searcher searcher; /**<Handle to the searcher>*/
	private String indexPath; /**<Path to directory with index files>*/
	private SVNRepository repository; /**<Handle to repository manager>*/
	private FinderFiles finder; /**<Object managing connection to SVN>*/
	private Collection<SVNLogEntry> entry; /**<Collection with logs from one revision>*/
	private FileIndex fileIndex; /**<Handle to index manager>*/
	private FinderFilesListener listener; /**< Listener which can server files and directories (adding entry to index)>*/
	private Logger logger = Logger.getLogger("svnindexer");
	long latestRev = -1;
	
	/**
	 * Class constructor, makes connection to index.
	 * @param indexPath - path to index files
	 * @param config - Map: fileType -> Class name suitable to parse this type
	 * @throws ClassNotFoundException When implementation need external class to load and coulden't find it - to load class to find file type or to get body from supported file type
	 * @throws IOException during low level problem with access to read or write index files.
	 */
	public IndexUpdater(String indexPath, HashMap<String, String> config) throws IOException, ClassNotFoundException {
		this.indexPath = indexPath;
		fileIndex = new FileIndex();
		try{
			fileIndex.prepareIndex(indexPath);
			listener = new GetterNeededFiles(fileIndex, config);
		} catch (IOException e){
			throw e;
		} catch (ClassNotFoundException e){
			throw e;
		}
		
		
	}
	
	/**
	 * This method delete row for given file from index
	 * @param filePath - path to file, which has to be removed from index
	 * @param reader - index reader, which can delete rows from index.
	 * @return true - if operation finished with success
	 * @throws IOException during low level problem with access to read or write index files.
	 * @throws SVNException Exception if some error in preparing connection occurred. 
	 */
	public boolean delete(String filePath) throws IOException, SVNException{
		int spr;
		IndexReader indexReader = null;
		try {
			logger.debug("Deleting file from index");
	   	 indexReader = IndexReader.open(indexPath);
			
			if(filePath != null)
				spr = indexReader.deleteDocuments( new Term("FileName", repository.getRepositoryRoot(false).toString() + filePath) );
			else
				spr = indexReader.deleteDocuments( new Term("RepositoryName", repository.getRepositoryRoot(false).toString() ) );
			
			if(spr != 1)
				checkDir(filePath);
			
		} catch (IOException e) {
			throw e;
		} catch (SVNException e) {
			throw e;
		} finally {
			if (indexReader != null) {
				indexReader.close();
				indexReader = null;
			}
		}
		return true;
	}
	
	/**
	 * This method adds file to index (calling suitable listener)
	 * @param filePath - path to the file
	 * @throws IOException during low level problem with access to read or write index files.
	 * @throws ClassNotFoundException When implementation need external class to load and coulden't find it - to load class to find file type or to get body from supported file type
	 */
	private void addFile(String filePath, String comment) throws ClassNotFoundException, IOException{
			try {
				logger.debug("Adding file to index");
				listener.getFileName(repository.getRepositoryRoot(false).toString(), filePath, repository, latestRev, comment, latestRev);
			} catch (ClassNotFoundException e) {
				throw e;
			}catch (OutOfMemoryError e)	 {
				logger.error("Out of memory error occurred, file: "+filePath+" could not be indexed, Reason: "+e.getMessage());
			}
			catch (IOException e) {
				throw e;
			}catch (SVNException e) {
				logger.error("File: "+filePath+" coulden't be indexed: "+e.getMessage());
			}
	}
	
	/**
	 * This method is using when Indexer find object, which was copied
	 * from other path. Then this origin 'path' is being 'crawl' (searching recurrently)
	 * and index all files from given directory path.
	 * @param path - path to main directory
	 * @throws IOException during low level problem with access to read or write index files.
	 * @throws ClassNotFoundException When implementation need external class to load and coulden't find it - to load class to find file type or to get body from supported file type
	 */
	private void addFolder(String path)throws ClassNotFoundException, IOException {
		logger.info("Crawling repository: "+path);
		try {
			finder.crawlRepository(path, listener, this);
		}catch (OutOfMemoryError e){
			logger.error("Out of memory error during crawling path: "+ path+", "+e.getMessage());
		}
		catch (ClassNotFoundException e){
			throw e;
		}
		catch (IOException e){
			throw e;
		}
	}
	
	/**
	 * This method optimizes index 
	 * @throws IOException during low level problem with access to read or write index files.
	 *
	 */
	public void optimize() throws IOException{
		try{
			fileIndex.optimize();
		} catch (IOException e){
			throw e;
		}
	}
	
	/**
	 * This method checks if directory was deleted.
	 * If yes, then all sub directories and all files will be deleted from index
	 * @param path - path to sub directory
	 * @throws IOException during low level problem with access to read or write index files.
	 * @throws SVNException Exception if some error in preparing connection occurred. 
	 */
	private void checkDir(String path) throws SVNException, IOException {
		try {
			logger.debug("Checking if path is directory and crawl it");
			if( repository.checkPath(path, latestRev) == SVNNodeKind.NONE) {
				closeSearcher();
				searcher = new IndexSearcher(indexPath);
				query = new WildcardQuery(new Term("FileName", repository.getRepositoryRoot(false).toString() + path + "/*"));
				hits = searcher.search(query);
				for(int i = 0 ; i < hits.length(); i++)
					delete(hits.doc(i).get("FileName").substring(repository.getRepositoryRoot(false).toString().length(), hits.doc(i).get("FileName").length()));
			}
		} catch (SVNException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}finally {
			closeSearcher();
		}
	}
	
	/**
	 * Checking if given path was in TreeMap (each part of them). 
	 * @param path - checking path
	 * @return true if was is TreeMap
	 */
	private boolean myCompare(String path) {

			logger.debug("Checking previous paths");
			int indexOf = 0, lastIndex = 1;
			if(looked != null && looked.size() != 0) {
				while(lastIndex < path.length()) {
					indexOf = path.indexOf('/', lastIndex);
					if(indexOf == -1) {
						if( looked.containsKey(path) )
							return true;
						else
							return false;
					}
					else {
						//checking if directory "over" this file wasn't before in 'looked'
						if(looked.containsKey(path.substring(0, indexOf)))
							return true;
						lastIndex = ++indexOf;
					}
				}
			}
			return false;
	}
	
	/**
	 * Method searches index and returns result of searching.
	 * Only exactly matching elements are found
	 * @param key - category to being search
	 * @param value - value to find
	 * @return - return exactly matching adjustment of the value to this found in index.
	 * @throws IOException during low level problem with access to read or write index files.
	 * @throws BooleanQuery.TooManyClauses To many documents to index. Set grater value in properties file for 'maxClouseCount'
	 */
	private Hits search(String key, String value)throws IOException, BooleanQuery.TooManyClauses {
		try {
			logger.debug("Searching old keys in index");
			closeSearcher();
			query = new TermQuery(new Term(key, value));
			searcher = new IndexSearcher(indexPath);
			return searcher.search(query);
		} catch(BooleanQuery.TooManyClauses  e) {
			throw e;
		}
		catch (IOException e) {
			throw e;
		}
	}
	
	/**
	 * Main method updating repository.
	 * This method implements all logic connected with updating repository
	 * 
	 * Method gets log from SVN repository and analyzes it.
	 * Checking each file i each revision it got. Checking special flags 'type'.
	 * If decide to index file, then calls suitable method to add file to index.
	 * <Possible modification is to make more operation on index in one session, but there is no sure it takes some pickup>
	 *
	 * @param url - svn address repository
	 * @param user - username
	 * @param password - password for given username
	 * @throws IOException during low level problem with access to read or write index files. These are critical errors. Indexer ends works after them.
	 * @throws BooleanQuery.TooManyClauses To many documents to index. Set grater value in properties file for 'maxClouseCount'. This exception only breaks indexing current repository.
	 * @throws SVNException Exception if some error in preparing connection occurred. This exception only breaks indexing current repository.	
	 * @throws ClassNotFoundException When implementation need external class to load and coulden't find it - to load class to find file type or to get body from supported file type. These are critical errors. Indexer ends works after them.
	 */
	@SuppressWarnings("unchecked")
	public long update(String url, String user, String password) throws ClassNotFoundException, SVNException, IOException, BooleanQuery.TooManyClauses {
		latestRev = -1;
		try {
			SVNLogEntryPath path;
			TreeMap<String, SVNLogEntryPath> m;
			ArrayList<SVNLogEntry> l;
			SVNLogEntry ent;
			String[] changedPaths = new String[1];
	
			looked = new TreeMap<String, String>();
			entry = new Vector<SVNLogEntry>();
			finder = new FinderFiles(url, user, password);
			latestRev = finder.prepareConnection();
			repository = finder.getRepository();

			//repositories names must be exactly the same as root of the repository
			hits = search("RepositoryName", repository.getRepositoryRoot(false).toString());
			logger.debug("Updating files");
			if((hits != null) && (hits.length() > 0)){
				if(latestRev > new Long (hits.doc(0).get("Revision")) ) {
					logger.info("Downloading log from repository...");
					repository.log(changedPaths, entry, latestRev, (new Long (hits.doc(0).get("Revision"))) + 1L, true, true);
					closeSearcher();
					///each object in 'l' represent one revision
					l = new ArrayList<SVNLogEntry>(entry);
					logger.debug("Checking log entry");
					boolean onlyOnce = true;
					while(!l.isEmpty()) {
						ent = l.remove(0);
						m = new TreeMap<String, SVNLogEntryPath>(ent.getChangedPaths());
						
						///each object in 'm' represent one changed path in this revision
						boolean notUpdated = false;
						while(!m.isEmpty()) {
							path = m.remove(m.firstKey());
							hits = search("FileName", repository.getRepositoryRoot(false).toString() + path.getPath() );
							//if current file hasn't been in index yet or it's revision is older than 
							// currently taken from 'log'.
							if (notUpdated && onlyOnce){
								logger.info("Checking for documents indexed in before indexing...");
								onlyOnce = false;
							}
							if( (hits.length() == 0) 
									|| (Long.parseLong(hits.doc(0).get("Revision")) < ent.getRevision()) ) {
								closeSearcher();
								if( ! myCompare( path.getPath() ) )
								{
									delete(path.getPath());
									if( path.getType() == 'M')
										addFile(path.getPath(), ent.getMessage());
									else if(path.getType() == 'A') {
										if(path.getCopyPath() != null){
											if (repository.checkPath(path.getPath(), latestRev) == SVNNodeKind.FILE){
												//delete(path.getCopyPath());
												addFile(path.getPath(), ent.getMessage());
											}
											else {
												addFolder(path.getPath());
											}
										}
										else
											addFile(path.getPath(), ent.getMessage());
									}
									else if(path.getType() == 'R')
										addFile(path.getPath(), ent.getMessage());
									else if(path.getType() == 'D')
										looked.put( path.getPath(), (new Character(path.getType())).toString() );
									
								}
								notUpdated = false;
							}else{
								notUpdated = true;
							}
						}
					}
				}
				// deleting old information about 'current index revision'
				delete(null);
				// set latest revision as 'current index revision'. 
				fileIndex.addEntry(repository.getRepositoryRoot(false).toString(), null, latestRev, false, null);
				
			}else {
				logger.warn("Repository: "+repository.getRepositoryRoot(false).toString()+" is empty, make sure you type URL and rep. name correctly." );
				closeSearcher();
				finder.closeConnection();
				optimize();
				return 0;
			}

		// these exceptions only break indexing current repository.	
		}catch (SVNException e) {
			throw e;
		}
		catch (BooleanQuery.TooManyClauses e){
			throw e;
			
		} // all exceptions below are critical errors. Indexer end works after them
		catch (ClassNotFoundException e){
			throw e;
		}
		catch (IOException e) {
			throw e;
		} 
		catch (OutOfMemoryError e){
			logger.error("Out of memory errr occurred during indexing repository: "+repository.getRepositoryRoot(false).toString()+". Repo. is not indexed. Try to run SVNIndexer with more memory.");
			e.printStackTrace();
			return 0;
		}
		finally {
			closeSearcher();
			if (repository != null)
				logger.info("Optymalizing index after indexing repository: "+repository.getRepositoryRoot(false).toString());
			optimize();
			if (finder != null)
				finder.closeConnection();
		}
		return latestRev;
	}
	private void closeSearcher() throws IOException{
		if(searcher != null){
			searcher.close();
			searcher = null;
		}
	}
}
