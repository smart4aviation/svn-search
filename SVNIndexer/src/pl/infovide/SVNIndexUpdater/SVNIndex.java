package pl.infovide.SVNIndexUpdater;

import java.io.IOException;
import java.io.FileNotFoundException;
import org.apache.log4j.Logger;

import pl.infovide.SVNIndexer.ConfigReaderException;
import pl.infovide.SVNInfo.ConfigurationProvider;
import pl.infovide.SVNInfo.ConfProviderException;

/**
 * Main class for SVNIndexer
 * @author Przemyslaw KLeszczewski
 *
 */
public class SVNIndex
{
	private Index index;
	private ConfigurationProvider provider;
	private IndexRepositoryList indexList;
	Logger logger = Logger.getLogger("svnindexer");

	
	public SVNIndex() {
	
	}
	
	/**
	 * This method makes index, and starts method updating it
	 */
	public void index() {
		try{
			org.apache.log4j.PropertyConfigurator.configure("log4j.properties");
		} catch (Exception e){
			logger.fatal("SVNIndexer failed: "+e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
		
		provider = new ConfigurationProvider();
		try {
		 provider.getConfiguration(provider.getFilename());
		
		} catch (FileNotFoundException e) {
			logger.fatal("SVNIndexer failed: "+e.getMessage());
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			logger.fatal("SVNIndexer failed: "+e.getMessage());
			e.printStackTrace();
			System.exit(1);
		} catch (ConfProviderException e) {
			logger.fatal("SVNIndexer failed: "+e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}

		index = new Index();
		
		//finding existed index or create new in given directory
		try {
			index.prepareIndex(provider.getIndexPath());
		}catch  (IOException ex){
			logger.fatal("SVNIndexer failed. Index is not prepared: "+ex.getMessage());
			ex.printStackTrace();
			System.exit(1);
		}
		
			indexList = new IndexRepositoryList(provider.getIndexPath());
		try {	
			indexList.setIndexRepository(provider.getUrl(), provider.getUser(), provider.getPassword(), provider.getDirectPath());
			indexList.updateIndexedRepositories(provider.getUser(), provider.getPassword());	
		}catch (IOException ex){
			logger.fatal("SVNIndexer failed: "+ex.getMessage());
			ex.printStackTrace();
			System.exit(1);
		}catch (ConfigReaderException e){
			logger.fatal("SVNIndexer failed: "+e.getMessage());
			e.printStackTrace();
			System.exit(1);
		} catch (ClassNotFoundException e){
			logger.fatal("SVNIndexer failed: "+e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	
	public static void main(String[] args) {
		Logger logger = Logger.getLogger("svnindexer");
		if (args.length != 1) {
			logger.fatal("Usage: java pl.infovide.SVNIndexUpdater.SVNIndex fileName.properties");
			System.exit(1);
		}
		SVNIndex in = new SVNIndex();
		ConfigurationProvider.setFilename(args[0]);
		in.index();
		logger.info("SVNIndexer finished");

	}
}