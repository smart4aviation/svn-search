:: This script install needed library to your local maven repository
:: %MY_LIBRARY_PATH% should be seted to %SVNINDEXER_PATH%/lib
:: find more information in 'readme.txt' 
:: Comment line below, if you execute this script not from SVNIndexer root directory
:: and use direct path then

set MY_LIBRARY_PATH=lib/

CALL mvn install:install-file -DgroupId=lib -DartifactId=log4j -Dversion=1.2.8 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/log4j-1.2.8.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=javasvn -Dversion=1 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/javasvn.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=ganymed -Dversion=1 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/ganymed.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=icu4j_3_4_4 -Dversion=1 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/icu4j_3_4_4.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=javasvn-cli -Dversion=1 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/javasvn-cli.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=javasvn-javahl -Dversion=1 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/javasvn-javahl.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=lucene-core -Dversion=2.0.0 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/lucene-core-2.0.0.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=PDFBox -Dversion=0.7.2 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/PDFBox-0.7.2.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=poi -Dversion=3.0.1-FINAL-20070705 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/poi-3.0.1-FINAL-20070705.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=poi-contrib -Dversion=3.0.1-FINAL-20070705 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/poi-contrib-3.0.1-FINAL-20070705.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=poi-scratchpad -Dversion=3.0.1-FINAL-20070705 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/poi-scratchpad-3.0.1-FINAL-20070705.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=svnkit -Dversion=1 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/svnkit.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=svnkit-cli -Dversion=1 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/svnkit-cli.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=svnkit-javahl -Dversion=1 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/svnkit-javahl.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=ganymed -Dversion=1 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/ganymed.jar
  
CALL mvn install:install-file -DgroupId=lib -DartifactId=jmimemagic -Dversion=0.1.2 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/jmimemagic-0.1.2.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=commons-logging -Dversion=1.1 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/commons-logging-1.1.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=oro -Dversion=2.0.8 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/oro-2.0.8.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=xerces -Dversion=2.4.0 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/xerces-2.4.0.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=xercesImpl -Dversion=2.7.1 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/xercesImpl-2.7.1.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=xml-apis -Dversion=2.0.2 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/xml-apis-2.0.2.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=xmlParserAPIs -Dversion=2.0.2 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/xmlParserAPIs-2.0.2.jar

CALL mvn install:install-file -DgroupId=lib -DartifactId=truezip -Dversion=6 -Dpackaging=jar -Dfile=%MY_LIBRARY_PATH%/truezip-6.jar