Quick usage:

1) Edit 'config.properties' to set obligatory properties.
2) Edit 'addCron.sh 'and set path to your SVNIndexer directory (you can also modify date parameters and path to property file or leave it default).
3) execute './addCron.sh' to add task indexing to crontab for your user.

For manually start SVNIndexer: 
		1) export your environment viriable $SVNINDEXER_PATH to main directory of SVNIndexer application.
		2) execute '$SVNINDEXER_PATH/startJar.sh $SVNINDEXER_PATH/conf.properties'


------------------------------------------------
Quick description: How to  compile SVNIndexer:

You need installed maven (http://maven.apache.org)

1) Export variable $MY_LIBRARY_PATH to '$SVNINDEXER_PATH/lib' in 'script.sh' file,
or leave it default, but your current directory must be 'SVNIndexer' root directory then. 
2) execute './script.sh' to install all necessarily libraries.
3) compile with 'mvn package'
4) in target directory you have SVNIndexer-1.3.0.jar file. On top of this document you can find description about using SVNIndexer.