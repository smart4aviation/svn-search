#!/bin/sh

###### CONFIGURATION PART
##set date for starting SVNINdexer:
## * - for each minute/hour/day/month/week-day
m="30"; #minute 
h="03"; #hour
d="*"; #day
mo="*"; #month
w="1-7"; #week day

##set path to directory with SVNIndexer java application:
SVNINDEXER_PATH=/path/to/directory/with/SVNIndexer;

##properties file name (in SVNIndexer directory) or path to the file if it's in other directory.
property_file=conf.properties;

####### END OF CONFIGURATION PART

####### DO NOT modify this part
export SVNINDEXER_PATH;

echo "checking previous cron tasks..."
crontab -l > temp;

chmod -f 755 temp; 
echo " "$m" "$h" "$d" "$mo" "$w"  export SVNINDEXER_PATH=$SVNINDEXER_PATH ; cd \$SVNINDEXER_PATH ; ./startJar.sh $property_file ;"    >> temp;

crontab temp;

rm temp;

## Use this option to see current cron tasks
#crontab -l ;

echo "Task to cron added.
Each execution of this script adds new task for indexer.
Use 'crontab -l' to see current cron tasks.
To modify crontab task use 'crontab -e'"
