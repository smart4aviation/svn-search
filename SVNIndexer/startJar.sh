#!/bin/sh

# This file is using for start SVNIndexer using jar.
# You can execute it manually with configuration file
# as an argument.

if test "x$1" != "x"
then
	echo "Starting indexer..."
	cd $SVNINDEXER_PATH
	java -cp 'lib/*' -jar SVNIndexer-1.3.0.jar $1
	echo "Indexing finished"
else
	echo "Usage error: properties file must be specified!"
	echo "Example: ./startJar.sh conf.properties"
fi
